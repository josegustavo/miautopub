<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comunity_meta".
 *
 * @property string $id
 * @property string $comunity_id
 * @property integer $time
 * @property string $key
 * @property string $value
 *
 * @property Comunity $comunity
 */
class ComunityMeta extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comunity_meta';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comunity_id', 'time', 'key'], 'required'],
            [['comunity_id', 'time'], 'integer'],
            [['value'], 'string'],
            [['key'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'comunity_id' => 'Comunity ID',
            'time' => 'Time',
            'key' => 'Key',
            'value' => 'Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComunity()
    {
        return $this->hasOne(Comunity::className(), ['id' => 'comunity_id']);
    }
}
