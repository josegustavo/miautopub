<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log".
 *
 * @property string $id
 * @property string $user_id
 * @property string $displayname
 * @property integer $datetime
 * @property string $controller_action
 * @property string $event
 * @property string $data
 */
class Log extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'datetime'], 'integer'],
            [['data'], 'string'],
            [['displayname'], 'string', 'max' => 30],
            [['controller_action', 'event'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'displayname' => 'Displayname',
            'datetime' => 'Datetime',
            'controller_action' => 'Controller Action',
            'event' => 'Event',
            'data' => 'Data',
        ];
    }
}
