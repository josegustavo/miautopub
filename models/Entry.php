<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "entry".
 *
 * @property string $id
 * @property string $comunity_id
 * @property string $post_global_id
 * @property string $post_title
 * @property string $post_url
 * @property string $post_categories
 * @property string $publish_links
 * @property string $publish_messages
 * @property string $publish_pictures
 * @property string $publish_names
 * @property string $publish_captions
 * @property string $publish_descriptions
 * @property integer $created_time
 * @property integer $modified_time
 * @property integer $last_published_time
 * @property string $og_id
 * @property string $og_image
 * @property integer $og_share_count
 * @property integer $og_comment_count
 * @property integer $publication_count
 * @property integer $can_publish
 * @property string $publishers
 * @property string $tags_groups
 *
 * @property Comunity $comunity
 * @property Publication[] $publications
 */
class Entry extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'entry';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comunity_id', 'created_time', 'modified_time', 'last_published_time', 'og_id', 'og_share_count', 'og_comment_count', 'publication_count', 'can_publish'], 'integer'],
            [['post_global_id', 'post_url', 'post_categories', 'publish_links', 'publish_messages', 'publish_pictures', 'publish_names', 'publish_captions', 'publish_descriptions', 'og_image', 'publishers', 'tags_groups'], 'string'],
            [['post_title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'comunity_id' => 'Comunity ID',
            'post_global_id' => 'Post Global ID',
            'post_title' => 'Post Title',
            'post_url' => 'Post Url',
            'post_categories' => 'Post Categories',
            'publish_links' => 'Publish Links',
            'publish_messages' => 'Publish Messages',
            'publish_pictures' => 'Publish Pictures',
            'publish_names' => 'Publish Names',
            'publish_captions' => 'Publish Captions',
            'publish_descriptions' => 'Publish Descriptions',
            'created_time' => 'Created Time',
            'modified_time' => 'Modified Time',
            'last_published_time' => 'Last Published Time',
            'og_id' => 'Og ID',
            'og_image' => 'Og Image',
            'og_share_count' => 'Og Share Count',
            'og_comment_count' => 'Og Comment Count',
            'publication_count' => 'Publication Count',
            'can_publish' => 'Can Publish',
            'publishers' => 'Publishers',
            'tags_groups' => 'Tags Groups',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComunity()
    {
        return $this->hasOne(Comunity::className(), ['id' => 'comunity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublications()
    {
        return $this->hasMany(Publication::className(), ['entry_id' => 'id']);
    }
}
