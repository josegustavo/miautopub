<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "publisher".
 *
 * @property string $id
 * @property string $comunity_id
 * @property string $username
 * @property string $email
 * @property string $name
 * @property string $status
 * @property string $image_url
 * @property integer $created_time
 * @property integer $modified_time
 * @property string $creator_id
 *
 * @property GroupPublisher[] $groupPublishers
 * @property Group[] $groups
 * @property Publication[] $publications
 * @property Comunity $comunity
 * @property PublisherMeta[] $publisherMetas
 * @property Subscription[] $subscriptions
 */
class Publisher extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'publisher';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'comunity_id', 'username', 'email', 'status', 'image_url', 'created_time', 'modified_time'], 'required'],
            [['id', 'comunity_id', 'created_time', 'modified_time', 'creator_id'], 'integer'],
            [['image_url'], 'string'],
            [['username'], 'string', 'max' => 64],
            [['email', 'name'], 'string', 'max' => 128],
            [['status'], 'string', 'max' => 32]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'comunity_id' => 'Comunity ID',
            'username' => 'Username',
            'email' => 'Email',
            'name' => 'Name',
            'status' => 'Status',
            'image_url' => 'Image Url',
            'created_time' => 'Created Time',
            'modified_time' => 'Modified Time',
            'creator_id' => 'Creator ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupPublishers()
    {
        return $this->hasMany(GroupPublisher::className(), ['publisher_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroups()
    {
        return $this->hasMany(Group::className(), ['id' => 'group_id'])->viaTable('group_publisher', ['publisher_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublications()
    {
        return $this->hasMany(Publication::className(), ['publisher_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComunity()
    {
        return $this->hasOne(Comunity::className(), ['id' => 'comunity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublisherMetas()
    {
        return $this->hasMany(PublisherMeta::className(), ['publisher_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriptions()
    {
        return $this->hasMany(Subscription::className(), ['creator_id' => 'id']);
    }
}
