<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "group".
 *
 * @property string $id
 * @property string $comunity_id
 * @property string $wall_id
 * @property string $name
 * @property string $image_url
 * @property integer $can_publish
 * @property string $reason
 * @property integer $members_count
 * @property integer $publication_count
 * @property integer $last_published_time
 * @property string $tags
 *
 * @property Comunity $comunity
 * @property GroupPublisher[] $groupPublishers
 * @property Publisher[] $publishers
 * @property Publication[] $publications
 */
class Group extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comunity_id', 'wall_id', 'name'], 'required'],
            [['comunity_id', 'wall_id', 'can_publish', 'members_count', 'publication_count', 'last_published_time'], 'integer'],
            [['tags'], 'string'],
            [['name', 'image_url', 'reason'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'comunity_id' => 'Comunity ID',
            'wall_id' => 'Wall ID',
            'name' => 'Name',
            'image_url' => 'Image Url',
            'can_publish' => 'Can Publish',
            'reason' => 'Reason',
            'members_count' => 'Members Count',
            'publication_count' => 'Publication Count',
            'last_published_time' => 'Last Published Time',
            'tags' => 'Tags',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComunity()
    {
        return $this->hasOne(Comunity::className(), ['id' => 'comunity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupPublishers()
    {
        return $this->hasMany(GroupPublisher::className(), ['group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublishers()
    {
        return $this->hasMany(Publisher::className(), ['id' => 'publisher_id'])->viaTable('group_publisher', ['group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublications()
    {
        return $this->hasMany(Publication::className(), ['group_id' => 'id']);
    }
}
