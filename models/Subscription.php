<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "subscription".
 *
 * @property string $id
 * @property string $comunity_id
 * @property string $creator_id
 * @property string $order_id
 * @property string $plan
 * @property integer $active_users
 * @property integer $created_time
 * @property integer $start_time
 * @property integer $end_time
 * @property integer $active
 *
 * @property Order[] $orders
 * @property Publisher $creator
 * @property Comunity $comunity
 * @property Order $order
 */
class Subscription extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subscription';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'plan', 'active_users', 'created_time', 'start_time', 'end_time'], 'required'],
            [['id', 'comunity_id', 'creator_id', 'order_id', 'active_users', 'created_time', 'start_time', 'end_time', 'active'], 'integer'],
            [['plan'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'comunity_id' => 'Comunity ID',
            'creator_id' => 'Creator ID',
            'order_id' => 'Order ID',
            'plan' => 'Plan',
            'active_users' => 'Active Users',
            'created_time' => 'Created Time',
            'start_time' => 'Start Time',
            'end_time' => 'End Time',
            'active' => 'Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['subscription_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(Publisher::className(), ['id' => 'creator_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComunity()
    {
        return $this->hasOne(Comunity::className(), ['id' => 'comunity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
}

