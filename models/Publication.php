<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "publication".
 *
 * @property string $id
 * @property string $comunity_id
 * @property string $publisher_id
 * @property string $group_id
 * @property string $entry_id
 * @property string $fb_object_id
 * @property string $content
 * @property string $status
 * @property integer $created_time
 * @property integer $modified_time
 * @property integer $published_time
 * @property integer $comment_count
 * @property integer $like_count
 *
 * @property Entry $entry
 * @property Group $group
 * @property Comunity $comunity
 * @property Publisher $publisher
 */
class Publication extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'publication';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comunity_id', 'publisher_id', 'group_id', 'entry_id', 'created_time', 'modified_time', 'published_time', 'comment_count', 'like_count'], 'integer'],
            [['fb_object_id', 'created_time', 'modified_time', 'published_time'], 'required'],
            [['content'], 'string'],
            [['fb_object_id'], 'string', 'max' => 128],
            [['status'], 'string', 'max' => 32]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'comunity_id' => 'Comunity ID',
            'publisher_id' => 'Publisher ID',
            'group_id' => 'Group ID',
            'entry_id' => 'Entry ID',
            'fb_object_id' => 'Fb Object ID',
            'content' => 'Content',
            'status' => 'Status',
            'created_time' => 'Created Time',
            'modified_time' => 'Modified Time',
            'published_time' => 'Published Time',
            'comment_count' => 'Comment Count',
            'like_count' => 'Like Count',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntry()
    {
        return $this->hasOne(Entry::className(), ['id' => 'entry_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Group::className(), ['id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComunity()
    {
        return $this->hasOne(Comunity::className(), ['id' => 'comunity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublisher()
    {
        return $this->hasOne(Publisher::className(), ['id' => 'publisher_id']);
    }
}
