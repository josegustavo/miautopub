<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "publisher_meta".
 *
 * @property string $id
 * @property string $publisher_id
 * @property integer $time
 * @property string $key
 * @property string $value
 *
 * @property Publisher $publisher
 */
class PublisherMeta extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'publisher_meta';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['publisher_id', 'time', 'key'], 'required'],
            [['publisher_id', 'time'], 'integer'],
            [['value'], 'string'],
            [['key'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'publisher_id' => 'Publisher ID',
            'time' => 'Time',
            'key' => 'Key',
            'value' => 'Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublisher()
    {
        return $this->hasOne(Publisher::className(), ['id' => 'publisher_id']);
    }
}
