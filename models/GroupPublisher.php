<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "group_publisher".
 *
 * @property string $group_id
 * @property string $publisher_id
 * @property integer $active
 * @property string $reason
 *
 * @property Publisher $publisher
 * @property Group $group
 */
class GroupPublisher extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'group_publisher';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id', 'publisher_id'], 'required'],
            [['group_id', 'publisher_id', 'active'], 'integer'],
            [['reason'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'group_id' => 'Group ID',
            'publisher_id' => 'Publisher ID',
            'active' => 'Active',
            'reason' => 'Reason',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublisher()
    {
        return $this->hasOne(Publisher::className(), ['id' => 'publisher_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Group::className(), ['id' => 'group_id']);
    }
}
