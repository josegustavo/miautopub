<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comunity".
 *
 * @property string $id
 * @property string $name
 * @property string $status
 * @property integer $created_time
 *
 * @property ComunityMeta[] $comunityMetas
 * @property Entry[] $entries
 * @property Group[] $groups
 * @property Order[] $orders
 * @property Publication[] $publications
 * @property Publisher[] $publishers
 * @property Subscription[] $subscriptions
 */
class Comunity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comunity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name', 'status', 'created_time'], 'required'],
            [['id', 'created_time'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['status'], 'string', 'max' => 32]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'status' => 'Status',
            'created_time' => 'Created Time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComunityMetas()
    {
        return $this->hasMany(ComunityMeta::className(), ['comunity_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntries()
    {
        return $this->hasMany(Entry::className(), ['comunity_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroups()
    {
        return $this->hasMany(Group::className(), ['comunity_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['comunity_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublications()
    {
        return $this->hasMany(Publication::className(), ['comunity_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublishers()
    {
        return $this->hasMany(Publisher::className(), ['comunity_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriptions()
    {
        return $this->hasMany(Subscription::className(), ['comunity_id' => 'id']);
    }
}
