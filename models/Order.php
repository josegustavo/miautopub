<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order".
 *
 * @property string $id
 * @property string $comunity_id
 * @property string $subscription_id
 * @property integer $created_time
 * @property string $status
 * @property string $plan
 * @property string $meta
 *
 * @property Subscription $subscription
 * @property Comunity $comunity
 * @property Subscription[] $subscriptions
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'comunity_id', 'created_time', 'plan'], 'required'],
            [['id', 'comunity_id', 'subscription_id', 'created_time'], 'integer'],
            [['status', 'plan', 'meta'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'comunity_id' => 'Comunity ID',
            'subscription_id' => 'Subscription ID',
            'created_time' => 'Created Time',
            'status' => 'Status',
            'plan' => 'Plan',
            'meta' => 'Meta',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscription()
    {
        return $this->hasOne(Subscription::className(), ['id' => 'subscription_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComunity()
    {
        return $this->hasOne(Comunity::className(), ['id' => 'comunity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriptions()
    {
        return $this->hasMany(Subscription::className(), ['order_id' => 'id']);
    }
}

