<?php

return [
    
    'adminEmail' => 'admin@miautopub.info',
    'timezone' => 'America/Lima',
    
    'FbLoginAppId' => "100302690321439", //MiAutoPub Login
    'FbLoginAppSecret' => "5bb88b2a4ed878f765935794a8974118", 
    //'FbLoginAppId' => "1642272955991009", //ID de la aplicación para loguearse en Facebook
    //'FbLoginAppSecret' => "24c7979755cae38cbf0e9e6f1f417b3b", //Secret de la aplicación para loguearse en Facebook
    
    'appsToPublish' => [
        "379332348917570" => [
            "app_id" => "379332348917570",
            "app_secret" => "35ddeb128d2699aac1dbeac8c5082417",
            "version" => "v2.2",
            "user_token" => "CAAFZAADHot0IBAGZAYMRXxZB9qn2GpA77Fgs0f17A8WvVBZCwqq0UZAmrPl0yCEhTVbwAIQ3iaXTPwBPn5gjqKiGuwMqFaSZBgT0cmBQB57w28auDDmQDyq54CLuNuBo3WS8hTwvH5ZBKrjXHsJ6gZC9aXBoYFZCs6HYkNfx0UrJZCbUXXxVXutb2QSQOPs0SxnZAcZD",
            "expire" => '1454643739'
        ],
        "1642272955991009" => [
            "app_id" => "1642272955991009",
            "app_secret" => "24c7979755cae38cbf0e9e6f1f417b3b",
            "version" => "v2.3",
            "user_token" => "CAAXVo3gqgZBEBABiOT5OACeuJ3sGf2946ivYECXZAuz2tOZBB8OQSZCz3osQ73ymfSqhYb5iXsGo9b6gHz1LObNjGHGTEPLQXupC03LzTkd1KeNUomnC1bNaRFX5egdYEBNoy37oK4uvsfGtNxzDj3e7kmiWGHNKj08BArHaMSOVZCgbh7WbO",
            "expire" => "1454643833"
        ]
    ],
    
    'paypal' => [
        'url' => 'https://www.paypal.com/cgi-bin/webscr',
        'pdt_identity_token' => 'mEiDw4ygfP2R4BQ3vT-4vTPhbCymEBqzzOailvz8YXwJ1_hFBC-fGU0oDUy',
        'business' => 'K8PTRXKV9E43L'
     ],
    
    'roles' => [
        'admin' => "Administrador",
        'user' => "Usuario",
        'publisher' => "Bot",
    ],
    
    'urlRoot' => ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')?'https' : 'http') . '://' .  $_SERVER['HTTP_HOST'].preg_replace('/\/$/', '', $_SERVER['CONTEXT_PREFIX']),
    'uriCDN' => ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')?'https' : 'http') . '://' .  $_SERVER['HTTP_HOST'].preg_replace('/\/$/', '', $_SERVER['CONTEXT_PREFIX']),
    
    
    'planes' => [
        'free' => [
            'name' => 'FREE',
            'publishers' => 1,
            'discount_month' => 0,
            'days' => 3,
            'price_other' => 0,
            'price_peru' => 0,
        ],
        'basic' => [
            'name' => 'BASIC',
            'publishers' => 1,
            'discount_month' => 0,
            'month_min' => 1,
            'month_max' => 20,
            'price_other' => 10,
            'price_peru' => 30,
        ],
        'business' => [
            'name' => 'BUSINESS',
            'publishers' => 3,
            'discount_month' => 5,
            'month_min' => 1,
            'month_max' => 20,
            'price_other' => 15,
            'price_peru' => 45,
        ],
        'premiun' => [
            'name' => 'PREMIUN',
            'publishers' => 5,
            'discount_month' => 10,
            'month_min' => 1,
            'month_max' => 20,
            'price_other' => 20,
            'price_peru' => 60,
        ],
    ],
    
    'css_include' => [
		'css/app.min.css',/*
        'bower_components/bootstrap/dist/css/bootstrap.css',
        'bower_components/animate.css/animate.css',
        'bower_components/font-awesome/css/font-awesome.min.css',
        'bower_components/simple-line-icons/css/simple-line-icons.css',
        //'bower_components/bootstrap/colorpicker.css',
        'css/font.css',
        'css/app.css',*/
    ],
    
    'js_include' => [
		'js/app.min.js',/*
        'bower_components/jquery/dist/jquery.min.js',
        'bower_components/jquery-ui/ui/jquery.ui.core.js',
        'bower_components/jquery-ui/ui/jquery.ui.widget.js',
        'bower_components/jquery-ui/ui/jquery.ui.mouse.js',
        'bower_components/jquery-ui/ui/jquery.ui.sortable.js',
        'bower_components/angular/angular.js',
        'bower_components/angular/angular_locale_es-pe.js',
        //'js/filters/angular-slugify.js',
        //'js/filters/angular-underscore.js',
        'bower_components/angular-animate/angular-animate.js',
        'bower_components/angular-cookies/angular-cookies.js',
        'bower_components/angular-resource/angular-resource.js',
        'bower_components/angular-sanitize/angular-sanitize.js',
        'bower_components/angular-touch/angular-touch.js',
        'bower_components/angular-ui-router/release/angular-ui-router.js',
        'bower_components/angular-ui-utils/ui-utils.js',
        'bower_components/ngstorage/ngStorage.js',
        'bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
        //'bower_components/angular-ui-ace/ace.js',
        //'bower_components/angular-ui-ace/ui-ace.js',
        'bower_components/oclazyload/dist/ocLazyLoad.js',
        'bower_components/angular-translate/angular-translate.js',
        'bower_components/angular-translate-loader-static-files/angular-translate-loader-static-files.js',
        'bower_components/angular-translate-storage-cookie/angular-translate-storage-cookie.js',
        'bower_components/angular-translate-storage-local/angular-translate-storage-local.js',
        //'bower_components/bootstrap/bootstrap-colorpicker-module.js',
        'js/app.js',
        'js/config.js',
        'js/config.lazyload.js',
        'js/config.router.js',
        'js/main.js',
        'js/services/ui-load.js',
        'js/filters/fromNow.js',
        'js/directives/setnganimate.js',
        'js/directives/ui-butterbar.js',
        'js/directives/ui-focus.js',
        //'js/directives/ng-context-menu.js',
        //'js/directives/ng-right-click.js',
        //'js/directives/angucomplete.js',
        //'js/directives/base64icon.js',
        'js/directives/ui-fullscreen.js',
        'js/directives/ui-jq.js',
        //'js/directives/sortable.js',
        'js/directives/ui-module.js',
        'js/directives/ui-nav.js',
        'js/directives/ui-scroll.js',
        'js/directives/ui-shift.js',
        'js/directives/ui-toggleclass.js',
        'js/controllers/bootstrap.js',*/
    ]
];
