<?php

return[
    
    'GET    /publications/cron' => 'publication/cron',
    
    'GET    /publisher' => 'publisher/query',
    'GET    /publisher/byKey' => 'publisher/query-by-key',
    'GET    /publisher/<id:\d+>/groups' => 'publisher/get-groups',
    'POST   /publisher/requestToken' => 'publisher/request-token',
    'GET   /publisher/verifyRequestToken' => 'publisher/verify-request-token',
    'POST   /publisher/updateFromFacebook' => 'publisher/update-from-facebook',
    'POST   /publisher/setTokenFacebook' => 'publisher/set-token-facebook',
    'POST   /publisher/changeGroupStatus' => 'publisher/change-group-status',
    'GET    /publisher/create' => 'publisher/create',
    'PUT    /publisher/<id:\d+>' => 'publisher/update',
    'DELETE /publisher/<id:\d+>' => 'publisher/remove',
    
    'GET    /entry/getEntries'          => 'entry/get-entries',
	'PUT    /entry/<id:\d+>'            => 'entry/update',
    'POST   /entry'                     => 'entry/create',
    'POST   /entry/loadDataFromLink'    => 'entry/load-data-from-link',
    'POST   /entry/changeStatus'        => 'entry/change-status',
    'POST   /entry/changePublishList'   => 'entry/change-publish-list',
    'POST   /entry/changeTagsGroups'    => 'entry/change-tags-groups',
    'POST   /entry/changePublishers'    => 'entry/change-publishers',
    'DELETE /entry/<id:\d+>'            => 'entry/remove',
    
    'GET  /group/getTags' => 'group/get-tags',
    'POST /group/changeStatus' => 'group/change-status',
    'POST /group/addCategory' => 'group/add-category',
    'POST /group/removeCategory' => 'group/remove-category',
    
    'GET /call-register' => 'call/call-register',
    'GET /call-back-register' => 'call/call-back-register',
    'GET /call-authorize' => 'call/call-authorize',
    'GET /call-back-authorize' => 'call/call-back-authorize',
    
	'GET /subscription/<number:\d+>' => 'subscription/view',
	
    'PUT /comunity/<id:\d+>' => 'comunity/update',
    
    'login' => 'site/login',
    'logout' => 'site/logout',
    'signup' => 'site/signup',
    'pricing' => 'site/pricing',
    'payment' => 'payment',
    'order/<number:\d+>' => 'order/view',
    'order/<number:\d+>/pay' => 'order/pay',
    'order/<number:\d+>/aprove' => 'order/aprove',
    
    'GET <controller:admin.*>' => 'site/admin',
    
];