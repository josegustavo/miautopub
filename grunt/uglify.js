module.exports = {
  web:{
    src:[
      'web/js/app.src.js'
    ],
    dest:'web/js/app.min.js'
  },
  nologin:{
    src:[
      'web/js/app.nologin.js'
    ],
    dest:'web/js/app.nologin.min.js'
  }
}
