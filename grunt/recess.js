module.exports = {
	less: {
        files: {
          'css/app.css': [
            'css/less/app.less'
          ]
        },
        options: {
          compile: true
        }
    },
    web: {
        files: {
            'web/css/app.min.css': [
                'bower_components/bootstrap/dist/css/bootstrap.css',
                'bower_components/animate.css/animate.css',
                'bower_components/font-awesome/css/font-awesome.css',
                'bower_components/simple-line-icons/css/simple-line-icons.css',
				'bower_components/angular-ui-select/dist/select.css',
				'bower_components/bootstrap-slider/bootstrap-slider.css',
				'css/*.css',
                'web/css/*.css'
            ]
        },
        options: {
            compress: true
        }
    },
}
