<?php
namespace app\helpers;

use Yii;

class Controller extends \yii\web\Controller
{
        public $config;
        
        public function behaviors()
        {
            return [
                'access' => [
                    'class' => \yii\filters\AccessControl::className(),
                    'rules' => [
                        [
                            'actions' => ['index','error','login','signup','pricing','payment','help','cron','call-register','call-back-register','call-authorize','call-back-authorize'],
                            'allow' => true,
                        ],
                        [
                            'allow' => true,
                            'roles' => ['@']
                        ]
                    ],
                ],
            ];
        }
        
        public $success=null;
        
        public $error=array();
        
        public $debug="";
        
        private $memo_ini;
        private $memo_end;
        
        protected $user_id;
        protected $comunity_id;
        
        public function comunity_id()
        {
            return $this->comunity_id;
        }

        public function beforeAction($action) 
        {
            
            if (parent::beforeAction($action))
            {
                $this->config = new Config();
                $this->memo_ini = memory_get_usage();
                
                $user = Yii::$app->user;
                if(!$user->isGuest)
                {
                    $this->user_id = $user->id;
                    $this->comunity_id = $user->identity->comunity_id;
                }
                return true;
            }
            else 
            {
                return false;
            }
        }

        public function afterAction($action, $result)
        {
            $result = parent::afterAction($action, $result);
            $response = array();
            if(isset($this->success))
            {
                $response['success'] = $this->success;
            }
            if(!empty($this->error))
            {
                $response['error'] = $this->error;
            }
            if(!empty($response))
            {
                $this->memo_end = memory_get_usage();
                if(TRUE)
                {
                    $response['status']['time'] = round((1000*(Yii::getLogger()->getElapsedTime())),2)." ms";
                    $response['status']['memo'] = round((($this->memo_end-$this->memo_ini)/1048576),2)." MB";
                    $response['status']['peak'] = round(memory_get_peak_usage()/1048576,2) . " MB";
                    $response['status']['size'] = round(strlen(json_encode($response))/1024,2). " kB";
                    $response['status']['debug'] = $this->debug;
                }
                $body = json_encode($response);
                if(isset($response['success']))
                {
                    $result = $this->sendResponse("200");
                }
                else if(!empty ($response['error']))
                {
                    $result = $this->sendResponse ("400");
                }
                echo $body;
                return;
            }
            
            return $result;
        }
        
        /**
         * Send raw HTTP response
         * @param int $status HTTP status code
         * @param string $body The body of the HTTP response
         * @param string $contentType Header content-type
         * @return HTTP response
         */
        protected function sendResponse($status = 200, $body = '', $contentType = 'application/json')
        {
            Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
            // Set the status
            //$statusHeader = 'HTTP/1.1 ' . $status . ' ' . $this->getStatusCodeMessage($status);
            Yii::$app->response->headers->set('content-type', $contentType.'; charset=UTF-8');
            Yii::$app->response->statusCode = $status;//Cache
            // Set the content type
            
            
            Yii::$app->response->send();
        }

        /**
         * Return the http status message based on integer status code
         * @param int $status HTTP status code
         * @return string status message
         */
        protected function getStatusCodeMessage($status)
        {
            $codes = array(
                        100 => 'Continue',
                        101 => 'Switching Protocols',
                        200 => 'OK',
                        201 => 'Created',
                        202 => 'Accepted',
                        203 => 'Non-Authoritative Information',
                        204 => 'No Content',
                        205 => 'Reset Content',
                        206 => 'Partial Content',
                        300 => 'Multiple Choices',
                        301 => 'Moved Permanently',
                        302 => 'Found',
                        303 => 'See Other',
                        304 => 'Not Modified',
                        305 => 'Use Proxy',
                        306 => '(Unused)',
                        307 => 'Temporary Redirect',
                        400 => 'Bad Request',
                        401 => 'Unauthorized',
                        402 => 'Payment Required',
                        403 => 'Forbidden',
                        404 => 'Not Found',
                        405 => 'Method Not Allowed',
                        406 => 'Not Acceptable',
                        407 => 'Proxy Authentication Required',
                        408 => 'Request Timeout',
                        409 => 'Conflict',
                        410 => 'Gone',
                        411 => 'Length Required',
                        412 => 'Precondition Failed',
                        413 => 'Request Entity Too Large',
                        414 => 'Request-URI Too Long',
                        415 => 'Unsupported Media Type',
                        416 => 'Requested Range Not Satisfiable',
                        417 => 'Expectation Failed',
                        500 => 'Internal Server Error',
                        501 => 'Not Implemented',
                        502 => 'Bad Gateway',
                        503 => 'Service Unavailable',
                        504 => 'Gateway Timeout',
                        505 => 'HTTP Version Not Supported',

            );
            return (isset($codes[$status])) ? $codes[$status] : '';
        }

        /**
         * Gets RestFul data and decodes its JSON request
         * @return mixed
         */
        public function getJsonInput()
        {
                return CJSON::decode(file_get_contents('php://input'));
        }
        /*
        public function init() {
            parent::init();
            Yii::$app->attachEventHandler('onError',array($this, 'handleApiError'));
            Yii::$app->attachEventHandler('onException',array($this, 'handleApiError'));
        }
        */    
        
        public function handleApiError(CEvent $event)
        {
            $event->handled = true;
            echo CJSON::encode(array('error'=>$event->exception->getMessage()));
            
        }
        
        
}