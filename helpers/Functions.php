<?php
namespace app\helpers;


class Functions
{
    public static function parse_signed_request($signed_request)
    {
        list($encoded_sig, $payload) = explode('.', $signed_request, 2); 
        $secret = \Yii::$app->params['FbLoginAppSecret'];// Use your app secret here
        // decode the data
        $sig = base64_decode(strtr($encoded_sig, '-_', '+/'));
        $data = json_decode(base64_decode(strtr($payload, '-_', '+/')), true);

        // confirm the signature
        $expected_sig = hash_hmac('sha256', $payload, $secret, $raw = true);
        
        if ($sig !== $expected_sig) return;
        return $data;
    }
    
    public static function getStatusOrderName($status)
    {
        if($status == 'pending')
        {
            return "Pendiente";
        }
        else if($status == 'canceled')
        {
            return "Cancelado";
        }
        else if($status == 'validating')
        {
            return "Validando";
        }
        else if($status == 'paid')
        {
            return "Pagado";
        }
        else
        {
            return $status;
        }
    }

    public static function file_get_contents($url)
    {
        $return = NULL;
        try 
        {
            if(ini_get('allow_url_fopen') == true)
            {
                $return = file_get_contents($url);
            }
            else
            {
                $curl_handle=curl_init();
                curl_setopt($curl_handle,CURLOPT_URL,$url);
                curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,2);
                curl_setopt($curl_handle,CURLOPT_RETURNTRANSFER,1);
                $return = curl_exec($curl_handle);
                curl_close($curl_handle);
            }
        } 
        catch (\Exception $ex) {
            //echo ($ex->getMessage());
        }
        return $return;
    }
    
    static function getDateTimeNow($timezone = NULL)
    {
        if($timezone === NULL)
        {
            $timezone = \Yii::$app->params['timezone'];
        }
        return \Carbon\Carbon::now($timezone);
    }
    
    static function getDateTime($timestamp, $timezone = NULL)
    {
        if($timezone === NULL)
        {
            $timezone = \Yii::$app->params['timezone'];
        }
        return \Carbon\Carbon::createFromTimestamp($timestamp, $timezone)->format('c');
    }
    
    static function getTimestampFromDate($date_iso8601)
    {
        
        $datetime = \DateTime::createFromFormat(\DateTime::ISO8601, $date_iso8601);
        if($datetime)
        {
            return $datetime->getTimestamp();
        }
        else
        {
            return time();
        }
    }
    
    static function list_pluck( $list, $field ) {
        $newlist = [];
        foreach ( $list as $key => $value ) {
                if ( is_object( $value ) && isset($value->$field) )
                        $newlist[ $key ] = $value->$field;
                else if(array_key_exists($field, $value))
                        $newlist[ $key ] = $value[ $field ];
        }

        return $newlist;
    }
    
    
    static function getFingerPrint()
    {
        $string = "";
        $args = func_get_args();
        foreach ($args as $arg)
        {
            
            $string .= serialize($arg);
        }
            
        return md5($string);
    }
    
    static function isEqualFinger($a, $b)
    {
        return self::getFingerPrint($a) === self::getFingerPrint($b);
    }
    
    /* Call this function to create a slug from $string */
    /* Only for projects */
    static function create_slug($string){
        $string = trim(str_replace("(Copia)", "", $string));        
        $string = self::remove_accents($string);
        $string = self::symbols_to_words($string);
        $string = strtolower($string); // Force lowercase
        $space_chars = array(
         " ", // space
         "…", // ellipsis
         "–", // en dash
         "—", // em dash
         "/", // slash
         "\\", // backslash
         ":", // colon
         ";", // semi-colon
         ".", // period
         "+", // plus sign
         "#", // pound sign
         "~", // tilde
         "_", // underscore
         "|", // pipe
        );
        foreach($space_chars as $char){
         $string = str_replace($char, '-', $string); // Change spaces to dashes
        }
        // Only allow letters, numbers, and dashes
        $string = preg_replace('/([^a-zA-Z0-9\-]+)/', '', $string);
        $string = preg_replace('/-+/', '-', $string); // Clean up extra dashes
        if(substr($string, -1)==='-'){ // Remove - from end
         $string = substr($string, 0, -1);
        }
        if(substr($string, 0, 1)==='-'){ // Remove - from start
         $string = substr($string, 1);
        }
        
        return $string;
    }
    
    static function seems_utf8($str) {
	$length = strlen($str);
	for ($i=0; $i < $length; $i++) {
		$c = ord($str[$i]);
		if ($c < 0x80) $n = 0; # 0bbbbbbb
		elseif (($c & 0xE0) == 0xC0) $n=1; # 110bbbbb
		elseif (($c & 0xF0) == 0xE0) $n=2; # 1110bbbb
		elseif (($c & 0xF8) == 0xF0) $n=3; # 11110bbb
		elseif (($c & 0xFC) == 0xF8) $n=4; # 111110bb
		elseif (($c & 0xFE) == 0xFC) $n=5; # 1111110b
		else return false; # Does not match any model
		for ($j=0; $j<$n; $j++) { # n bytes matching 10bbbbbb follow ?
			if ((++$i == $length) || ((ord($str[$i]) & 0xC0) != 0x80))
				return false;
		}
	}
	return true;
    }

    /**
    * @param string $string Text that might have accent characters
    * @return string Filtered string with replaced "nice" characters.
    */
    static function remove_accents($string) {
        if(!preg_match('/[\x80-\xff]/', $string)){
            return $string;
        }
        if(Functions::seems_utf8($string)){
            $chars = array(
            // Decompositions for Latin-1 Supplement
            chr(195).chr(128) => 'A', chr(195).chr(129) => 'A',
            chr(195).chr(130) => 'A', chr(195).chr(131) => 'A',
            chr(195).chr(132) => 'A', chr(195).chr(133) => 'A',
            chr(195).chr(135) => 'C', chr(195).chr(136) => 'E',
            chr(195).chr(137) => 'E', chr(195).chr(138) => 'E',
            chr(195).chr(139) => 'E', chr(195).chr(140) => 'I',
            chr(195).chr(141) => 'I', chr(195).chr(142) => 'I',
            chr(195).chr(143) => 'I', chr(195).chr(145) => 'N',
            chr(195).chr(146) => 'O', chr(195).chr(147) => 'O',
            chr(195).chr(148) => 'O', chr(195).chr(149) => 'O',
            chr(195).chr(150) => 'O', chr(195).chr(153) => 'U',
            chr(195).chr(154) => 'U', chr(195).chr(155) => 'U',
            chr(195).chr(156) => 'U', chr(195).chr(157) => 'Y',
            chr(195).chr(159) => 's', chr(195).chr(160) => 'a',
            chr(195).chr(161) => 'a', chr(195).chr(162) => 'a',
            chr(195).chr(163) => 'a', chr(195).chr(164) => 'a',
            chr(195).chr(165) => 'a', chr(195).chr(167) => 'c',
            chr(195).chr(168) => 'e', chr(195).chr(169) => 'e',
            chr(195).chr(170) => 'e', chr(195).chr(171) => 'e',
            chr(195).chr(172) => 'i', chr(195).chr(173) => 'i',
            chr(195).chr(174) => 'i', chr(195).chr(175) => 'i',
            chr(195).chr(177) => 'n', chr(195).chr(178) => 'o',
            chr(195).chr(179) => 'o', chr(195).chr(180) => 'o',
            chr(195).chr(181) => 'o', chr(195).chr(182) => 'o',
            chr(195).chr(182) => 'o', chr(195).chr(185) => 'u',
            chr(195).chr(186) => 'u', chr(195).chr(187) => 'u',
            chr(195).chr(188) => 'u', chr(195).chr(189) => 'y',
            chr(195).chr(191) => 'y',
            // Decompositions for Latin Extended-A
            chr(196).chr(128) => 'A', chr(196).chr(129) => 'a',
            chr(196).chr(130) => 'A', chr(196).chr(131) => 'a',
            chr(196).chr(132) => 'A', chr(196).chr(133) => 'a',
            chr(196).chr(134) => 'C', chr(196).chr(135) => 'c',
            chr(196).chr(136) => 'C', chr(196).chr(137) => 'c',
            chr(196).chr(138) => 'C', chr(196).chr(139) => 'c',
            chr(196).chr(140) => 'C', chr(196).chr(141) => 'c',
            chr(196).chr(142) => 'D', chr(196).chr(143) => 'd',
            chr(196).chr(144) => 'D', chr(196).chr(145) => 'd',
            chr(196).chr(146) => 'E', chr(196).chr(147) => 'e',
            chr(196).chr(148) => 'E', chr(196).chr(149) => 'e',
            chr(196).chr(150) => 'E', chr(196).chr(151) => 'e',
            chr(196).chr(152) => 'E', chr(196).chr(153) => 'e',
            chr(196).chr(154) => 'E', chr(196).chr(155) => 'e',
            chr(196).chr(156) => 'G', chr(196).chr(157) => 'g',
            chr(196).chr(158) => 'G', chr(196).chr(159) => 'g',
            chr(196).chr(160) => 'G', chr(196).chr(161) => 'g',
            chr(196).chr(162) => 'G', chr(196).chr(163) => 'g',
            chr(196).chr(164) => 'H', chr(196).chr(165) => 'h',
            chr(196).chr(166) => 'H', chr(196).chr(167) => 'h',
            chr(196).chr(168) => 'I', chr(196).chr(169) => 'i',
            chr(196).chr(170) => 'I', chr(196).chr(171) => 'i',
            chr(196).chr(172) => 'I', chr(196).chr(173) => 'i',
            chr(196).chr(174) => 'I', chr(196).chr(175) => 'i',
            chr(196).chr(176) => 'I', chr(196).chr(177) => 'i',
            chr(196).chr(178) => 'IJ',chr(196).chr(179) => 'ij',
            chr(196).chr(180) => 'J', chr(196).chr(181) => 'j',
            chr(196).chr(182) => 'K', chr(196).chr(183) => 'k',
            chr(196).chr(184) => 'k', chr(196).chr(185) => 'L',
            chr(196).chr(186) => 'l', chr(196).chr(187) => 'L',
            chr(196).chr(188) => 'l', chr(196).chr(189) => 'L',
            chr(196).chr(190) => 'l', chr(196).chr(191) => 'L',
            chr(197).chr(128) => 'l', chr(197).chr(129) => 'L',
            chr(197).chr(130) => 'l', chr(197).chr(131) => 'N',
            chr(197).chr(132) => 'n', chr(197).chr(133) => 'N',
            chr(197).chr(134) => 'n', chr(197).chr(135) => 'N',
            chr(197).chr(136) => 'n', chr(197).chr(137) => 'N',
            chr(197).chr(138) => 'n', chr(197).chr(139) => 'N',
            chr(197).chr(140) => 'O', chr(197).chr(141) => 'o',
            chr(197).chr(142) => 'O', chr(197).chr(143) => 'o',
            chr(197).chr(144) => 'O', chr(197).chr(145) => 'o',
            chr(197).chr(146) => 'OE',chr(197).chr(147) => 'oe',
            chr(197).chr(148) => 'R',chr(197).chr(149) => 'r',
            chr(197).chr(150) => 'R',chr(197).chr(151) => 'r',
            chr(197).chr(152) => 'R',chr(197).chr(153) => 'r',
            chr(197).chr(154) => 'S',chr(197).chr(155) => 's',
            chr(197).chr(156) => 'S',chr(197).chr(157) => 's',
            chr(197).chr(158) => 'S',chr(197).chr(159) => 's',
            chr(197).chr(160) => 'S', chr(197).chr(161) => 's',
            chr(197).chr(162) => 'T', chr(197).chr(163) => 't',
            chr(197).chr(164) => 'T', chr(197).chr(165) => 't',
            chr(197).chr(166) => 'T', chr(197).chr(167) => 't',
            chr(197).chr(168) => 'U', chr(197).chr(169) => 'u',
            chr(197).chr(170) => 'U', chr(197).chr(171) => 'u',
            chr(197).chr(172) => 'U', chr(197).chr(173) => 'u',
            chr(197).chr(174) => 'U', chr(197).chr(175) => 'u',
            chr(197).chr(176) => 'U', chr(197).chr(177) => 'u',
            chr(197).chr(178) => 'U', chr(197).chr(179) => 'u',
            chr(197).chr(180) => 'W', chr(197).chr(181) => 'w',
            chr(197).chr(182) => 'Y', chr(197).chr(183) => 'y',
            chr(197).chr(184) => 'Y', chr(197).chr(185) => 'Z',
            chr(197).chr(186) => 'z', chr(197).chr(187) => 'Z',
            chr(197).chr(188) => 'z', chr(197).chr(189) => 'Z',
            chr(197).chr(190) => 'z', chr(197).chr(191) => 's',
            // Euro Sign
            chr(226).chr(130).chr(172) => 'E',
            // GBP (Pound) Sign
            chr(194).chr(163) => '');
            $string = strtr($string, $chars);
        } else {
            // Assume ISO-8859-1 if not UTF-8
            $chars['in'] = chr(128).chr(131).chr(138).chr(142).chr(154).chr(158)
             .chr(159).chr(162).chr(165).chr(181).chr(192).chr(193).chr(194)
             .chr(195).chr(196).chr(197).chr(199).chr(200).chr(201).chr(202)
             .chr(203).chr(204).chr(205).chr(206).chr(207).chr(209).chr(210)
             .chr(211).chr(212).chr(213).chr(214).chr(216).chr(217).chr(218)
             .chr(219).chr(220).chr(221).chr(224).chr(225).chr(226).chr(227)
             .chr(228).chr(229).chr(231).chr(232).chr(233).chr(234).chr(235)
             .chr(236).chr(237).chr(238).chr(239).chr(241).chr(242).chr(243)
             .chr(244).chr(245).chr(246).chr(248).chr(249).chr(250).chr(251)
             .chr(252).chr(253).chr(255);
            $chars['out'] = "EfSZszYcYuAAAAAACEEEEIIIINOOOOOOUUUUYaaaaaaceeeeiiiinoooooouuuuyy";
            $string = strtr($string, $chars['in'], $chars['out']);
            $double_chars['in'] = array(chr(140), chr(156), chr(198), chr(208), chr(222), chr(223), chr(230), chr(240), chr(254));
            $double_chars['out'] = array('OE', 'oe', 'AE', 'DH', 'TH', 'ss', 'ae', 'dh', 'th');
            $string = str_replace($double_chars['in'], $double_chars['out'], $string);
        }
        return $string;
    }

    static function symbols_to_words($output){
     $output = str_replace('@', ' at ', $output);
     $output = str_replace('%', ' percent ', $output);
     $output = str_replace('&', ' and ', $output);
     return $output;
    }
    
    static function is_serialized( $data, $strict = true ) {
	// if it isn't a string, it isn't serialized.
	if ( ! is_string( $data ) ) {
		return false;
	}
	$data = trim( $data );
 	if ( 'N;' == $data ) {
		return true;
	}
	if ( strlen( $data ) < 4 ) {
		return false;
	}
	if ( ':' !== $data[1] ) {
		return false;
	}
	if ( $strict ) {
		$lastc = substr( $data, -1 );
		if ( ';' !== $lastc && '}' !== $lastc ) {
			return false;
		}
	} else {
		$semicolon = strpos( $data, ';' );
		$brace     = strpos( $data, '}' );
		// Either ; or } must exist.
		if ( false === $semicolon && false === $brace )
			return false;
		// But neither must be in the first X characters.
		if ( false !== $semicolon && $semicolon < 3 )
			return false;
		if ( false !== $brace && $brace < 4 )
			return false;
	}
	$token = $data[0];
	switch ( $token ) {
		case 's' :
			if ( $strict ) {
				if ( '"' !== substr( $data, -2, 1 ) ) {
					return false;
				}
			} elseif ( false === strpos( $data, '"' ) ) {
				return false;
			}
			// or else fall through
		case 'a' :
		case 'O' :
			return (bool) preg_match( "/^{$token}:[0-9]+:/s", $data );
		case 'b' :
		case 'i' :
		case 'd' :
			$end = $strict ? '$' : '';
			return (bool) preg_match( "/^{$token}:[0-9.E-]+;$end/", $data );
	}
	return false;
}
    public static function human_time_diff( $from, $to = '' ) 
    {
        if( empty($from) )
        {
            return NULL;
        }
            defined('MINUTE_IN_SECONDS') || define( 'MINUTE_IN_SECONDS', 60 );
            defined('HOUR_IN_SECONDS') || define( 'HOUR_IN_SECONDS',   60 * MINUTE_IN_SECONDS );
            defined('DAY_IN_SECONDS') || define( 'DAY_IN_SECONDS',    24 * HOUR_IN_SECONDS   );
            defined('WEEK_IN_SECONDS') || define( 'WEEK_IN_SECONDS',    7 * DAY_IN_SECONDS    );
            defined('YEAR_IN_SECONDS') || define( 'YEAR_IN_SECONDS',  365 * DAY_IN_SECONDS    );
            
          if ( empty( $to ) )
                  $to = time();

          $diff = (int) abs( $to - $from );

          if ( $diff < HOUR_IN_SECONDS ) {
                  $mins = round( $diff / MINUTE_IN_SECONDS );
                  if ( $mins <= 1 )
                          $mins = 1;
                  /* translators: min=minute */
                  $since = sprintf( $mins==1?'%s minuto':'%s minutos', $mins );
          } elseif ( $diff < DAY_IN_SECONDS && $diff >= HOUR_IN_SECONDS ) {
                  $hours = round( $diff / HOUR_IN_SECONDS );
                  if ( $hours <= 1 )
                          $hours = 1;
                  $since = sprintf( $hours==1?'%s hora':'%s horas', $hours );
          } elseif ( $diff < WEEK_IN_SECONDS && $diff >= DAY_IN_SECONDS ) {
                  $days = round( $diff / DAY_IN_SECONDS );
                  if ( $days <= 1 )
                          $days = 1;
                  $since = sprintf( $days==1?'%s día':'%s días', $days );
          } elseif ( $diff < 30 * DAY_IN_SECONDS && $diff >= WEEK_IN_SECONDS ) {
                  $weeks = round( $diff / WEEK_IN_SECONDS );
                  if ( $weeks <= 1 )
                          $weeks = 1;
                  $since = sprintf($weeks==1?'%s semana':'%s semanas', $weeks );
          } elseif ( $diff < YEAR_IN_SECONDS && $diff >= 30 * DAY_IN_SECONDS ) {
                  $months = round( $diff / ( 30 * DAY_IN_SECONDS ) );
                  if ( $months <= 1 )
                          $months = 1;
                  $since = sprintf( $months==1?'%s mes':'%s meses', $months );
          } elseif ( $diff >= YEAR_IN_SECONDS ) {
                  $years = round( $diff / YEAR_IN_SECONDS );
                  if ( $years <= 1 )
                          $years = 1;
                  $since = sprintf($years==1?'%s año':'%s años', $years );
          }

          return $since;
    }
    
    static function getState($start_date="", $end_date="")
    {
        $result = [];
        $now = time();
        
        if(!$start_date || $start_date=="")
        {
            $result['state'] = -2;
            $result['state_ago'] = 'Nunca comienza';
            $result['state_date'] = 'No se definió la fecha de inicio';
        }
        else if( $now < $start_date )
        {
            $result['state'] = 0;
            $result['state_ago'] = 'Inicia en ' . self::human_time_diff($now,$start_date);
            $result['state_date'] = date( 'd/m/Y h:i:sa', $start_date);
        }
        else if(!$end_date  || $end_date=="")
        {
            $result['state'] = 1;
            $result['state_ago'] = 'Inició hace ' . self::human_time_diff($start_date,$now);
            $result['state_date'] = date( 'd/m/Y h:i:sa', $start_date) . " (No se definió fecha de fin)";
        }
        else if( $now < $end_date )
        {
            $result['state'] = 1;
            $result['state_ago'] = 'Termina en ' . self::human_time_diff($now,$end_date);
            $result['state_date'] = date( 'd/m/Y h:i:sa', $end_date);
        }
        else if( $now >= $end_date )
        {
            $result['state'] = -1;
            $result['state_ago'] = 'Terminó hace ' . self::human_time_diff($end_date,$now);
            $result['state_date'] = date( 'd/m/Y h:i:sa', $end_date);
        }
        
        return $result;
    }
    
    static function is_running($start_date="", $end_date="")
    {
        $state = self::getState($start_date, $end_date);
        $val = $state['state'];
        return ($val === 1);
    }
}