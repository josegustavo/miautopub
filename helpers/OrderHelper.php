<?php
namespace app\helpers;

use Yii;
use app\models\Order;
use app\models\Comunity;
use app\models\ComunityMeta;

class OrderHelper
{
    public static function getOrder($id)
    {
			  $query = Order::find()->with('comunity')->where(['id' => $id]);
        $order = $query
                ->asArray()->one();
        if(!$order) return;
        
        $isAdmin = PublisherHelper::isAdmin();
        
        if( ($order['comunity_id'] == PublisherHelper::getComunityId()) || $isAdmin)
        {
            $order['plan'] = @unserialize($order['plan']);
            $order['meta'] = @unserialize($order['meta']);
            return $order;
        }
        else
        {
            throw new \Exception("No tiene permisos para ver esta orden", 1300 );
        }
    }
    
    public static function getOrders($options = [])
    {
        $isAdmin = PublisherHelper::isAdmin();
        
        $where = [];
        if(!$isAdmin) $where['comunity_id'] = PublisherHelper::getComunityId();
        
        if(array_key_exists('onlyPending',$options) && $options['onlyPending'])
        {
            $where['order.status'] = 'pending';
        }
			
			  if(array_key_exists('filter_status',$options) && $options['filter_status'])
        {
            $where['order.status'] = $options['filter_status'];
        }
        
        $query = Order::find()->where($where)
                ->orderBy('created_time DESC')
                ->asArray();
        
        $notIncludeComunity = array_key_exists('notIncludeComunity',$options) && $options['notIncludeComunity'];
        if(!$notIncludeComunity)
            $query->joinWith(['comunity']);
        
        $orders_db = $query->all();
        
        $orders = [];
        
        foreach ($orders_db as $order)
        {
            $order['plan'] = @unserialize($order['plan']);
            $order['meta'] = @unserialize($order['meta']);
            
						if(array_key_exists('above_zero',$options) && $options['above_zero'])
						{
								if($order['plan']['total'] > 0)
									$orders []= $order;
						}
						else
						{
							$orders []= $order;
						}
        }
        return $orders;
    }
    
    public static function comunityHasPendingOrder($comunity_id)
    {
        return Order::find()->where(['comunity_id' => $comunity_id, 'status' => 'pending'])->count() > 0;
    }


    public static function createOrder($plan, $comunity_id = NULL)
    {
        $time = time();
        
        if(!$comunity_id) $comunity_id = PublisherHelper::getComunityId();
        
        $order = new Order();
        
        $order->id = self::generateUniqueOrderId();
        $order->comunity_id = $comunity_id;
        $order->created_time = $time;
        $order->plan = serialize($plan);
        
        if($plan['plan'] == 'free')
        {
            $order->status = 'paid';
        }
        
        $order->save();
        
        return $order;
    }
    
    public static function cancelOrder($id)
    {
        $order = Order::findOne($id);
        
        if($order->status == 'pending')
        {
            $order->status = 'canceled';
            $order->save();
        }
        else
        {
            throw new \yii\base\Exception('Esta orden no puede ser cancelada');
        }
        
        return $order;
        
    }
    
    public static function validateOrder($id, $numOper)
    {
        $order = Order::findOne($id);
        $meta = unserialize($order['meta']);
        if(!$meta) $meta = [];
        
        $meta['numberOperation'] = $numOper;
        
        $order->meta = serialize($meta);
        $order->status = 'validating';
        $order->save();
        
        return $order;
    }

    public static function aproveOrder($id)
    {
        $order = Order::findOne($id);
        $meta = unserialize($order['meta']);
        if(!$meta) $meta = [];
        
        if($order->status == 'validating')
        {
            $order->status = 'paid';
            if($subscription = SubscriptionHelper::createSubscription($order))
            {
                $meta['aprove_date'] = $subscription['created_time'];
                $order->meta = serialize($meta);
                $order->subscription_id = $subscription['id'];
                $order->save();
            }
        }
		else if($order->status == 'pending')
		{
			$order->status = 'paid';
            if($subscription = SubscriptionHelper::createSubscription($order))
            {
                $meta['aprove_date'] = $subscription['created_time'];
                $order->meta = serialize($meta);
                $order->subscription_id = $subscription['id'];
                $order->save();
            }
		}
		
		
        
        return $order;
    }
    
    public static function validatePaypal($data)
    {
        if(!isset($data['txn_id'])) return;
        $txn = $data['txn_id'];
        
        $paypal_cfg = \Yii::$app->params['paypal'];
        
        $pdt_data = self::paypal_PDT_request($txn, $paypal_cfg['pdt_identity_token']);
        
        if($pdt_data && isset($pdt_data['item_number']))
        {
            $order_id = $pdt_data['item_number'];
            
            if($pdt_data['payment_status'] == 'Completed')
            {
                $order = Order::findOne($order_id);
                if($order->status == 'pending')
                {
                    $meta = unserialize($order['meta']);if(!$meta) $meta = [];
                    $meta['aprove_date'] = time();
                    $meta['numberOperation'] = $txn;
                    $meta['paypal_post_data'] = $data;
                    $meta['paypal_pdt_data'] = $pdt_data;
                    $order->meta = serialize($meta);
                    $order->status = 'paid';
                    
                    $order->save();
                    
                    if($subscription = SubscriptionHelper::createSubscription($order))
                    {
                        $order->subscription_id = $subscription['id'];
                        $order->save();
                    }

                    return $order;
                }
            }
        }
    }

    private static function paypal_PDT_request($tx, $pdt_identity_token) 
    {
        $request = curl_init();

        $paypal_cfg = \Yii::$app->params['paypal'];
        
        // Set request options
        curl_setopt_array($request, array
            (
              CURLOPT_URL => $paypal_cfg['url'],
              CURLOPT_POST => TRUE,
              CURLOPT_POSTFIELDS => http_build_query(array
                  (
                    'cmd' => '_notify-synch',
                    'tx' => $tx,
                    'at' => $pdt_identity_token,
                  )
              ),
              CURLOPT_RETURNTRANSFER => TRUE,
              CURLOPT_HEADER => FALSE,
              CURLOPT_SSL_VERIFYPEER => FALSE,
              
            )
        );

        $response = curl_exec($request);
        $status   = curl_getinfo($request, CURLINFO_HTTP_CODE);

        // Cerrar la conexión
        curl_close($request);
        
        $lines = explode("\n", $response);
        $keyarray = array();
        
        if (strcmp ($lines[0], "SUCCESS") == 0) 
        {
            for ($i=1; $i<count($lines);$i++)
            {
                @list($key,$val) = explode("=", $lines[$i]);
                $keyarray[urldecode($key)] = urldecode($val);
            }
            return $keyarray;
        }
        else if (strcmp ($lines[0], "FAIL") == 0) {
            // log for manual investigation
        }
        
        return NULL;
    }

    private static function generateUniqueOrderId()
    {
        $new_id = 0;
        do 
        {
            $new_id = mt_rand(1000000000,9999999999);
            
        } 
        while(Order::find()->where(['id' => $new_id])->count() > 0);
        
        return $new_id;
        
    }
    
}