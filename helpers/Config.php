<?php
namespace app\helpers;

use Yii;


class Config extends \yii\base\Object
{
    private static $dbConfig = [];
    private static $config;
    
    private static function getDbConfig($key)
    {
        if(empty(self::$dbConfig))
        {
            self::$dbConfig = \app\models\Config::find()->asArray()->all();
            if(is_array(self::$dbConfig) && !empty(self::$dbConfig))
            {
                $result = [];
                foreach (self::$dbConfig as $c) { $result[$c['key']] = $c['value']; }
                self::$dbConfig = $result;
            }
        }
        if(array_key_exists($key, self::$dbConfig))
        {
            $resp = self::$dbConfig[$key];
            return Functions::is_serialized($resp)?unserialize($resp):$resp;
        }
        return NULL;
    }
    
    private static function setDbConfig($key, $value)
    {
        if(!is_string($value) || !is_numeric($value))
        {
            $value = serialize($value);
        }
        if( is_null(self::getDbConfig($key)) )
        {
            (new \app\models\Config(['key' => $key, 'value' => $value]))->save();
        }
        else
        {
            $conf = \app\models\Config::findOne($key);
            $conf->value = $value;
            $conf->save();
        }
        
    }

    public function __get($name)
    {
        return $this->get($name);
    }
    
    public function __set($name, $value)
    {
        return $this->set($name, $value);
    }
    
    //getUriCDN()//$request = Yii::$app->request; $value = $request->hostInfo . $request->baseUrl;
    
    public function get($key, $default = NULL)
    {
        
        $value_cache = $this->getFromCache($key);
        
        if(!$value_cache)
        {
            $value = self::getDbConfig($key);
            if( !is_null($value) )
            {
                $this->saveToCache($key, $value);
            }
            else if( isset(Yii::$app->params[$key]) )
            {
                $value = Yii::$app->params[$key];
                $this->saveToCache($key, $value);
            }
            else
            {
                $value = $default;
            }
            
            return $value; 
        }
        else
        {
            return $value_cache;
        }
    }
    
    public static function __callStatic($method, $args)
    {
        if (preg_match('/^([gs]et)([A-Z])(.*)$/', $method, $match)) 
        {
            $property = strtolower($match[2]). $match[3];
            if(!self::$config) self::$config = new Config();
            switch($match[1])
            {
                case 'get': return self::$config->get($property, isset($args[0])?$args[0]:NULL);
                case 'set': return self::$config->set($property, $args[0]);
            }
            
          }
    }
    
    public function set($key, $value)
    {
        self::setDbConfig($key, $value);
        Yii::$app->cache->set($key, $value);
    }
    
    private function getFromCache($key)
    {
        return  Yii::$app->cache->get($key);
    }
    
    public function saveToCache($key, $value)
    {
        Yii::$app->cache->set($key, $value);
    }
    
    public function deleteFronCache($key)
    {
        Yii::$app->cache->delete($key);
    }
    
    public function deleteAllConfigCache()
    {
        Yii::$app->cache->flush();
    }
    
    public static function inst()
    {
        
    }
}