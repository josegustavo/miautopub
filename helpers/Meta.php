<?php
namespace app\helpers;

Class Meta
{
    private $model;
    private $id;
    
    public function __construct($config = [])
    {
        
        if (!empty($config)) {
            foreach ($config as $name => $value) {
                $this->$name = $value;
            }
        }
    }
    
    public function __get($name)
    {
        return $this->_get($name);
    }
    
    public function _get($name, $lastOne = TRUE)
    {
        $class = "app\\models\\".ucfirst($this->model)."Meta";
        $table = strtolower($this->model);
        $db = $class::find()
                ->where(['key' => $name, $table.'_id' => $this->id])
                ->orderBy('time DESC')
                ->select(['key','value','time'])->asArray();
        
        $result = ($lastOne)?$db->one():$db->all();
        
        if($result)
        {
            if($lastOne && isset($result['value']) && $resp = $result['value'])
            {
                if(Functions::is_serialized($resp)) $resp = unserialize($resp);
                return $resp;
            }
            else if(!$lastOne)
            {
                $return = [];
                foreach ($result as $item)
                {
                    $return[$item['time']] = $item['value'];
                }
                return $return;
            }
        }
    }
    
    public function get($keys=[], $lastOne = TRUE)
    {
        $class = "app\\models\\".ucfirst($this->model)."Meta";
        $table = strtolower($this->model);
        $db = NULL;
        if(!$keys || $keys == 'all')
        {
            $db = $class::find()->where([$table.'_id' => $this->id])->select(['key','value','time'])->asArray()->all();
        }
        else
        {
            $db = $class::find()->where(['key' => $keys, $table.'_id' => $this->id])->select(['key','value','time'])->asArray()->all();
        }
        $result = [];
        foreach ($db as $v)
        {
            $k = $v['key'];
            if($lastOne)
            {
                if(!isset($result[$k]))
                {
                    $result[$k] = Functions::is_serialized($v['value'])?unserialize($v['value']):$v['value'];
                }
            }
            else
            {
                if(!isset($result[$k])) $result[$k] = [];
                $result[$k][$v['time']] = Functions::is_serialized($v['value'])?unserialize($v['value']):$v['value'];
            }
            
        }
        return $result;
    }
    
    public function __set($key, $value)
    {
        return $this->_set($key, $value);
    }
    
    public function _set($key, $value, $update = TRUE)
    {
        $class = "app\\models\\".ucfirst($this->model)."Meta";
        $table = strtolower($this->model);
        
        $value_ser = $value;
        if(!is_string($value) && !is_numeric($value))
        {
            $value_ser = serialize($value);
        }
        
        $add = true;
        if($update)
        {
            $exist = $class::find()
                ->where(['key' => $key, $table.'_id' => $this->id])
                ->orderBy('time DESC')
                ->asArray()
                ->one();
            if($exist)
            {
                $meta_id = $exist['id'];
                $class::updateAll(['value' => $value_ser], ['id' => $meta_id]);
                $add = FALSE;
            }
        }
        
        if($add)
        {
            $db = new $class;
            $db->key = $key;
            $db->{$table.'_id'} = $this->id;
            $db->value = $value_ser."";
            $db->time = time();
            $db->save();
        }
        
        return $value;
    }


    public function set($values = [], $update = TRUE)
    {
        foreach ($values as $key => $value)
        {
            $this->_set($key, $value, $update);
        }
        return $values;
    }


    public static function __callStatic($method, $args)
    {
        if (preg_match('/^([a-z_A-Z]+)$/', $method, $match)) 
        {
            
            $model = $match[1];
            return new Meta(['model' => $model, 'id' => $args[0]]);
        }
    }
    
}
