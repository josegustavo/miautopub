<?php
namespace app\helpers;

use Yii;
use app\models\Entry;
use app\models\Publication;

class EntryHelper
{
    private static $access_token;
    
    public static $orderByOptions = [
        'recientes' => 'created_time', 'actualizados' => 'modified_time',
        'compartidos' => 'og_share_count', 'comentados' => 'og_comment_count',
        'publicados' => 'publication_count'
    ];
    
    public static function getOrderByOptions($include_columns = FALSE)
    {
        $superlatives = ['Más' => 'DESC', 'Menos' => 'ASC'];
        $result = [];
        
        foreach (self::$orderByOptions as $k_order => $order)
        {
            foreach ($superlatives as $k_super => $super)
            {
                if($include_columns === TRUE)
                {
                    $result[sprintf("%s %s", $k_super, $k_order)] = sprintf("%s %s", $order, $super);
                }
                else
                {
                    $result []= sprintf("%s %s", $k_super, $k_order);
                }
            }
        }
        return $result;
    }


    public static function obtainNewEntries($comunity_id)
    {
        $entries_ids = [];
        $time = time();
        
        $api_wordpress = Meta::comunity($comunity_id)->wordpress;
        
        if($api_wordpress)
        {
            set_time_limit(600);

            $last = Entry::find()
                    ->where(['not', ['post_url' => NULL]])
                    ->orderBy('created_time DESC')->one();

            $after = NULL;
            if($last)
            {
                $after = Functions::getDateTime($last->created_time+1);
            }

            $url = "https://public-api.wordpress.com/rest/v1/sites/$api_wordpress/posts/?fields=ID,global_ID,title,date,URL,categories&number=100&";
            if($after)
            {
                $url .= "after=".$after."&";
            }



            $exist = TRUE;
            $before = NULL;
            $max = 10;
            $current = 0;
            while ($exist && ($current<$max))
            {
                if($before)
                    $url_final = $url.'before='.$before;
                else
                    $url_final = $url;
                $url_final = urldecode($url_final);

                $content = json_decode(file_get_contents($url_final), true);

                if(isset($content['found']) && ($found = $content['found']) && isset($content['posts']) && ($posts = $content['posts']))
                {
                    $exist = ($found > 0) && ($found>count($posts));

                    if($found > 0 && $found>count($posts))
                    {
                        $before = $posts[count($posts)-1]['date'];
                    }

                    foreach ($posts as $post)
                    {
                        if(!Entry::findOne(['post_global_id' => $post['global_ID']]))
                        {
                            $categories = join(",",array_keys($post['categories']));
                            $date = Functions::getTimestampFromDate($post['date']);

                            $entry = new Entry([
                                        "post_title" => $post['title'],
                                        "post_url" => $post['URL'],
                                        "post_global_id" => $post['global_ID'],
                                        "post_categories" => $categories,

                                        "publish_links" => serialize([$post['URL']]),
                                        //"publish_created_time" => $time,
                                        "created_time" => $date,
                                        "modified_time" => $time,
                                    ]);

                            $entry->save();
                            $entries_ids []= $entry->id;
                        }
                    }
                }
                else
                {
                    $exist = FALSE;
                }
                $current++;
            }
        }
        return $entries_ids;
    }
    
    public static function filter_entries(array $entries)
    {
        foreach ($entries as $k => $entry)
        {
            $entries[$k] = self::filter_entry($entry);
        }
        return $entries;
    }
    
    public static function filter_entry($entry)
    {
        $result = is_object($entry)?$entry->attributes:$entry;
        
        $tz = NULL;
        //$result['created_time'] = Functions::getDateTime($result['created_time'], $tz);
        //$result['publish_created_time'] = Functions::getDateTime($result['publish_created_time'], $tz);
        //$result['modified_time'] = Functions::getDateTime($result['modified_time'], $tz);
        
        $result['can_publish'] = (bool)($result['can_publish']);
        
        $result['og_comment_count'] = (int)($result['og_comment_count']);
        
        
        
        $result['tags_groups'] = @unserialize($result['tags_groups']);
        $result['publishers'] = @unserialize($result['publishers']);
        
        $result['publish_links'] = @unserialize($result['publish_links']);
        $result['publish_messages'] = $result['publish_messages']?@unserialize($result['publish_messages']):[];
        $result['publish_pictures'] = $result['publish_pictures']?@unserialize($result['publish_pictures']):[];
        $result['publish_names'] = $result['publish_names']?@unserialize($result['publish_names']):[];
        $result['publish_captions'] = $result['publish_captions']?@unserialize($result['publish_captions']):[];
        $result['publish_descriptions'] = $result['publish_descriptions']?@unserialize($result['publish_descriptions']):[];
        
        $result['publish_count'] = (int)(Publication::find()->where(['entry_id' => $entry['id']])->count());
        
        
        //$result['publishers_count'] = (int)(Publication::find()->where(['entry_id' => $entry['id']])->count());
        
        
        if((!$result['og_id'] || !$result['og_image'])  && ($post_url = $result['post_url']))
        {
            if(($token = self::getAccessToken()))
            {
                set_time_limit(600);
                if($update = self::updateFromFacebook($result))
                {
                    $result = array_merge($result,$update);
                }
            }
        }
        return $result;
    }

    public static function updateFromFacebook($entry)
    {
        
        if($update = self::loadFromFacebook($entry['post_url']))
        {
            $update = array_intersect_key($update, array_flip(['og_id','og_image','og_share_count','og_comment_count']));
            Entry::updateAll($update, ['id' => $entry['id']]);
            return $update;
        }
        return [];
    }
    
    public static function loadFromFacebook($post_url)
    {
        if(($token = self::getAccessToken()) && $post_url)
        {
            $update = [];

            $getUrl = "https://graph.facebook.com/v2.3/?id=$post_url&access_token=$token";
            $resp = json_decode(Functions::file_get_contents($getUrl), true);
            if(isset($resp['og_object']) && ($og_object = $resp['og_object']))
            {
                if(isset($og_object['id']) && ($og_id = $og_object['id']))
                {
                    $update['og_id'] = $og_id;

                    //Obtener también la imagen
                    $getUrl_2 = "https://graph.facebook.com/$og_id?access_token=$token";
                    
                    $resp_2 = json_decode(Functions::file_get_contents($getUrl_2), true);
                    if(isset($resp_2['image']) && isset($resp_2['image'][0]) && isset($resp_2['image'][0]['url']))
                    {
                        $update['og_image'] = $resp_2['image'][0]['url'];
                    }
                }
                if(isset($og_object['title']) && ($og_title = $og_object['title']))
                {
                    $update['og_title'] = $og_title;
                }
                if(isset($og_object['description']) && ($og_description = $og_object['description']))
                {
                    $update['og_description'] = $og_description;
                }
            }
            if(isset($resp['share']) && ($share = $resp['share']))
            {
                $update['og_share_count'] = $share['share_count'];
                $update['og_comment_count'] = $share['comment_count'];
            }

            if($update)
            {
                return $update;
            }
        }
        return [];
    }

    private static function getAccessToken()
    {
        if(!self::$access_token)
        {
            
            $user_id = $user_id = Yii::$app->user->id;
            
            if($user_id)
            {
                $user = \app\models\PublisherMeta::find()->where(['publisher_id' => $user_id,'key' => 'access_token'])->select('value')->asArray()->one();
                if($user && isset($user['value']))
                {
                    self::$access_token = $user['value'];
                }
            }
        }
        return self::$access_token;
    }
}