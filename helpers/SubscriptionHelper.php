<?php
namespace app\helpers;

use Yii;
use app\models\Subscription;
use app\models\Comunity;
use app\models\ComunityMeta;

class SubscriptionHelper
{
    public static function getSubscription($id)
    {
        $subscription = Subscription::find()->with('comunity')->where(['id' => $id])
                ->asArray()->one();
        if(!$subscription) return;
        
        $isAdmin = PublisherHelper::isAdmin();
        
        if( ($subscription['comunity_id'] == PublisherHelper::getComunityId()) || $isAdmin)
        {
            $subscription['plan'] = @unserialize($subscription['plan']);
            //$subscription['meta'] = @unserialize($subscription['meta']);
            return $subscription;
        }
        else
        {
            throw new \Exception("No tiene permisos para ver esta suscripción", 1300 );
        }
    }
    
    public static function getSubscriptions($options = [])
    {
        $isAdmin = PublisherHelper::isAdmin();
        
        $where = [];
        if(!$isAdmin) $where['comunity_id'] = PublisherHelper::getComunityId();
        else if(array_key_exists('comunity_id',$options) && $options['comunity_id'])
        {
            $where['comunity_id'] = $options['comunity_id'];
        }
        
        if(array_key_exists('onlyActive',$options) && $options['onlyActive'])
        {
            $where['active'] = 1;
        }
        
        $query = Subscription::find()->where($where)
                ->orderBy('created_time DESC')
                ->asArray();
        
        $notIncludeComunity = array_key_exists('notIncludeComunity',$options) && $options['notIncludeComunity'];
        if(!$notIncludeComunity)
            $query->joinWith(['comunity']);
        
        $subscriptions_db = $query->all();
        
        $subscriptions = [];
        
        foreach ($subscriptions_db as $subscription)
        {
            $subscription['plan'] = @unserialize($subscription['plan']);
            //$subscription['meta'] = @unserialize($subscription['meta']);
            
            $subscriptions []= $subscription;
        }
        
        return $subscriptions;
    }
    
    public static function comunityHasActiveSubscription($comunity_id)
    {
        $time = time();
        $one = Subscription::find()
                ->andWhere(['comunity_id' => $comunity_id])
                ->andWhere("start_time < :time AND end_time >= :time", ['time' => $time])
                ->orderBy('created_time DESC')
                ->asArray()
                ->one()
                ;
        if($one)
        {
            if(!$one['active'])
            {
                Subscription::updateAll(['active' => 0], ['comunity_id' => $comunity_id]);
                Subscription::updateAll(['active' => 1], ['id' => $one['id']]);
            }
            
            return $one;
        }
    }


    public static function createSubscription($order)
    {
        $time = time();
        
        if(isset($order->attributes)) $order = $order->attributes;
        
        $order_id = $order['id'];
        $plan = unserialize($order['plan']);
        $comunity_id = $order['comunity_id'];
        
        $active_users = $plan['publishers'];
        
        $start_time = $time;
        $active_subscription = self::comunityHasActiveSubscription($comunity_id);
        if($active_subscription)
        {
            $start_time = $active_subscription['end_time'];
        }
        
        $end_time = $start_time;
        if(isset($plan['months']) && ($months = $plan['months']))
        {
            $end_time = strtotime("+$months month", $start_time);
        }
        else if(isset ($plan['days'])  && ($days = $plan['days']))
        {
            $end_time = strtotime("+$days days", $start_time);
        }

        $user_id = Yii::$app->user->id;
        
        $subscription = new Subscription([
            'id' => self::generateUniqueSubscriptionId(),
            'comunity_id' => $comunity_id,
            'creator_id' => $user_id,
            'order_id' => $order_id,
            'plan' => serialize($plan),
            'active_users' => $active_users,
            'created_time' => $time,
            'start_time' => $start_time,
            'end_time' => $end_time,
            'active' => $active_subscription?0:1
        ]);
        
        if($subscription->save())
		{
			$emails = ComunityHelper::getAdminEmails($comunity_id);
			
			if(empty($emails)) $emails ['admin@miautopub.info'] = 'Admin MiAutoPub';
			
				
			$subscription['plan'] = @unserialize($subscription['plan']);
			$subscription['created_time'] = date("D d M Y", $subscription['created_time']);
			$subscription['start_time'] = date("d/m/Y h:i a", $subscription['start_time']);
			$subscription['end_time'] = date("d/m/Y h:i a", $subscription['end_time']);
			
			Yii::$app->mailer->compose('thanksforsuscribing', ['subscription' => $subscription->attributes])
				->setFrom(['admin@miautopub.info' => 'Admin MiAutoPub'])
				->setTo( $emails )
				->setSubject('MiAutoPub - Confirmación de suscripción')
				->send();
			
			return $subscription;
		}
            
    }
    
    public static function cancelSubscription($id)
    {
        $subscription = Subscription::findOne($id);
        
        if($subscription->status == 'pending')
        {
            $subscription->status = 'canceled';
            $subscription->save();
        }
        else
        {
            throw new \yii\base\Exception('Esta suscripción no puede ser cancelada');
        }
        
        return $subscription;
        
    }
    
    public static function validateSubscription($id, $numOper)
    {
        $subscription = Subscription::findOne($id);
        $meta = unserialize($subscription['meta']);
        if(!$meta) $meta = [];
        
        $meta['numberOperation'] = $numOper;
        
        $subscription->meta = serialize($meta);
        $subscription->status = 'validating';
        $subscription->save();
        
        return $subscription;
    }

    public static function aproveSubscription($id)
    {
        $subscription = Subscription::findOne($id);
        $meta = unserialize($subscription['meta']);
        if(!$meta) $meta = [];
        
        if($subscription->status == 'validating')
        {
            if(SubscriptionHelper::createSubscription($subscription))
                $subscription->status = 'paid';
        }
        $subscription->save();
        
        return $subscription;
    }

    public static function validatePaypal($data)
    {
        if(!isset($data['txn_id'])) return;
        $txn = $data['txn_id'];
        
        $pdt_data = self::paypal_PDT_request($txn, "9Lj7j-IEEQnS8PR6LXKLcVbyznTV5WtKttNYdZ7P7H1nfGChnkRgwY8jiuS");
        
        if($pdt_data && isset($pdt_data['item_number']))
        {
            $subscription_id = $pdt_data['item_number'];
            
            if($pdt_data['payment_status'] == 'Completed')
            {
                $subscription = Subscription::findOne($subscription_id);
                if($subscription->status == 'pending')
                {
                    $meta = unserialize($subscription['meta']);if(!$meta) $meta = [];
                    $meta['numberOperation'] = $txn;
                    $meta['paypal_post_data'] = $data;
                    $meta['paypal_pdt_data'] = $pdt_data;
                    $subscription->meta = serialize($meta);

                    if(SubscriptionHelper::createSubscription($subscription))
                        $subscription->status = 'paid';
                    $subscription->save();

                    return $subscription;
                }
            }
        }
    }

    private static function paypal_PDT_request($tx, $pdt_identity_token) 
    {
        $request = curl_init();

        // Set request options
        curl_setopt_array($request, array
            (
              CURLOPT_URL => 'https://www.sandbox.paypal.com/cgi-bin/webscr',
              CURLOPT_POST => TRUE,
              CURLOPT_POSTFIELDS => http_build_query(array
                  (
                    'cmd' => '_notify-synch',
                    'tx' => $tx,
                    'at' => $pdt_identity_token,
                  )
              ),
              CURLOPT_RETURNTRANSFER => TRUE,
              CURLOPT_HEADER => FALSE,
              CURLOPT_SSL_VERIFYPEER => FALSE,
              
            )
        );

        $response = curl_exec($request);
        $status   = curl_getinfo($request, CURLINFO_HTTP_CODE);

        // Cerrar la conexión
        curl_close($request);
        
        $lines = explode("\n", $response);
        $keyarray = array();
        
        if (strcmp ($lines[0], "SUCCESS") == 0) 
        {
            for ($i=1; $i<count($lines);$i++)
            {
                @list($key,$val) = explode("=", $lines[$i]);
                $keyarray[urldecode($key)] = urldecode($val);
            }
            return $keyarray;
        }
        else if (strcmp ($lines[0], "FAIL") == 0) {
            // log for manual investigation
        }
        
        return NULL;
    }

    private static function generateUniqueSubscriptionId()
    {
        $new_id = 0;
        do 
        {
            $new_id = mt_rand(1000000000,9999999999);
            
        } 
        while(Subscription::find()->where(['id' => $new_id])->count() > 0);
        
        return $new_id;
        
    }
    
}