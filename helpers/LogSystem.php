<?php
namespace app\helpers;

use Yii;
use app\models\Log;

Class LogSystem
{
    
    public static function logSystem($event, $data = [])
    {
        $time = time();
        $app = Yii::$app;
        if($app->user)
        {
            $user_id = Yii::$app->user->id;
            $displayname = Yii::$app->user->id;
        }
        else
        {
            $user_id = NULL;
            $displayname = "";
        }
        
        
        $request = $app->request;
        
        $headers = array(
            'requestUri' => $request->url,
            'pathInfo' =>  $request->pathInfo,
            'scriptFile' =>  $request->scriptFile,
            'scriptUrl' =>  $request->scriptUrl,
            'hostInfo' =>  $request->hostInfo,
            'baseUrl' =>  $request->baseUrl,
            'userAgent' => $request->userAgent,
            'userHost' => $request->userHost,
            'acceptTypes' => $request->acceptableContentTypes,
            'requestType' => $request->contentType,
            'serverName' => $request->serverName,
        );
        
        if(!$data) $data = [];
        /*
        $data['ip'] = $request->userIP?$request->userIP:'unknow';
        $data['host'] = $request->userHost?$request->userHost:'unknow';
        $data['url'] = $request->referrer;
        $data['headers'] = $headers;
        */
        $logSystem = new Log();
        $logSystem->setAttributes(array(
            'user_id' => $user_id,
            'display' => $displayname,
            'datetime' => $time,
            'controller_action'=> Yii::$app->controller->id.".".Yii::$app->controller->action->id,
            'event' => $event,
            'data' => json_encode($data)
        ));
        $logSystem->save();
    }
}