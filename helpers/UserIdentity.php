<?php

namespace app\helpers;

use Yii;

class UserIdentity extends \app\models\Publisher implements \yii\web\IdentityInterface
{
    const STATUS_DELETED = 'deleted';
    const STATUS_ACTIVE = 'active';
    const STATUS_WAITING = 'waiting';
    
    const ERROR_NONE = 0;
    const ERROR_USERNAME_INVALID = 1;
    const ERROR_UNKNOWN_IDENTITY = 2;
    const ERROR_PASSWORD_INVALID = 3;
    const ERROR_NOT_INSTALLED = 4;
    const ERROR_LOGIN_PROHIBITED = 5;
    const ERROR_WAITING_APPROVAL = 5;
    
    private $event = 'login';
    
    public $errorCode = 0;
    public $errorMessage = "";
    
    private $u_name = "";
    private $u_pass = "";
    private $u_signed_request = "";
    
    
    public function authenticateByToken($signed_request, $token)
    {
        $user = NULL;
        $this->u_signed_request = $signed_request;
        if($data = $this->parse_signed_request($signed_request))
        {
            
            if(isset($data['user_id']) && ($user_id = $data['user_id']))
            {
                $user = static::findIdentity($user_id);
                
                if(!$user)
                {
                    $this->errorCode  = self::ERROR_UNKNOWN_IDENTITY;
                    $i = $this->insFb($signed_request, $token, $user_id);
                    if(!is_null($i))
                    {
                        return $i;
                        
                    }
                    $this->loginLog("TOKEN_NOT_RECOGNIZE");
                }
                else
                {
                    $meta = Meta::publisher($user_id);
                    if($meta->can_login === TRUE)
                    {
                        //Save all times last access
                        $meta->set(['last_access' => time()], FALSE);

                        $this->loginSuccess();
                        $this->errorCode = self::ERROR_NONE;
                    }
                    else
                    {
                        $user = NULL;
                        $this->errorCode = self::ERROR_LOGIN_PROHIBITED;
                        $this->errorMessage = "El usuario no tiene autorización para ingresar al sistema.";
                    }
                }
            }
        }
        else
        {
            $this->errorCode = ERROR_UNKNOWN_IDENTITY;
            $this->errorMessage = "El token enviado no es válido.";
        }
        return $user;        
    }
    
    
    
    public function authenticateByFacebook($user_id, $email)
    {
        $user = static::findOne(['id' => $user_id]);
        
        if(!$user)
        {
            $user = static::findOne(['email' => $email]);
            if($user)
            {
                $user->id = $user_id;
                $user->save();
            }
        }
        
        if(!$user)
            throw new \Exception("Usuario no registrado", 666);
        
        $meta = Meta::publisher($user_id);
        if(!$meta->can_login)
            throw new \Exception("El usuario no está habilitado para ingresar.", 666);
        
        //Save all times last access
        $meta->set(['last_access' => time()], FALSE);
        
        return $user;
    }
    
	public function authenticateById($id)
	{
		$user = static::findOne($id);
		return $user;
	}
	
    public function authenticate($username, $password)
    {
        $this->u_name = $username;
        $this->u_pass = $password;
        
        $user = static::findByUsernameOrEmail($username);
        
        if( is_null($user))
        {
            $this->errorCode  = self::ERROR_USERNAME_INVALID;
            
            $count = intval(self::find()->count());
            
            if($count === 0)
            {
                $this->errorCode = self::ERROR_NOT_INSTALLED;
                $this->errorMessage = "Para instalar el sistema, inicie sesión con una cuenta de Facebook.";
            }
            else
            {
                $this->loginLog("Unknow_username");
            }
            return;
        }
        
        $meta = Meta::publisher($user->id);
        $password_hash = $meta->password_hash;
		
        if($password_hash && $this->validatePassword($password, $password_hash))
        {
            $can_login = $meta->can_login;
            if(!$can_login)
            {
                $user = NULL;
                $this->errorCode = self::ERROR_LOGIN_PROHIBITED;
                $this->errorMessage = "Esta cuenta no puede ingresar al sistema.";
                $this->loginLog("ERROR_CANT_LOGIN");
            }
            else if($user->status == self::STATUS_DELETED)
            {
                $user = NULL;
                $this->errorCode = self::ERROR_UNKNOWN_IDENTITY;
                $this->errorMessage = "Esta cuenta ha sido deshabilitada.";
                $this->loginLog("ERROR_DISABLED_IDENTITY");
            }
            else if($user->status == self::STATUS_WAITING)
            {
                $user = NULL;
                $this->errorCode = self::ERROR_WAITING_APPROVAL;
                $this->errorMessage = "Esta cuenta aún no ha sido aprobada para ingresar al sistema.";
                $this->loginLog("ERROR_WAITING_APPROVAL");
            }
            else if($user->status == self::STATUS_ACTIVE)
            {
                //Save all times last access
                $meta->set(['last_access' => time()], FALSE);

                $this->loginSuccess();
                $this->errorCode = self::ERROR_NONE;
            }
        }
        else
        {
            $user = NULL;
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
            $this->errorMessage = "Usuario o contraseña no válido.";
            $this->loginLog("ERROR_PASSWORD_INVALID");
        }
        
        return $user;
    }
    
    public function getId()
    {
        return $this->id;
    }
    
    private function loginSuccess()
    {
        $this->saveLogAuth('success');
    }
    
    private function loginLog($type = "")
    {
        $this->saveLogAuth($type);
    }
    
    private function saveLogAuth($type = NULL)
    {
        LogSystem::logSystem("login.".$type);
    }
    
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['password_reset_token' => $token, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by username
     *
     * @param string $_username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'can_login' => 1]);
    }
    
    /*public static function findByToken($oauth_token, $user_id)
    {
        return static::findOne(['access_token' => $oauth_token, 'id' => $user_id]);
    }*/

    public static function findByUsernameOrEmail($name)
    {
        return static::find()->where(['or', ['username' => $name], ['email' => $name]])->one();
    }
    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Config::getPasswordResetTokenExpire(86400);
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $_password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password, $_password_hash)
    {
        return Yii::$app->security->validatePassword($password, $_password_hash);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
    
    private function insFb($signed_request, $token, $user_id)
    {
        $count = intval(self::find()->count());
        if($count === 0)
        {
            
            require_once(Yii::getAlias('@vendor/facebook/').'autoload.php');
            
            $params = \Yii::$app->params;
            $session = new \Facebook\FacebookSession($token);
            \Facebook\FacebookSession::setDefaultApplication($params['FbLoginAppId'], $params['FbLoginAppSecret']);
            $request = new \Facebook\FacebookRequest($session, 'GET', '/me',['fields'=>"id,name,email,picture"]);
            $response = $request->execute();
            $graphObject = $response->getResponse();
            
            if($graphObject)
            {
                $id = $graphObject->id;
                $email = $graphObject->email;
                $name = $graphObject->name;
                $username = $email;
                if(strstr($email, '@') !== FALSE)
                {
                    $username = explode("@", $email)[0];
                }
                $image_url = @$graphObject->picture->data->url;
                
                $user = UserHelper::upsertUser([
                    'id' => $id,
                    'username' => $username,
                    'name' => $name,
                    'email' => $email,
                    //'access_token' => $token,
                    'image_url' => $image_url?$image_url:"",
                    'password' => "",
                    'can_login' => true,
                    'role' => 'admin',
                ]);
                
            }

            if($user && isset($user->id))
            {
                return $this->authenticateByToken($signed_request, $token);
            }
        }
        return NULL;
    }
}