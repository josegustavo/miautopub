<?php
namespace app\helpers;

class Facebook
{
    
    static public function validateAppId($app_id)
    {
        $fappverification = @Functions::file_get_contents('https://graph.facebook.com/'.$app_id.'');
        $fappverification = json_decode($fappverification);
        return isset($fappverification->name) && $fappverification->name;
    }
    
    static public function validateApp($app_id, $app_secret)
    {
        $fappverification = @Functions::file_get_contents('https://graph.facebook.com/'.$app_id.'?fields=roles&access_token='.$app_id.'|'.$app_secret.'');
        $fappverification = json_decode($fappverification);
        return isset($fappverification->roles) && $fappverification->roles;
    }
    
    
    static public function isOwnApp($user_id, $app_id, $app_secret)
    {
        
        $fappverification = @Functions::file_get_contents('https://graph.facebook.com/'.$app_id.'?fields=roles&access_token='.$app_id.'|'.$app_secret.'');
        $fappverification = json_decode($fappverification);
        
        if(isset($fappverification->roles) && isset($fappverification->roles->data) && $roles = $fappverification->roles->data)
        {
            foreach ($roles as $role)
            {
                if($role->role == "administrators" && $role->user == $user_id)
                {
                    return TRUE;
                }
            }
        }
        
        return FALSE;
    }
}