<?php
namespace app\helpers;

use Yii;
use app\models\Comunity;
use app\models\Publication;
use app\models\Publisher;

class ComunityHelper
{
    public static function create($data)
    {
        $time = time();
        
        $name = $data['name'];
        
        $exist = Comunity::find()->where(['like', 'name', $name])->count();
        if($exist) throw new \Exception("No puede usar el nombre '$name' para su comunidad.", 666);
        
        $id = hexdec(substr(sha1(uniqid(microtime(true), true)),0,10));
        
        $comunity = new Comunity([
            'id' => $id,
            'name' => $name,
            'status' => 'active',
            'created_time' => $time
        ]);
        $save = $comunity->save();
        if($save)
        {
            return $comunity;
        }
        else
        {
            throw new \Exception("Ocurrió un error al crear la comunidad");
        }
        return NULL;        
    }
	
	public static function getAdminEmails($comunity_id)
	{
		$publishers = Publisher::find()
			->joinWith('publisherMetas')
			->where(['comunity_id' => $comunity_id, 'publisher_meta.key' => 'can_login', 'publisher_meta.value' => 'b:1;'])
			->asArray()->all();
		$emails = [];
		
		foreach($publishers as $publisher)
		{
			if($publisher['email']) $emails [$publisher['email']] = $publisher['name'];
		}
		
		return $emails;
	}
}