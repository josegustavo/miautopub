<?php
namespace app\helpers;

use Yii;
use app\models\Publisher;
use app\models\Publication;
use app\models\PublisherMeta;
use app\models\GroupPublisher;

class PublisherHelper
{
    
    public static function getComunityId($user_id = NULL)
    {
        if(!$user_id && ($user = Yii::$app->user))
        {
            $user_id = Yii::$app->user->id;
            return Yii::$app->user->identity->comunity_id;
        }
        else
        {
            $comunity_id = Publisher::findOne(['id' => $user_id])->comunity_id;
            return $comunity_id;
        }
        
    }
    
    public static function isAdmin($user_id = NULL)
    {
        return self::hasRole('admin', $user_id);
    }
    
    public static function isUser($user_id = NULL)
    {
        return self::hasRole('user', $user_id);
    }
    
    public static function isPublisher($user_id = NULL)
    {
        return self::hasRole('publisher', $user_id);
    }
    
    public static function hasRole($role, $user_id = NULL)
    {
        if(!$user_id && ($user = Yii::$app->user))
        {
            $user_id = Yii::$app->user->id;
        }
        $publisher = PublisherMeta::find()
                ->where(['publisher_id' => $user_id,'key' => 'role', 'value' => $role])
                ->orderBy('time DESC')
                ->asArray()
                ->one();
        return $publisher != NULL;
    }

    public static function filter_users(array &$users)
    {
        foreach ($users as $k => $user)
        {
            self::filter_user($users[$k]);
        }
        return $users;
    }
    
    public static function publisherBelongsComunity($publisher_id, $comunity_id)
    {
        
    }
    
    public static function filter_user(array &$user)
    {
        //$groups = GroupPublisher::find()->joinWith('group')->where(['publisher_id' => $user['id']])->asArray()->all();
        if(isset($user['groupPublishers']) && ($groups_publisher = $user['groupPublishers']))
        {
            $mi_groups = [];
            foreach ($groups_publisher as $group_publisher)
            {
                $add = [];
                $group = $group_publisher['group'];
                
                $add['id'] = $group['id'];
                $add['name'] = $group['name'];
				$add['wall_id'] = $group['wall_id'];
                $add['image_url'] = $group['image_url'];
                $add['can_publish'] = (bool)$group['can_publish'];
                $add['active'] = (bool)$group_publisher['active'];
                $mi_groups []= $add;
            }
            $user['groups'] = $mi_groups;
            unset($user['groupPublishers']);
        }
        

        $select_metas = ['can_login','can_publish','from','until','error_count','cant_publish_reason','role','app_id','app_valid','oportunity_publish','time_betwen_pubs','last_publication_time','publication_counts'];
        
        $meta = Meta::publisher($user['id']);
        
        $metas = $meta->get($select_metas);
        $user = array_merge($user, $metas);
        
        if(isset($user['from']))
        {
            $user['from'] = intval($user['from']);
        }
        
        if(isset($user['until']))
        {
            $user['until'] = intval($user['until']);
        }
        
        //Oportunidad de publicación, default 75%
        if(isset($user['oportunity_publish']))
        {
            $user['oportunity_publish'] = intval($user['oportunity_publish']);
        }
        else
        {
            $user['oportunity_publish'] = 75;
        }
        
        //Tiempo entre publicaciones, default 1 minuto
        if(isset($user['time_betwen_pubs']))
        {
            $user['time_betwen_pubs'] = intval($user['time_betwen_pubs']);
        }
        else
        {
            $user['time_betwen_pubs'] = 1;
        }
        
        if(isset($user['created_time']))
        {
            $user['created_time'] = $user['created_time'];
        }
        if(isset($user['modified_time']))
        {
            $user['modified_time'] = $user['modified_time'];
        }
        
        $today = mktime(0, 0, 0);
        $seconds_by_day = 60*60*24;
        if(!isset($user['publication_counts']))
        {
            $first_pub_time = Publication::find()
                    ->orderBy('id ASC')
                    ->select('published_time')
                    ->one()->published_time;
            if(!$first_pub_time)
            {
                $first_pub_time = $today - 30*$seconds_by_day;
            }
            
            $publication_counts = [];
            while ($today > $first_pub_time)
            {
                $end_day = $today + $seconds_by_day;
                $publications_today = Publication::find()
                        ->andWhere(['between', 'published_time', $today, $end_day])
                        ->andWhere(['publisher_id' => $user['id']])
                        ->count();
                $publication_counts[$today] = $publications_today;
                $today -= $seconds_by_day;
            }
            $meta->publication_counts = $publication_counts;
            $user['publication_counts'] = $publication_counts;
        }
        else
        {
            $publication_counts = $user['publication_counts'];
            
            //Si no se ha generado estadísticas hoy
            if(!array_key_exists($today, $publication_counts))
            {
                //Actualizar o generar las de ayer
                $yesterday = $today - $seconds_by_day;
                $publications_yesterday = Publication::find()
                        ->andWhere(['between', 'published_time', $yesterday, $today])
                        ->andWhere(['publisher_id' => $user['id']])
                        ->count();
                $publication_counts[$yesterday] = $publications_yesterday;
                
                //Generar las de hoy
                $publications_today = Publication::find()
                        ->andWhere(['between', 'published_time', $today, $today + $seconds_by_day])
                        ->andWhere(['publisher_id' => $user['id']])
                        ->count();
                $publication_counts[$today] = $publications_today;
                
                $meta->publication_counts = $publication_counts;
            }
            else //Si ya se ha generado estadísticas hoy, atualizar a la hora
            {
                $publications_today = Publication::find()
                        ->andWhere(['between', 'published_time', $today, $today + $seconds_by_day])
                        ->andWhere(['publisher_id' => $user['id']])
                        ->count();
                if($publication_counts[$today] != $publications_today)
                {
                    $publication_counts[$today] = $publications_today;
                    $meta->publication_counts = $publication_counts;
                }                
            }
            $user['publication_counts'] = $publication_counts;
        }
        
        //Si se tiene estadísticas, enviar de los últimos 15 días
        if($user['publication_counts'])
        {
            //var_dump($user['publication_counts']);
            //$user['publication_counts'] = array_values($user['publication_counts']);
            $user['publication_counts'] = array_slice(($user['publication_counts']), -15, 15);
            //var_dump($user['publication_counts']);
            //exit;
        }
        
        
        //$user['last_access'] = Functions::getDateTime($user['last_access'], $tz);
        
        //$user['can_login'] = (bool)$user['can_login'];
        //$user['can_publish'] = (bool)$user['can_publish'];
        
        unset($user['password_hash']);
        unset($user['creator_id']);
        
        return $user;
    }
    
    public static function getOnePublisher($user_id)
    {
        $user = Publisher::find()
                ->asArray()
                ->where(['publisher.id' => $user_id])->one();
        if($user)
        {
            return self::filter_user($user);
        }
    }

    public static function create($data)
    {
        $time = time();
        $loginuser = Yii::$app->user;
        $creator_id = ($loginuser->isGuest)?"":$loginuser->id;
        
        $user_id = $data['id'];
        $user_name = $data['name'];
        $user_email = $data['email'];
        
        $exist = Publisher::find()
                ->orWhere(['id' => $user_id])
                ->orWhere(['username' => $user_name])
                ->orWhere(['email' => $user_email])
                ->count();
        if($exist)
            throw new \Exception("El usuario ya se encuentra registrado", 666);


        $publisher = new Publisher([
            'id' => $user_id,
            'comunity_id' => $data['comunity_id'],
            'username' => $data['username'],
            'email' => $data['email'],
            'name' => $data['name'],
            'status' => UserIdentity::STATUS_ACTIVE,
            'image_url' => $data['image_url'],
            'created_time' => $time,
            'modified_time' => $time,
            'creator_id' => $creator_id
        ]);
        
        if($publisher->save())
        {
            $meta = Meta::publisher($publisher->id);
            $meta->password_hash = Yii::$app->security->generatePasswordHash($data['password']);
	    $meta->role = $data['role'];
            $meta->can_login = $data['can_login'];
            return $publisher;
        }
        else
        {
            print_r($publisher->getErrors());exit;
            throw new \Exception("UNKNOW");
        }
    }


    public static function upsertPublisher($attr, $role = NULL)
    {
        $time = time();
        $user = NULL;
        if(array_key_exists("id", $attr))
        {
            $user = Publisher::findOne($attr['id']);
        }
        if(!$user)
        {
            $user = new Publisher();
        }
        
        $is_new = $user->isNewRecord;
        $last_attr = $user->attributes;
        
	
	
        if(isset($attr['id'])) $user->id = $attr['id'];
		if(isset($attr['comunity_id'])) $user->comunity_id = $attr['comunity_id'];
        //if(isset($attr['role'])) $user->role = $attr['role'];
	
        //if(isset($attr['access_token'])) $user->access_token = $attr['access_token'];
        //if(isset($attr['can_login'])) $user->can_login = $attr['can_login'];
        //if(isset($attr['can_publish'])) $user->can_publish = $attr['can_publish'];
        if(isset($attr['image_url'])) $user->image_url = $attr['image_url'];
        if(isset($attr['username'])) $user->username = $attr['username'];
        
        
        //if(isset($attr['app_id'])) $user->app_id = isset($attr['app_id'])?$attr['app_id']:'';
        //if(isset($attr['app_secret'])) $user->app_secret = isset($attr['app_secret'])?$attr['app_secret']:'';
        
        if(isset($attr['email'])) $user->email = isset($attr['email'])?$attr['email']:'';
        if(isset($attr['name'])) $user->name = isset($attr['name'])?$attr['name']:'';
        
        
        if($is_new)
        {
            if(Yii::$app->user)
            {
                $user->creator_id = Yii::$app->user->id;
            }
            $user->created_time = $time;
            $user->modified_time = $time;
            //$user->last_access = 0;
            $user->status = UserIdentity::STATUS_ACTIVE;
        }
        
        
        $attributes = $user->attributes;
        $diff = array_diff_assoc($last_attr, $attributes) || array_diff_assoc($attributes, $last_attr);
        
        if(!$is_new)
        {
            $user->modified_time = $time;
        }
        
        if($is_new || $diff)
        {
            $save = $user->save();
            if($save)
            {
                
                $name = $user->username;
                if($is_new)
                {
                    LogSystem::logSystem("createPublisher", $user->attributes);
                }
                else
                {
                    LogSystem::logSystem("updatePublisher", array('old' => array_diff_assoc($last_attr, $attributes), 'new' => array_diff_assoc($attributes, $last_attr)));
                }
            }
        }
        if(array_key_exists("password", $attr) && $attr['password'])
        {
            $password_hash = Yii::$app->security->generatePasswordHash($attr['password']);
            $meta = Meta::publisher($user->id);
            $meta->password_hash = $password_hash;
        }
        return $user;    
    }

    
    public static function validatePublisher($model, $role = NULL)
    {
        $user = array();
        $is_new = true;
        if(isset($model['id']) && $model['id'] && is_numeric($model['id']))
        {
            $is_new = false;
            $user['id'] = $model['id'];
        }
        if(isset($model['username']) && $model['username'])
        {
            $username = trim(strtolower($model['username']));
            
            if(preg_match('/^[a-z0-9_\.]+$/u',$username,$match))
            {
                $exist = Publisher::find()->where(['username' => $username])->one();
                if( $exist && ( !array_key_exists('id', $user) || ($exist->id != $user['id'])) )
                {
                    throw new \Exception("El nombre de usuario '". $username ."' ya existe en el sistema");
                }
                else
                {
                    $user['username'] = $username;
                }
            }
            else
            {
                throw new \Exception("El nombre de usuario sólo puede contener caracteres alfanuméricos y '.'(punto).");
            }
        }
        else if($is_new)
        {
            throw new \Exception("Se necesita un nombre de usuario");
        }
        
        if(isset($model['email']) && $model['email'])
        {
            $email = $model['email'];
            if(!filter_var($email, FILTER_VALIDATE_EMAIL))
            {
                throw new \yii\base\Exception("El email ingresado no es válido");
            }
            else
            {
                $exist = Publisher::find()->where(['email' => $email])->one();
                if( $exist && ( !array_key_exists('id', $user) || ($exist->id != $user['id'])) )
                {
                    throw new \Exception("El email '". $email ."' ya está registrado en el sistema");
                }
                else
                {
                    $user['username'] = $username;
                }
                $user['email'] = $email;
            }
        }
        else if($is_new)
        {
            throw new \Exception("Se necesita el email del usuario.");
        }
        
        if(isset($model['name']) && trim($model['name']))
        {
            $name = trim($model['name']);
            $user['name'] = $name;
        }
        else if($is_new)
        {
            throw new \Exception("Se necesita el nombre del usuario.");
        }
        
        if(isset($model['password']) && count($model['password'])>0 && $model['password'] != '**********')
        {
            $user['password'] = $model['password'];
        }
        
        /*if(isset($model['access_token']))
        {
            $user['access_token'] = $model['access_token'];
        }*/
        if(isset($model['app_id']))
        {
            $user['app_id'] = $model['app_id'];
        }
        if(isset($model['app_secret']))
        {
            $user['app_secret'] = $model['app_secret'];
        }
        if(isset($model['role']))
        {
            $user['role'] = $model['role'];
        }
        if(isset($model['status']))
        {
            $user['status'] = $model['status'];
        }
        if(isset($model['can_login']))
        {
            $user['can_login'] = $model['can_login'];
        }
        if(isset($model['can_publish']))
        {
            $user['can_publish'] = $model['can_publish'];
        }
        return $user;
    }
    
    public static function deletePublisher($user_id)
    {
        $model = Publisher::findOne($user_id);
        $attr = $model->attributes;
        $transaction = Yii::$app->db->beginTransaction();
        try
        {
            if($model->delete())
            {
                $username = $attr['username'];
                unset($attr['password_hash']);
                LogSystem::logSystem("deletePublisher", $attr);
                $transaction->commit();
                return TRUE;
            }
            else
            {
                $transaction->rollback();
                return FALSE;
            }
        }
        catch (Exception $ex)
        {
            $transaction->rollback();
            Yii::$app->controller->debug = $ex->getMessage();
            return FALSE;
        }
    }
    
    public static function disablePublisher($user_id)
    {
        $model = Publisher::findOne($user_id);
        $attr = $model->attributes;
        $transaction = Yii::$app->db->beginTransaction();
        try
        {
            $model->status = UserIdentity::STATUS_DELETED;
            if($model->save())
            {
                $username = $attr['username'];
                unset($attr['password_hash']);
                LogSystem::logSystem("deletePublisher", $attr);
                $transaction->commit();
                return TRUE;
            }
            else
            {
                $transaction->rollback();
                return FALSE;
            }
        }
        catch (Exception $ex)
        {
            $transaction->rollback();
            if(YII_DEBUG)
            {
                Yii::$app->controller->debug = $ex->getMessage();
            }
            return FALSE;
        }
    }
    
    public static function enablePublisher($user_id)
    {
        $model = Publisher::findOne($user_id);
        $attr = $model->attributes;
        $transaction = Yii::$app->db->beginTransaction();
        try
        {
            $model->status = UserIdentity::STATUS_ACTIVE;
            if($model->save())
            {
                $username = $attr['username'];
                unset($attr['password_hash']);
                LogSystem::logSystem("deletePublisher", $attr);
                $transaction->commit();
                return TRUE;
            }
            else
            {
                $transaction->rollback();
                return FALSE;
            }
        }
        catch (Exception $ex)
        {
            $transaction->rollback();
            if(YII_DEBUG)
            {
                Yii::$app->controller->debug = $ex->getMessage();
            }
            return FALSE;
        }
    }

    /**
     * 
     * @param type $user_id default  Yii::$app->user->id
     */
    public static function getChildren($user_id = NULL)
    {
        if(!$user_id && ($user = Yii::$app->user))
        {
            $user_id = $user->id;
        }
        $parents = [$user_id];
        $publishers = Publisher::find()
                ->select(['id','creator_id'])
                ->where(['not', ['id'=>$user_id]])
                //->where(['status' => UserIdentity::STATUS_ACTIVE])
                ->asArray()
                ->all();
        
        $children = [];
        foreach ($publishers as $publisher)
        {
            if(in_array($publisher['creator_id'],$parents))
            {
                $children []= $publisher['id'];
            }
        }
        return $children;
    }
    
    public static function getParents($user_id = NULL)
    {
        if(!$user_id && ($user = Yii::$app->user))
        {
            $user_id = $user->id;
        }
        
        $publishers = Publisher::find()
                ->select(['id','creator_id'])
                ->where(['not', ['id'=>$user_id]])
                //->where(['status' => UserIdentity::STATUS_ACTIVE])
                ->asArray()
                ->all();
        
    }
    
    public static function isOwner($parent_id, $child_id) 
    {
        
    }

    
    public static function userBelongsApp($fb_id, $app)
    {
        list($app_id, $app_secret) = $app;
        $client = new \GuzzleHttp\Client([
            'base_uri' => 'https://graph.facebook.com/v2.2/',
            'verify' => FALSE,
            'headers' => [
                    'accept' => 'application/json',
                ],
            
            ]);
        try
		{
			
		
			$res = $client->get($fb_id, ['query' => ['access_token' => $app_id . "|" . $app_secret]]);
			$body = $res->getBody();
			$user = json_decode($body->getContents(), TRUE);
			$link = explode("/",  $user['link']);
			$user['app_scoped_user_id'] = $link[count($link)-2];

			$res = $client->get('/'.$app_id.'/roles', 
				['query' => [ 'access_token' => $app_id . "|" . $app_secret, 'limit' => 100 ]]
			);
			$body = $res->getBody();
			$content = json_decode($body->getContents(), TRUE);
			$data = $content['data'];
			$found = FALSE;
			foreach ($data as $d)
			{
				if($d['user'] == $user['app_scoped_user_id'])
				{
					$found = TRUE;
					break;
				}
			}
			return $found;
		}
		catch(\Exception $ex)
		{
			LogSystem::logSystem("userBelongsApp.error", ['ex' => $ex->getFile() . ":" . $ex->getLine() . "\n" . $ex->getMessage() . "\n\n" 
                    . var_export($ex->getRequest(), true) . "\n\n" 
                    . var_export($ex->getResponse(), true) . "\n\n" . $ex->getTraceAsString()]);
		}
    }
    
    
}
