<?php
namespace app\helpers;

use app\models\Group;
use app\models\GroupPublisher;
use app\models\Publication;

class GroupHelper
{
    private static $access_token;
    
    public static function getOrderByOptions($include_columns = FALSE)
    {
        $result = ['Nombre A-Z' => 'group.name ASC',
            'Nombre Z-A' => 'group.name DESC',
            'Más publicaciones' => 'group.publication_count DESC',
            'Menos publicaciones' => 'group.publication_count ASC',
            'Más recientemente publicados' => 'group.last_published_time DESC',
            'Menos recientemente publicados' => 'group.last_published_time ASC'
        ];
        
        if(!($include_columns === TRUE))
        {
            $result = array_keys($result);
        }
        return $result;
    }
    
    public static function filter_groups(array $groups)
    {
        foreach ($groups as $k => $group)
        {
            $groups[$k] = self::filter_group($group);
        }
        return $groups;
    }
    
    public static function filter_group($group)
    {
        $result = is_object($group)?$group->attributes:$group;
        
        $tz = NULL;
        //$result['created_time'] = Functions::getDateTime($result['created_time'], $tz);
        //$result['publish_created_time'] = Functions::getDateTime($result['publish_created_time'], $tz);
        //$result['modified_time'] = Functions::getDateTime($result['modified_time'], $tz);
        
        $result['can_publish'] = (bool)($result['can_publish']);
        
        $result['tags'] = $result['tags']?unserialize($result['tags']):[];
        
        
        $result['publication_count'] = (int)(Publication::find()->where(['group_id' => $group['id']])->count());
        
        
        if(isset($result['groupPublishers']))
        {
            $publishers_group = $result['groupPublishers'];
            $publishers = [];

            foreach ($publishers_group as $publisher_group)
            {
                $publishers []= [
                    'publisher_id' => $publisher_group['publisher_id'],
                    'active' => (bool)$publisher_group['active'],
                    'reason' => $publisher_group['reason'],
                    'publisher_name' => $publisher_group['publisher']['name'],
                    'publisher_can_publish' => unserialize(@$publisher_group['publisher']['publisherMetas'][0]['value'])
                ];
            }

            $result['publishers'] = $publishers;
            $result['publishers_count'] = count($publishers);
            
            unset($result['groupPublishers']);
        }
        
        return $result;
    }
    
    public static function upsertGroup($params)
    {
        $comunity_id = $params['comunity_id'];
        $wall_id = $params['wall_id'];
        
        
        if(!($group = Group::find()->where(['comunity_id' => $comunity_id, 'wall_id' => $wall_id])->one()))
        {
            $group = new Group($params);
        }
        else
        {
            $group->attributes = $params;
        }
        
        $group->save();
        
        return $group;
    }
}