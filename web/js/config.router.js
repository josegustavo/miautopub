'use strict';

/**
 * Config for the router
 */
angular.module('app')
  .run(
    [          '$rootScope', '$state', '$stateParams',
      function ($rootScope,   $state,   $stateParams) {
          $rootScope.$state = $state;
          $rootScope.$stateParams = $stateParams;        
      }
    ]
  )
  .config(
    [          '$stateProvider', '$urlRouterProvider', 
      function ($stateProvider,   $urlRouterProvider) {
        $urlRouterProvider
        .otherwise('/admin/publications');
        $stateProvider
            .state('admin', {
                  abstract: true,
                  url: '/admin',
                  templateUrl: 'tpl/layout.html'
            })
                .state('admin.comunity', {
                    url: '/comunity',
                    templateUrl: 'tpl/comunity/index.html',
                    resolve: {
                        deps: ['uiLoad', '$ocLazyLoad',
                          function( uiLoad, $ocLazyLoad){
                            $ocLazyLoad.load();
                            return uiLoad.load( 'js/app/comunity/index.js');
                        }]
                    }
                })
                .state('admin.publishers', {
                    url: '/publishers',
                    templateUrl: 'tpl/publishers/list.html',
                    resolve: {
                        deps: ['uiLoad',
                          function( uiLoad){
                            return uiLoad.load( 'js/app/publishers/list.js' );
                        }]
                    }
                })
                .state('admin.entries', {
                    url: '/entries',
                    templateUrl: 'tpl/entries/list.html',
                    resolve: {
                        deps: ['uiLoad',
                          function( uiLoad){
                            return uiLoad.load( 'js/app/entries/list.js' );
                        }]
                    }
                })
                .state('admin.groups', {
                    url: '/groups',
                    templateUrl: 'tpl/groups/list.html',
                    resolve: {
                        deps: ['uiLoad',
                          function( uiLoad){
                            return uiLoad.load( 'js/app/groups/list.js' );
                        }]
                    }
                })
                .state('admin.publications', {
                    url: '/publications',
                    templateUrl: 'tpl/publications/list.html',
                    resolve: {
                        deps: ['uiLoad', '$ocLazyLoad',
                          function( uiLoad, $ocLazyLoad){
                            $ocLazyLoad.load();
                            return uiLoad.load( 'js/app/publications/list.js');
                        }]
                    }
                })
            .state('login', {
                url: '/login',
                resolve: {
                    authenticate : function authenticate($q, user, $state, $timeout) {
                          if (user.isAuthenticated()) {
                            // Resolve the promise successfully
                            return $q.when()
                          } else {
                            // The next bit of code is asynchronously tricky.

                            $timeout(function() {
                              // This code runs after the authentication promise has been rejected.
                              // Go to the log-in page
                              $state.go('logInPage')
                            })

                            // Reject the authentication promise to prevent the state from loading
                            return $q.reject()
                          }
                  }
                }
            })

      }
    ]
  );
