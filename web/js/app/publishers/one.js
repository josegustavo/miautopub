

app.controller('PublisherCtrl', ['$scope', '$stateParams', '$state', '$http','$location', '$filter', '$timeout',  '$q', '$modal',
  function(                      $scope,   $stateParams,    $state,   $http,  $location,   $filter,   $timeout,  $q,  $modal) {
    var timeout = null;
    
    var publisher_id    = $stateParams.id;
    var tab             = $stateParams.tab;
    
    $scope.publisher = {};
    $scope.params = {};
    
    var loadPublisher = function(id, params)
    {
        var defered = $q.defer();
        var promise = defered.promise;
        $http.get('publisher/'+id,{params: params})
        .success(function(result) {
            defered.resolve(result);
        })
        .error(function(err){
            defered.reject(err)
        });
        return promise;
    };
    
    $scope.loadPublisher = function(id, params)
    {
        id = id || publisher_id;
        var params_send = angular.copy($scope.params);
        angular.forEach(params, function(value,key){
            params_send[key] = value;
        });
        
        $scope.gettingPublisher = true;
        loadPublisher(id,params_send)
        .then(function(result) {
            var success = result.success;
            if(success)
            {
                $scope.publisher = success.data;
                //$scope.totalItems = success.total_items;
                //$scope.totalPages = success.total_pages;
                //success.orderByOptions && ($scope.orderByOptions = success.orderByOptions);
                success.params && ($scope.params = success.params);
            }
            else
            {
                alert("Ocurrió un error al obtener el usuario");
            }
            $scope.gettingPublisher = false;
        });
    }
    window.setTimeout($scope.loadPublisher(), 0);
    
  }]
);

app.controller('ModalConfigureAppCtrl', function ($scope, $modalInstance,  $http,   publisher_id, app_id) 
{
    $scope.saving = false;
    
    $scope.app_id = app_id;
    $scope.app_secret = "";
    $scope.save = function ()
    {
        $scope.saving = true;
        $http.put('publisher/'+publisher_id, {app_id: $scope.app_id, app_secret : $scope.app_secret})
        .success(function(response)
        {
            if ( !response.success || response.error )
            {
                var msg = response.error && response.error.join(", ");
                $scope.resultmsg = msg || 'Ocurrió un error desconocido al configurar la aplicación de facebook.';
            }
            else
            {
                $modalInstance.close(response.success);
            }
        })
        .error(function(x) {
            var error = (x.data && x.data.error) || 'Error en el servidor';
            $scope.resultmsg = error;
        })
        .then(function(){
            $scope.saving = false;
        });
    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
});

app.controller('ModalChangePasswordCtrl', function ($scope, $modalInstance,  $http,   publisher_id) 
{
    $scope.saving = false;
    
    $scope.new_password = "";
    $scope.new_repassword = "";
    $scope.save = function ()
    {
        if($scope.new_password.length < 6)
        {
            $scope.resultmsg = "La contraseña debe contener al menos 6 caracteres";
            return;
        }
        
        if($scope.new_repassword != $scope.new_password)
        {
            $scope.resultmsg = "Las contraseñas no coinciden";
            return;
        }
        
        $scope.saving = true;
        $http.put('publisher/'+publisher_id, {password: $scope.new_password})
        .success(function(response)
        {
            if ( !response.success || response.error )
            {
                var msg = response.error && response.error.join(", ");
                $scope.resultmsg = msg || 'Ocurrió un error desconocido al configurar la aplicación de facebook.';
            }
            else
            {
                $modalInstance.close(response.success);
            }
        })
        .error(function(x) {
            var error = (x.data && x.data.error) || 'Error en el servidor';
            $scope.resultmsg = error;
        })
        .then(function(){
            $scope.saving = false;
        });
    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
});

app.controller('ModalChangeCanPublishCtrl', function ($scope, $modalInstance,  $http,   publisher_id, activate) 
{
    $scope.saving = false;
    
    $scope.date = new Date();
    $scope.time = new Date();
    
    $scope.now = new Date();
    
    $scope.activate = activate;
    
    $scope.getTomorrowHourExact = function(hour)
    {
        return moment().add(1, 'days').hours(hour).minutes(0).seconds(0).unix();
    }
    
    $scope.getAddMinutesFromNow = function(minutes)
    {
        var now = new Date();
        var future = moment(now).add(minutes, 'minutes');
        return future.unix();
    }
    
    $scope.getSelectedDateTime = function()
    {
        var day = $scope.date.getDate();
        var month = $scope.date.getMonth();
        var year = $scope.date.getFullYear();
        
        var hour = $scope.time.getHours();
        var minute = $scope.time.getMinutes();
        
        var future = new Date(year, month, day, hour, minute, 0, 0);
        
        var future_mtimestamp = future.getTime();
        var now_mtimestamp = (new Date()).getTime();
        
        if(future_mtimestamp > now_mtimestamp)
        {
            return (future_mtimestamp/1000);
        }
        else
        {
            return 0;
        }
    }
    
    $scope.save = function (timestamp)
    {
        $scope.saving = true;
        $http.put('publisher/'+publisher_id, {can_publish: activate, until : timestamp})
        .success(function(response)
        {
            if ( !response.success || response.error )
            {
                var msg = response.error && response.error.join("\n");
                $scope.resultmsg = msg || 'Ocurrió un error desconocido al configurar las opciones.';
            }
            else
            {
                $modalInstance.close(response.success);
            }
        })
        .error(function(x) {
            var error = (x.data && x.data.error) || 'Error en el servidor';
            $scope.resultmsg = error;
        })
        .then(function(){
            $scope.saving = false;
        });
    };

    $scope.cancel = function () {
        console.log("cancelar");
      $modalInstance.dismiss('cancel');
    };
});