

app.controller('PublisherListCtrl', ['$scope', '$http','$location', '$filter', '$timeout',  '$q', '$modal',
  function(                      $scope,   $http,  $location,   $filter,   $timeout,  $q,  $modal) {
    var timeout = null;
    $scope.selectedPublisher = null;
    
    var sliderTime = angular.element("#slider-time");
    var sliderProb = angular.element("#slider-prob");
    
    sliderTime.on('slideStop', function(data){
        $scope.savePublisher('time_betwen_pubs', data.value);
    });
    sliderProb.on('slideStop', function(data){
        $scope.savePublisher('oportunity_publish', data.value);
    });
    
    $scope.editPublisher = function(publisher)
    {
        $scope.selectedPublisher = publisher;
        sliderTime.slider('setValue', publisher.time_betwen_pubs);
        sliderProb.slider('setValue', publisher.oportunity_publish);
        $scope.tab(0);
    }
    
    $scope.publishers = [];
    $scope.params = {};
    
    var loadPublishers = function(params)
    {
        var defered = $q.defer();
        var promise = defered.promise;
        $http.get('publisher',{params: params})
        .success(function(result) {
            defered.resolve(result);
        })
        .error(function(err){
            defered.reject(err)
        });
        return promise;
    };
    
    
    
    $scope.loadPublishers = function(params)
    {
        var params_send = angular.copy($scope.params);
        angular.forEach(params, function(value,key){
            params_send[key] = value;
        });
        
        $scope.gettingPublishers = true;
        loadPublishers(params_send)
        .then(function(result) {
            var success = result.success;
            if(success)
            {
                $scope.publishers = success.data;
                //$scope.totalItems = success.total_items;
                //$scope.totalPages = success.total_pages;
                //success.orderByOptions && ($scope.orderByOptions = success.orderByOptions);
                success.params && ($scope.params = success.params);
            }
            else
            {
                alert("Ocurrió un error");
            }
            $scope.gettingPublishers = false;
        });
        
    }
    window.setTimeout($scope.loadPublishers(), 0);
    
    $scope.tabs = [true, false];
    $scope.tab = function(index){
      angular.forEach($scope.tabs, function(i, v) {
        $scope.tabs[v] = false;
      });
      $scope.tabs[index] = true;
    }
  
    $scope.loadGroups = function(publisher)
    {
        if(!publisher.groups)
        {
            $http.get('publisher/'+publisher.id+'/groups')
            .success(function(data){
                publisher.groups = data.success;
            })
            .error(function(data){
                
            })
        }
    }
    
    $scope.savePublisher = function(attr, value, time)
    {
        var publisher = $scope.selectedPublisher;
        if(!attr) return;
        if(!$scope.saving) $scope.saving = {};
        
        $scope.saving[attr] = value?value:publisher[attr];
        
        var time = time!=null?time:1000;
        if (timeout) {
            $timeout.cancel(timeout);
        }
        timeout = $timeout(function(){
          
            $http.put('publisher/'+publisher.id, $scope.saving)
            .success(function(response)
            {
                var error = response.error;
                angular.forEach(error, function(value){
                    alert(value)
                });
                
                var success = response.success;
                angular.forEach(success, function(value,key){
                    publisher[key] = value;
                });
            })
            .error(function(response){
                var error = response.error;
                
                if(!error)
                {
                    alert("Ocurrió un error en el servidor al guardar el valor, intentelo luego");
                }
				else
				{
					alert(error);
				}
            })
            .then(function(){
                $scope.saving = false;
            });
        }, time);
    }
    
    $scope.changeCanPublish = function(publisher)
    {
        if(publisher.can_publish == false)
        {
            $http.put('publisher/'+publisher.id, {can_publish: false})
            .success(function(response)
            {
                if ( !response.success || response.error )
                {
                    var msg = response.error && response.error.join("\n");
                    $scope.resultmsg = msg || 'Ocurrió un error desconocido al configurar las opciones.';
                    publisher.can_publish = true;
                }
                else
                {
                    angular.forEach(response.success, function(value,key){
                        publisher[key] = value;
                    });
                }
            })
            .error(function(x) {
                var error = (x.data && x.data.error) || 'Error en el servidor';
                $scope.resultmsg = error;
                publisher.can_publish = true;
            })
            
            return;
        }
        
        var modalInstance = $modal.open({
              templateUrl: 'ModalChangeCanPublishContent.html',
              controller : 'ModalChangeCanPublishCtrl',
              backdropClass  : 'modal-backdrop h-full',
              //size : 'sm',
              resolve: {
                publisher_id: function(){return publisher.id},
                activate: function(){return publisher.can_publish},
              }
            });
        modalInstance.result.then(function (result) {
            angular.forEach(result, function(value,key){
                publisher[key] = value;
            });
        }, function()
        {
            publisher.can_publish = !publisher.can_publish;
        });
    }

    var login_url = window.urlRoot + "/call-register?c="+window.comunity_id;
    
    $scope.copyLinkAddPublisher = function()
    {
        $scope.copiedLinkAddPublisher = true;
        var aux = document.createElement("input");
        aux.setAttribute("value", login_url);
        document.body.appendChild(aux);
        aux.select();
        document.execCommand("copy");
        document.body.removeChild(aux);
        
        $timeout(function(){
            $scope.copiedLinkAddPublisher = false;
        }, 1000);
    }
    
    $scope.copyLinkAuthorizeFacebook = function(publisher)
    {
        var url = window.urlRoot + '/call-authorize?u=' + publisher.id;
        
        var aux = document.createElement("input");
        aux.setAttribute("value", url);
        document.body.appendChild(aux);
        aux.select();
        document.execCommand("copy");
        document.body.removeChild(aux);
        alert("Enlace copiado");
    }
    
    $scope.createPublisher = function()
    {
        if(confirm("Se va a agregar una nueva cuenta desde Facebook, tiene que tener iniciada la sesión de la cuenta que desea agregar. Si tiene otra sesión abierta ciérrela antes de continuar.\n¿Desea continuar?"))
        {
            window.location = login_url;
        }
    }
    
    $scope.deletePublisher = function(publisher){
      if(confirm("¿Realmente desea eliminar este usuario?"))
      {
        var is_selected = (publisher == $scope.selectedPublisher);
        $http.delete('publisher/'+publisher.id).then(function(){
          $scope.publishers.splice($scope.publishers.indexOf(publisher), 1);
          if(is_selected)
          {
            $scope.selectedPublisher = null;
          }
        }, function(xhr){
          var msg = (xhr && xhr.data && xhr.data.error) || "Ocurrió un error en el servidor al eliminar el usuario, intentelo luego";
          alert(msg)
        })
      }
    }
    
    $scope.showConfigureApp = function(publisher)
    {
        var modalInstance = $modal.open({
              templateUrl: 'ModalConfigureAppContent.html',
              controller : 'ModalConfigureAppCtrl',
              backdropClass  : 'modal-backdrop h-full',
              resolve: {
                publisher_id: function(){return publisher.id},
                app_id: function(){return publisher.app_id},
              }
            });
        modalInstance.result.then(function (result) {
            angular.forEach(result, function(value,key){
                publisher[key] = value;
            });
        });
    };
    
    $scope.showChangePassword = function(publisher)
    {
        var modalInstance = $modal.open({
              templateUrl: 'ModalChangePasswordContent.html',
              controller : 'ModalChangePasswordCtrl',
              backdropClass  : 'modal-backdrop h-full',
              resolve: {
                publisher_id: function(){return publisher.id},
              }
            });
        modalInstance.result.then(function (result) {
            angular.forEach(result, function(value,key){
                publisher[key] = value;
            });
        });
    }
    
    $scope.changeStatusUserGroup = function(publisher, group)
    {
        var active = group.active;
        $http.post('publisher/changeGroupStatus', {user_id : publisher.id, group_id: group.id, active : !active})
        .then(function(response)
        {
            if ( !response.data.success )
            {
                alert('Ocurrió un error al cambiar el estado del grupo del publicador.');
            }
            else
            {
                group.active = response.data.success.active;
            }
        }, function(x) {
            var error = (x.data && x.data.error) || 'Error en el servidor';
            alert(error);
        }); 
    }
    
    var requirePublisherPermission = function(publisher, success)
    {
        var permision_groups_url = window.urlRoot + '/call-authorize?u=' + publisher.id;
        
        window.location = permision_groups_url;
    }
    
    $scope.updatePublisherFromFacebook = function(publisher)
    {
        if($scope.updatingFromFacebook)return;
        
        if(publisher.app_id)
        {
            $scope.updatingFromFacebook = true;
            $http.post('publisher/updateFromFacebook', {user_id : publisher.id})
            .then(function(response) {
                if ( !response.data.success )
                    alert('Error solicitar la carga datos desde Facebook');
                else
                {
                    $scope.selectedPublisher.groups = response.data.success.groups;
                    $scope.selectedPublisher.count_all_groups = response.data.success.count_all_groups;
                    
                    $scope.editPublisher($scope.selectedPublisher);
                }
                $scope.updatingFromFacebook = false;
            }, function(x) {
                var error = (x.data && x.data.error) || 'Error en el servidor';
                if(error == 'facebook_login_required')
                {
                    if(confirm("Aun no se ha concedido los permisos de Facebook. \nSe va a solicitar permisos para conectarse a Facebook, debe iniciar sesión con la cuenta de " + publisher.name + " para obtener sus grupos.\n\nTambién puede usar el enlace proporcionado para conceder los permisos desde otro navegador.\n\n¿Desea continuar a brindar los permisos?"))
                    {
                        requirePublisherPermission(publisher, function(){
                            alert("Permisos correctamente, vuelva a solicitar la actualización por facebook")
                        })
                    }
                }
                else
                {
                    alert(error);
                }
                $scope.updatingFromFacebook = false;
            });
                        
            
        }
        else
        {
            alert("Debe configurar el APP de Facebook con un App ID y App Secret válido.")
        }
    }
    
  }]
);

app.controller('ModalConfigureAppCtrl', function ($scope, $modalInstance,  $http, $timeout,   publisher_id, app_id) 
{
    $scope.saving = false;
    
    $scope.form = {
        app_id : app_id,
        app_secret : "",
        fb_id : "",
        verified : false,
        verifying : false,
    }
    
    $scope.requestToken = function()
    {
        var fb_id = $scope.form.fb_id;
        if(!fb_id)
        {
            alert("Debe ingresar un ID numérico del usuario de Facebook para el cual desea solicitar un token de aplicación.")
            return;
        }
        
        var verify = function(fb_id, app_id)
        {
            $http.get('publisher/verifyRequestToken', { params : {publisher_id : publisher_id, fb_id : fb_id, app_id : app_id}})
            .success(function(response) {
                var result = response.success;
                if(!result)
                {
                    if(!$scope.form.verified)
                        $timeout(function(){verify(fb_id, app_id);}, 10000);
                }
                else if(result.app_valid)
                {
                    $scope.form.verified = true;
                    alert("Su cuenta ha sido verificada y la aplicación correctamente configurada, ya puede cagar sus grupos desde Facebook.");
                    $modalInstance.close(result);
                }
                
            })
            .error(function(x) {
                var error = (x && x.error) || 'Error en el servidor';
                alert(error);
            });
        };
        
        
        
        $http.post('publisher/requestToken', { fb_id : fb_id, publisher_id : publisher_id})
            .success(function(response) {
                var result = response.success;
                var app_id = result.app_id;
                if(result.app_valid)
                {
                    $scope.form.verified = true;
                    alert("Esta cuenta ya se encuentra con la aplicación correctamente configurada, ya puede obtener sus grupos desde Facebook.");
                    $modalInstance.close(result);
                    
                }
                else if(app_id)
                    if(!$scope.form.verified)
                    {
                        $scope.form.verifying = true;
                        $timeout(function(){verify(fb_id, app_id);}, 10000);
                    }
                else
                    alert("Ocurrió un error en la solicitud, no se obtuvo el resultado esperado, por favor vuelva a intentarlo.");
            })
            .error(function(x) {
                var error = (x && x.error) || 'Error en el servidor';
                alert(error);
            });
    }
    
    $scope.save = function ()
    {
        $scope.saving = true;
        $http.put('publisher/'+publisher_id, {app_id: $scope.form.app_id, app_secret : $scope.form.app_secret})
        .success(function(response)
        {
            if ( !response.success || response.error )
            {
                var msg = response.error && response.error.join(", ");
                $scope.resultmsg = msg || 'Ocurrió un error desconocido al configurar la aplicación de facebook.';
            }
            else
            {
                $modalInstance.close(response.success);
            }
        })
        .error(function(x) {
            var error = (x.data && x.data.error) || 'Error en el servidor';
            $scope.resultmsg = error;
        })
        .then(function(){
            $scope.saving = false;
        });
    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
});

app.controller('ModalChangePasswordCtrl', function ($scope, $modalInstance,  $http,   publisher_id) 
{
    $scope.saving = false;
    
    $scope.new_password = "";
    $scope.new_repassword = "";
    $scope.save = function ()
    {
        if($scope.new_password.length < 6)
        {
            $scope.resultmsg = "La contraseña debe contener al menos 6 caracteres";
            return;
        }
        
        if($scope.new_repassword != $scope.new_password)
        {
            $scope.resultmsg = "Las contraseñas no coinciden";
            return;
        }
        
        $scope.saving = true;
        $http.put('publisher/'+publisher_id, {password: $scope.new_password})
        .success(function(response)
        {
            if ( !response.success || response.error )
            {
                var msg = response.error && response.error.join(", ");
                $scope.resultmsg = msg || 'Ocurrió un error desconocido al configurar la aplicación de facebook.';
            }
            else
            {
                $modalInstance.close(response.success);
            }
        })
        .error(function(x) {
            var error = (x.data && x.data.error) || 'Error en el servidor';
            $scope.resultmsg = error;
        })
        .then(function(){
            $scope.saving = false;
        });
    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
});

app.controller('ModalChangeCanPublishCtrl', function ($scope, $modalInstance,  $http,   publisher_id, activate) 
{
    $scope.saving = false;
    
    $scope.startDate = new Date();
    $scope.startTime = new Date();
    
    
    $scope.now = new Date();
    
    $scope.activate = activate;
    
    $scope.getTomorrowHourExact = function(hour)
    {
        return moment().add(1, 'days').hours(hour).minutes(0).seconds(0).unix();
    }
    
    $scope.getAddMinutesFromNow = function(minutes)
    {
        var now = new Date();
        var future = moment(now).add(minutes, 'minutes');
        return future.unix();
    }
    
    $scope.addMinutes = function(minutes)
    {
        var now = $scope.getStartDateTime();
        var future = moment(now).add(minutes, 'minutes');
        
        $scope.endDate = future.toDate();
        $scope.endTime = future.toDate();
    }
    
    $scope.getStartDateTime = function()
    {
        var day = $scope.startDate.getDate();
        var month = $scope.startDate.getMonth();
        var year = $scope.startDate.getFullYear();
        
        var hour = $scope.startTime.getHours();
        var minute = $scope.startTime.getMinutes();
        
        var dt = new Date(year, month, day, hour, minute, 0, 0);
        return dt;
    }
    
    $scope.getEndDateTime = function()
    {
        var day = $scope.endDate.getDate();
        var month = $scope.endDate.getMonth();
        var year = $scope.endDate.getFullYear();
        
        var hour = $scope.endTime.getHours();
        var minute = $scope.endTime.getMinutes();
        
        var dt = new Date(year, month, day, hour, minute, 0, 0);
        return dt;
    }
    
    $scope.save = function ()
    {
        var time_start = $scope.getStartDateTime().getTime()/1000;
        var time_end = $scope.getEndDateTime().getTime()/1000;
        
        if(time_end <= time_start)
        {
            alert("La fecha y hora de fin debe ser mayor que la fecha y hora de inicio, corrija esto por favor.");
            return;
        }
        
        $scope.saving = true;
        $http.put('publisher/'+publisher_id, {can_publish: activate, from : time_start, until : time_end})
        .success(function(response)
        {
            if ( !response.success || response.error )
            {
                var msg = response.error && response.error.join("\n");
                $scope.resultmsg = msg || 'Ocurrió un error desconocido al configurar las opciones.';
            }
            else
            {
                $modalInstance.close(response.success);
            }
        })
        .error(function(x) {
            var error = (x.data && x.data.error) || 'Error en el servidor';
            $scope.resultmsg = error;
        })
        .then(function(){
            $scope.saving = false;
        });
    };

    $scope.cancel = function () {
        console.log("cancelar");
      $modalInstance.dismiss('cancel');
    };
});