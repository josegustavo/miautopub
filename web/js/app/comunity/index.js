app.controller('ComunityIndexCtrl', ['$scope', '$http','$location', '$filter', '$timeout', '$interval','$q',  '$modal', '$element',
  function(                       $scope,   $http,  $location,   $filter,   $timeout,  $interval,  $q,   $modal, $element) {
    var timeout = null;
    
    $scope.params = [];
    
    $scope.comunity = null;
    $http.get('comunity')
    .success(function(result){
        $scope.comunity = result.success;
    })
    .error(function(err){
        
    });
    
    $scope.publishers = [];
    
    $http.get('publisher', {fields : 'name,image_url,last_publication_time'})
    .success(function(result){
        var success = result.success;
        if(success)
        {
            $scope.publishers = success.data;
        }
    })
    .error(function(err){
        
    });
    
    $scope.saveConfig = function(c)
    {
        var data = {
            time_betwen_pubs : c.time_betwen_pubs,
            oportunity_publish : c.oportunity_publish,
        };
        $scope.save(c, data);
    }
    
    $scope.saveOptions = function(c)
    {
        var data = {
            name : c.name,
            email : c.email,
            whatsapp : c.whatsapp
        };
        
        if(window.loginuser.role == "admin")
        {
            data.wordpress = c.wordpress;
        }
        
        $scope.save(c, data);
    }
    
    $scope.save = function(c,data)
    {
        $http.put('comunity/'+c.id, data)
        .success(function(response){
            var error = response.error;
                angular.forEach(error, function(value){
                    alert(value)
                });
                
                var success = response.success;
                angular.forEach(success, function(value,key){
                    $scope.comunity[key] = value;
                });
        })
        .error(function(response){
            var error = response.error;

            if(!error)
            {
                alert("Ocurrió un error en el servidor al guardar el valor, intentelo luego");
            }
            else
            {
                    alert(error);
            }
        })
    }
    
  }
]);