
app.factory('Entry', function($resource) {
  return $resource('entry/:entryId', {entryId:'@id'}, {update: { method:'PUT' }});
});

app.controller('EntryListCtrl', ['$scope', '$http','$location', '$filter', '$timeout', 'Entry', '$q',  '$modal',
  function(                       $scope,   $http,  $location,   $filter,   $timeout,   Entry,   $q,   $modal) {
    var timeout = null;
    $scope.entries = [];
    $scope.selectedEntry = null;
    
    $scope.editEntry = function(entry)
    {
      $scope.selectedEntry = entry;
    }
    
    $scope.params = {limit:12};
    
    $scope.orderByOptions = [];
    
    var loadEntries = function(params)
    {
        var defered = $q.defer();
        var promise = defered.promise;
        $http.get('entry',{params: params})
        .success(function(result) {
            defered.resolve(result);
        })
        .error(function(err){
            defered.reject(err)
        });
        return promise;
    };
    
    
    $scope.loadEntries = function(params)
    {
        var params_send = angular.copy($scope.params);
        angular.forEach(params, function(value,key){
            params_send[key] = value;
        });
        
        $scope.gettingEntries = true;
        loadEntries(params_send)
        .then(function(result) {
            var success = result.success;
            if(success)
            {
                $scope.entries = success.data;
                $scope.totalItems = success.total_items;
                $scope.totalPages = success.total_pages;
                success.orderByOptions && ($scope.orderByOptions = success.orderByOptions);
                success.params && ($scope.params = success.params);
            }
            else
            {
                alert("Ocurrió un error al cargar las entradas");
            }
            $scope.gettingEntries = false;
        });
        
    }
    window.setTimeout($scope.loadEntries(), 0);
    
    $scope.changeEntryStatus = function(entry)
    {
        $http.post('entry/changeStatus', {entry_id:entry.id, can_publish : entry.can_publish})
        .then(function(response) 
        {
            if ( !response.data.success )
            {
                alert('Error al cambiar de estado la entrada');
                entry.can_publish = !entry.can_publish;
            }
            else
            {
                var result = response.data.success;
                angular.forEach(result, function(value, key) {
                    entry[key] = value;
                })                
            }
        }, function(x) {
            var error = (x.data && x.data.error) || 'Error en el servidor';
            alert(error);
            entry.can_publish = !entry.can_publish;
        });
        
    }
    
    
    $scope.saveEntry = function(time)
    {
      $scope.saving = true;
      var time = time!=null?time:1000;
      if (timeout) {
          $timeout.cancel(timeout);
      }
      timeout = $timeout(function(){
        var clon = angular.copy($scope.selectedEntry);
        clon.$update(function(){
          $scope.saving = false;
          $scope.selectedEntry.gravatar = clon.gravatar;
        }, function(xhr){
           var msg = (xhr && xhr.data && xhr.data.error) || "Ocurrió un error en el servidor al guardar el valor, intentelo luego";
           alert(msg);
           $scope.saving = false;
        })
      }, time);
    };
    
    
    $scope.editPublishList = function(entry, column)
    {
        var modalInstance = $modal.open({
              templateUrl: 'ModalEditEntryListContent.html',
              controller : 'ModalEditEntryListCtrl',
              backdropClass  : 'modal-backdrop h-full',
              resolve: {
                entry: function(){return angular.copy(entry);},
                column : function(){return column},
              }
            });
        modalInstance.result.then(function (result) {
          entry[column] = result[column];
        });
    };
    
    $scope.all_tags = [];
    $http.get('group/getTags')
    .success(function(result) {
        $scope.all_tags = result.success;
    });    
    $scope.editTagsGroups = function(entry)
    {
        var modalInstance = $modal.open({
              templateUrl: 'ModalEditTagsGroupsContent.html',
              controller : 'ModalEditTagsGroupsCtrl',
              backdropClass  : 'modal-backdrop h-full',
              resolve: {
                entry: function(){return angular.copy(entry);},
                all_tags: function(){return $scope.all_tags;},
              }
            });
        modalInstance.result.then(function (result) {
          entry.tags_groups = result.tags_groups;
        });
    };
    
    $scope.publishers = [];
    $http.get('publisher/byKey')
    .success(function(result) {
        $scope.publishers = result.success;
    });
    $scope.editPublishers = function(entry)
    {
        var modalInstance = $modal.open({
              templateUrl: 'ModalEditPublishersContent.html',
              controller : 'ModalEditPublishersCtrl',
              backdropClass  : 'modal-backdrop h-full',
              resolve: {
                entry: function(){return angular.copy(entry);},
                all_publishers: function(){return $scope.publishers;},
              }
            });
        modalInstance.result.then(function (result) {
          entry.publishers = result.publishers;
        });
    };
    
    $scope.createEntry = function()
    {
        var modalInstance = $modal.open({
              templateUrl: 'ModalCreateEntryContent.html',
              controller : 'ModalCreateEntryCtrl',
              backdropClass  : 'modal-backdrop h-full',
              resolve: {
              }
            });
        modalInstance.result.then(function (result) {
          $scope.params = {filter:'cant_publish'};
          $scope.loadEntries();
        });
    }
    
    $scope.deleteEntry = function(entry){
        if(confirm("¿Realmente desea eliminar esta entrada?"))
        {
            entry.deleting = true;
            
            $http.delete('entry/'+entry.id)
            .success(function(data, status, headers, config){
                $scope.loadEntries();
            })
            .error(function(data, status, headers, config){
                data.error && alert(data.error) || alert("Ocurrió un error en el servidor al eliminar la entrada, intentelo luego");
                entry.deleting = false;
            })
        }
    }
    
    $scope.changeEntryPassword = function(entry)
    {
      if($scope.new_repassword === $scope.new_password)
      {
        $scope.selectedEntry.password = $scope.new_repassword;
        $scope.saveEntry(0);
        $scope.new_repassword = "";
        $scope.new_password = "";
      }
      else
      {
        alert('Las contraseñas no coinciden');
      }
    }
    
    var requireEntryPermission = function(entry, success)
    {
        FB.init({appId : entry.app_id, cookie : true, xfbml : true, version : 'v2.2'});
        FB.login(function(response)
            {
                if(response.authResponse && response.authResponse.accessToken)
                {
                    var token = response.authResponse.accessToken;
                    var token_user_id = response.authResponse.userID;
                    $http.post('entry/setTokenFacebook', {user_id : entry.id,token:token,token_user_id:token_user_id})
                    .then(function(response)
                    {
                        if ( !response.data.success )
                        {
                            alert('El token es inválido para el usuario');
                        }
                        else
                        {
                            success && success.constructor && success.call && success.apply && success(response);
                        }
                    });                    
                }
            },
            {scope: 'public_profile,email,read_stream,publish_actions,user_groups,user_likes,manage_pages'}
        );
    }
    
    $scope.updateEntryFromFacebook = function(entry)
    {
        if($scope.updatingFromFacebook)return;
        
        if(entry.app_id && entry.app_secret)
        {
            $scope.updatingFromFacebook = true;
            $http.post('entry/updateFromFacebook', {user_id : entry.id})
            .then(function(response) {
                if ( !response.data.success )
                    alert('Error solicitar la carga datos desde Facebook');
                else
                {
                    $scope.selectedEntry.groups = response.data.success.groups;
                    $scope.editEntry($scope.selectedEntry);
                }
                $scope.updatingFromFacebook = false;
            }, function(x) {
                var error = (x.data && x.data.error) || 'Error en el servidor';
                if(error == 'facebook_login_required')
                {
                    alert("Aun no se ha consedido los permisos de Facebook. \nSe va a solicitar permisos para conectarse a Facebook, debe iniciar sesión con la cuenta del usuario del cual quiere actualizar su información.");
                    requireEntryPermission(entry, function(){
                        alert("Permisos correctamente, vuelva a solicitar la actualización por facebook")
                    })
                }
                else
                {
                    alert(error);
                }
                $scope.updatingFromFacebook = false;
            });
                        
            
        }
        else
        {
            alert("Para dar permiso de publicación se necesita el app_id y el app_secret.")
        }
    }
    
  }]
);

app.controller('ModalEditEntryListCtrl', function ($scope, $modalInstance,  $state,   $filter, $http,  entry, column) 
{
    
    switch (column)
    {
        case 'publish_messages' : 
        {
            $scope.type = 'textarea';
            $scope.type_title = "Mensajes";
            break;
        }
        case 'publish_pictures' : 
        {
            $scope.type = 'input';
            $scope.type_title = "Imágenes";
            break;
        }
        case 'publish_names' : 
        {
            $scope.type = 'input';
            $scope.type_title = "Títulos";
            break;
        }
        case 'publish_captions' : 
        {
            $scope.type = 'input';
            $scope.type_title = "Leyendas";
            break;
        }
        case 'publish_descriptions' : 
        {
            $scope.type = 'textarea';
            $scope.type_title = "Descripciones";
            break;
        }
    }
    
    $scope.list_values = entry[column]?entry[column]:[];
    
    //var initial_values = angular.copy($scope.list_values);
    
    $scope.save = function () {
        $scope.saving = true;
        
        $http.post('entry/changePublishList', {entry_id:entry.id, list : column, value : $scope.list_values})
        .then(function(response) 
        {
            if ( !response.data.success )
            {
                $scope.resultmsg = ('Ocurrió un error desconocido al guardar la lista');
            }
            else
            {
                var result = response.data.success;
                $modalInstance.close(result);
            }
            $scope.saving = false;
        }, function(x) {
            var error = (x.data && x.data.error) || 'Error en el servidor';
            $scope.resultmsg = error;
            $scope.saving = false;
        });
    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
});


app.controller('ModalEditTagsGroupsCtrl', function ($scope, $modalInstance,  $state,   $filter, $http,  entry, all_tags) 
{
    $scope.tags = all_tags;
    
    $scope.tags_groups = entry.tags_groups?entry.tags_groups:{rule:'all', tags :[]};
    
    if(!$scope.tags_groups.rule)$scope.tags_groups.rule='all';
    if(!$scope.tags_groups.tags)$scope.tags_groups.tags= [];
    
    //var initial_values = angular.copy($scope.list_values);
    
    $scope.save = function () {
        $scope.saving = true;
        
        $http.put('entry/'+entry.id, {tags_groups : $scope.tags_groups})
        .success(function(response) 
        {
            if ( !response.success )
            {
                $scope.resultmsg = ('Ocurrió un error desconocido al guardar la lista');
            }
            else
            {
                $modalInstance.close(response.success);
            }
            $scope.saving = false;
        }).error(function(x) {
            var error = (x && x.error) || 'Error en el servidor';
            $scope.resultmsg = error;
        }).then(function(){
            $scope.saving = false;
        });
    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
});
app.controller('ModalEditPublishersCtrl', function ($scope, $modalInstance,  $state,   $filter, $http,  entry, all_publishers) 
{
    $scope.all_publishers = all_publishers;
    
    $scope.publishers = entry.publishers?entry.publishers:{rule:'all', list :[]};
    
    if(!$scope.publishers.rule)$scope.publishers.rule='all';
    if(!$scope.publishers.list)$scope.publishers.list= [];
    
    angular.forEach($scope.publishers.list, function(elem, index){
        $filter('filter')($scope.all_publishers, function(el,ind){
            if(el.id == elem.id) 
            {
                $scope.publishers.list[index] = el;
            }
        })
    });
    
    $scope.save = function () {
        $scope.saving = true;
        
        $http.post('entry/changePublishers', {entry_id:entry.id, publishers : $scope.publishers})
        .then(function(response)
        {
            if ( !response.data.success )
            {
                $scope.resultmsg = ('Ocurrió un error desconocido al guardar la lista');
            }
            else
            {
                var result = response.data.success;
                $modalInstance.close(result);
            }
            $scope.saving = false;
        }, function(x) {
            var error = (x.data && x.data.error) || 'Error en el servidor';
            $scope.resultmsg = error;
            $scope.saving = false;
        });
    };
    
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
});

app.controller('ModalCreateEntryCtrl', function ($scope, $modalInstance,  $state,   $filter, $http) 
{
    $scope.loginFromLink = false;
    var entry = $scope.entry = {};
    
    $scope.loadFromLink = function() {
        $scope.loginFromLink = true;
        $http.post('entry/loadDataFromLink', {link:entry.link})
        .success(function(data, status, headers, config){
            var og = data.success;
            if(og)
            {
                entry.og_id = og.og_id;
                entry.title = og.og_title;
                entry.description = og.og_description;
                entry.image = og.og_image;
            }
            $scope.loginFromLink = false;
        })
        .error(function(data, status, headers, config){
            data.error && alert(data.error);
            $scope.loginFromLink = false;
        });
    }
    
    $scope.save = function () {
        $scope.saving = true;
        
        $http.post('entry', entry)
        .success(function(data, status, headers, config)
        {
            if ( !data.success )
            {
                $scope.resultmsg = ('Ocurrió un error desconocido al guardar la lista');
            }
            else
            {
                var result = data.success;
                $modalInstance.close(result);
            }
            $scope.saving = false;
        })
        .error(function(data, status, headers, config) {
            var error = (data && data.error) || 'Error en el servidor';
            $scope.resultmsg = error;
            $scope.saving = false;
        });
    };
    
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
});