
app.factory('Group', function($resource) {
  return $resource('group/:groupId', {groupId:'@id'}, {update: { method:'PUT' }});
});

app.controller('GroupListCtrl', ['$scope', '$http','$location', '$filter', '$timeout', 'Group', '$q',  '$modal',
  function(                       $scope,   $http,  $location,   $filter,   $timeout,   Group,   $q,   $modal) {
    var timeout = null;
    $scope.groups = [];
    $scope.selectedGroup = null;
    
    $scope.editGroup = function(group)
    {
      $scope.selectedGroup = group;
    }
    
    $scope.params = {limit:20};
    
    $scope.orderByOptions = [];
    
    var loadEntries = function(params)
    {
        var defered = $q.defer();
        var promise = defered.promise;
        $http.get('group',{params: params})
        .success(function(result) {
            defered.resolve(result);
        })
        .error(function(err){
            defered.reject(err)
        });
        return promise;
    };
    
    
    $scope.loadEntries = function(params)
    {
        var params_send = angular.copy($scope.params);
        angular.forEach(params, function(value,key){
            params_send[key] = value;
        });
        
        $scope.gettingEntries = true;
        loadEntries(params_send)
        .then(function(result) {
            var success = result.success;
            if(success)
            {
                $scope.groups = success.data;
                $scope.totalItems = success.total_items;
                $scope.totalPages = success.total_pages;
                success.orderByOptions && ($scope.orderByOptions = success.orderByOptions);
                success.params && ($scope.params = success.params);
            }
            else
            {
                alert("Ocurrió un error");
            }
            $scope.gettingEntries = false;
        });
        
    }
    window.setTimeout($scope.loadEntries(), 0);
    
    $scope.changeGroupsStatus = function(groups, can_publish,reason)
    {
        var group_ids = [];
        angular.forEach(groups, function(group){
            group_ids.push(group.id);
        });
        $http.post('group/changeStatus', {group_ids:group_ids, can_publish : can_publish, reason : reason})
        .then(function(response) 
        {
            if ( !response.data.success )
            {
                alert('Error al cambiar de estado los grupos');
            }
            else
            {
                var result = response.data.success;
                angular.forEach(groups, function(group) {
                    group.can_publish = result.can_publish;
                    group.reason = result.reason;
                })                
            }
        }, function(x) {
            var error = (x.data && x.data.error) || 'Error en el servidor';
            alert(error);
        });
        
    }
    
    var sendAddGroupsCategory = function(groups)
    {
        var group_ids = [];
        angular.forEach(groups, function(group){
            group_ids.push(group.id);
        });
        
        var s = (groups.length>1?'s':'');
        var al = (groups.length==1?'al':'a los');
        var msg = "Ingrese la categoría que agregará "+al+ " " + (groups.length>1?groups.length:'') + " grupo" + s  + " seleccionado"+s+".";
        var category = prompt(msg);
        if (category != null)
        {
            $http.post('group/addCategory', {group_ids:group_ids, tag : category})
            .then(function(response) 
            {
                if ( !response.data.success )
                {
                    alert('Error al agregar la categoría');
                }
                else
                {
                    var results = response.data.success;
                    angular.forEach(results, function(result) 
                    {
                        var group = $filter('filter')(groups, {id : result.id})[0];
                        group.tags = result.tags;
                    })
                }
            }, function(x) {
                var error = (x.data && x.data.error) || 'Error en el servidor';
                alert(error);
            });
        }
    };
    
    $scope.addGroupsCategory = function()
    {
        var groups = $scope.getSelectedsGroups();
        if(groups.length)
        {
            sendAddGroupsCategory(groups);
        }
        else
        {
            alert("Para agregar una categoría, debe seleccionar al menos un grupo.");
        }
    };
    
    $scope.addGroupCategory = function(group)
    {
        sendAddGroupsCategory([group]);
    };
    
    $scope.removeGroupCategory = function(tag, group)
    {
        $http.post('group/removeCategory', {group_id: group.id, tag : tag})
        .success(function(response) 
        {
            if ( !response.success )
            {
                alert('Error al remover la categoría');
            }
            else
            {
                var results = response.success;
                group.tags = results.tags;
            }
        })
        .error(function(x) {
            var error = (x && x.error) || 'Error en el servidor';
            alert(error);
        });
    }
    
    $scope.showCategoriesEdit = function()
    {
        
    };
    
    $scope.selectAllGroups = function()
    {
        var select = true;
        $scope.isAllSelected() && (select=false);
        angular.forEach($scope.groups,function(group){
            group.selected=select;
        })
    }
    
    $scope.isNoneSelected = function()
    {
        var count = $scope.getSelectedsGroups().length;
        return (count==0)
    }
    $scope.isAnySelected = function()
    {
        var count = $scope.getSelectedsGroups().length;
        return (count > 0 && ($scope.groups.length != count))
    }
    $scope.isAllSelected = function()
    {
        var count = $scope.getSelectedsGroups().length;
        return ($scope.groups.length > 0 && ($scope.groups.length == count))
    }
    
    $scope.getSelectedsGroups = function(onlyIds)
    {
        onlyIds = onlyIds || false;
        var result = [];
        angular.forEach($scope.groups,function(group){
            (group.selected==true) && result.push(onlyIds?group.id:group);
        });
        return result;
    }
    
    $scope.disableSelecteds = function()
    {
        var selecteds = $scope.getSelectedsGroups();
        
        var groups = [];
        angular.forEach(selecteds, function(group){
            group.can_publish && groups.push(group);
        });
        
        if(groups.length)
        {
            var msg = "¿Está seguro que desea deshabilitar " + groups.length + " grupo" + (groups.length>1?'s':'')  + "?\nIngrese un motivo.";
            var reason = prompt(msg);
            if (reason != null) {
                $scope.changeGroupsStatus(groups,false,reason);
            }
        }
        else
        {
            alert("Para deshabilitar grupos, seleccione al menos uno.");
        }
    }
    
    $scope.enableSelecteds = function()
    {
        var selecteds = $scope.getSelectedsGroups();
        
        var groups = [];
        angular.forEach(selecteds, function(group){
            !group.can_publish && groups.push(group);
        });
        
        if(groups.length)
        {
            $scope.changeGroupsStatus(groups,true);
        }
    }
    
    $scope.saveGroup = function(time)
    {
      $scope.saving = true;
      var time = time!=null?time:1000;
      if (timeout) {
          $timeout.cancel(timeout);
      }
      timeout = $timeout(function(){
        var clon = angular.copy($scope.selectedGroup);
        clon.$update(function(){
          $scope.saving = false;
          $scope.selectedGroup.gravatar = clon.gravatar;
        }, function(xhr){
           var msg = (xhr && xhr.data && xhr.data.error) || "Ocurrió un error en el servidor al guardar el valor, intentelo luego";
           alert(msg);
           $scope.saving = false;
        })
      }, time);
    };
    
    
    $scope.editPublishList = function(group, column)
    {
        var modalInstance = $modal.open({
              templateUrl: 'ModalEditGroupListContent.html',
              controller : 'ModalEditGroupListCtrl',
              backdropClass  : 'modal-backdrop h-full',
              resolve: {
                group: function(){return angular.copy(group);},
                column : function(){return column},
              }
            });
        modalInstance.result.then(function (result) {
          group[column] = result[column];
        });
    };

    $scope.createGroup = function()
    {
        if(confirm("Se va a agregar una nueva cuenta desde Facebook, si tiene una sesión de facebook abierta se cerrará para ingresar con la nueva cuenta a agregar."))
        {
            var loginWithFacebook = function()
            {
                FB.login(
                    function(response)
                    {
                        if (response.authResponse && response.status == 'connected')
                        {
                            var signedRequest = response.authResponse.signedRequest;
                            var token = response.authResponse.accessToken;
                            $http.post('group', {signed_request: signedRequest, token : token})
                            .then(function(response) {
                                if ( !response.data.success )
                                {
                                    alert('Error al crear el usuario');
                                }
                                else
                                {
                                    var group = response.data.success;
                                    $scope.groups.push(group);
                                    $scope.editGroup(group);
                                }
                            }, function(x) {
                                var error = (x.data && x.data.error) || 'Error en el servidor';
                                alert(error);
                            });
                        } else {
                            alert("Inicie sesión en facebook para continuar");
                        }
                    },
                    {scope: 'public_profile,email'}
                );
            };
            
            FB.getLoginStatus(function(response)
            {
                if(response.status === 'connected')
                {
                    FB.logout(function(response) {
                        loginWithFacebook();
                    });
                }
                else
                {
                    loginWithFacebook();
                }
            });
            
        }
    }
    
    $scope.deleteGroup = function(group){
      if(confirm("¿Realmente desea eliminar este usuario?"))
      {
        var is_selected = (group == $scope.selectedGroup);
        group.$delete(function(){
          $scope.groups.splice($scope.groups.indexOf(group), 1);
          if(is_selected)
          {
            $scope.selectedGroup = null;
          }
        }, function(xhr){
          var msg = (xhr && xhr.data && xhr.data.error) || "Ocurrió un error en el servidor al eliminar el usuario, intentelo luego";
          alert(msg)
        })
      }
    }
    
    $scope.changeGroupPassword = function(group)
    {
      if($scope.new_repassword === $scope.new_password)
      {
        $scope.selectedGroup.password = $scope.new_repassword;
        $scope.saveGroup(0);
        $scope.new_repassword = "";
        $scope.new_password = "";
      }
      else
      {
        alert('Las contraseñas no coinciden');
      }
    }
    
    var requireGroupPermission = function(group, success)
    {
        FB.init({appId : group.app_id, cookie : true, xfbml : true, version : 'v2.2'});
        FB.login(function(response)
            {
                if(response.authResponse && response.authResponse.accessToken)
                {
                    var token = response.authResponse.accessToken;
                    var token_user_id = response.authResponse.userID;
                    $http.post('group/setTokenFacebook', {user_id : group.id,token:token,token_user_id:token_user_id})
                    .then(function(response)
                    {
                        if ( !response.data.success )
                        {
                            alert('El token es inválido para el usuario');
                        }
                        else
                        {
                            success && success.constructor && success.call && success.apply && success(response);
                        }
                    });                    
                }
            },
            {scope: 'public_profile,email,read_stream,publish_actions,user_groups,user_likes,manage_pages'}
        );
    }
    
    $scope.updateGroupFromFacebook = function(group)
    {
        if($scope.updatingFromFacebook)return;
        
        if(group.app_id && group.app_secret)
        {
            $scope.updatingFromFacebook = true;
            $http.post('group/updateFromFacebook', {user_id : group.id})
            .then(function(response) {
                if ( !response.data.success )
                    alert('Error solicitar la carga datos desde Facebook');
                else
                {
                    $scope.selectedGroup.groups = response.data.success.groups;
                    $scope.editGroup($scope.selectedGroup);
                }
                $scope.updatingFromFacebook = false;
            }, function(x) {
                var error = (x.data && x.data.error) || 'Error en el servidor';
                if(error == 'facebook_login_required')
                {
                    alert("Aun no se ha consedido los permisos de Facebook. \nSe va a solicitar permisos para conectarse a Facebook, debe iniciar sesión con la cuenta del usuario del cual quiere actualizar su información.");
                    requireGroupPermission(group, function(){
                        alert("Permisos correctamente, vuelva a solicitar la actualización por facebook")
                    })
                }
                else
                {
                    alert(error);
                }
                $scope.updatingFromFacebook = false;
            });
                        
            
        }
        else
        {
            alert("Para dar permiso de publicación se necesita el app_id y el app_secret.")
        }
    }
    
  }]
);

app.controller('ModalEditGroupListCtrl', function ($scope, $modalInstance,  $state,   $filter, $http,  group, column) 
{
    
    switch (column)
    {
        case 'publish_messages' : 
        {
            $scope.type = 'textarea';
            $scope.type_title = "Mensajes";
            break;
        }
        case 'publish_pictures' : 
        {
            $scope.type = 'input';
            $scope.type_title = "Imágenes";
            break;
        }
        case 'publish_names' : 
        {
            $scope.type = 'input';
            $scope.type_title = "Títulos";
            break;
        }
        case 'publish_captions' : 
        {
            $scope.type = 'input';
            $scope.type_title = "Leyendas";
            break;
        }
        case 'publish_descriptions' : 
        {
            $scope.type = 'textarea';
            $scope.type_title = "Descripciones";
            break;
        }
    }
    
    $scope.list_values = group[column]?group[column]:[];
    
    //var initial_values = angular.copy($scope.list_values);
    
    $scope.save = function () {
        $scope.saving = true;
        
        $http.post('group/changePublishList', {group_id:group.id, list : column, value : $scope.list_values})
        .then(function(response) 
        {
            if ( !response.data.success )
            {
                $scope.resultmsg = ('Ocurrió un error desconocido al guardar la lista');
            }
            else
            {
                var result = response.data.success;
                $modalInstance.close(result);
            }
            $scope.saving = false;
        }, function(x) {
            var error = (x.data && x.data.error) || 'Error en el servidor';
            $scope.resultmsg = error;
            $scope.saving = false;
        });
    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
});

app.controller('ModalEditCategoriesGroupCtrl', function ($scope, $modalInstance,  $state,   $filter, $http,  group) 
{
    
    $scope.tags = group.tags?group.tags:{};
    
    
    $scope.deleteTag = function(tag)
    {
        
    };
    
    $scope.addTag = function(tag)
    {
        
    };
    
    $scope.save = function () {
        $scope.saving = true;
        
        $http.post('group/xxxxxxxx', {group_id:group.id, list : column, value : $scope.list_values})
        .then(function(response) 
        {
            if ( !response.data.success )
            {
                $scope.resultmsg = ('Ocurrió un error desconocido al guardar la lista');
            }
            else
            {
                var result = response.data.success;
                $modalInstance.close(result);
            }
            $scope.saving = false;
        }, function(x) {
            var error = (x.data && x.data.error) || 'Error en el servidor';
            $scope.resultmsg = error;
            $scope.saving = false;
        });
    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
});

