app.controller('PublicationListCtrl', ['$scope', '$http','$location', '$filter', '$timeout', '$interval','$q',  '$modal', '$element',
  function(                       $scope,   $http,  $location,   $filter,   $timeout,  $interval,  $q,   $modal, $element) {
    var timeout = null;
    
    $scope.params = [];
    
    $scope.comunity = null;
    $http.get('comunity')
    .success(function(result){
        $scope.comunity = result.success;
    })
    .error(function(err){
        
    });
    
    $scope.publishers = [];
    
    $http.get('publisher', {fields : 'name,image_url,last_publication_time'})
    .success(function(result){
        var success = result.success;
        if(success)
        {
            $scope.publishers = success.data;
        }
    })
    .error(function(err){
        
    });
    
    $scope.publications = [];
    
    var loadPublications = function(params)
    {
        var defered = $q.defer();
        var promise = defered.promise;
        $http.get('publication',{params: params})
        .success(function(result) {
            defered.resolve(result);
        })
        .error(function(err){
            defered.reject(err)
        });
        return promise;
    };
    
    
    $scope.loadPublications = function(params, replace)
    {
        //Si replace es -1. se agrega antes, si es +1 se agrega después
        //si replace es null o false, se reemplaza;
        replace = replace || 0; 
        var params_send = angular.copy($scope.params);
        angular.forEach(params, function(value,key){
            params_send[key] = value;
        });
        
        $scope.gettingPublications = true;
        loadPublications(params_send)
        .then(function(result) {
            var success = result.success;
            if(success)
            {
                if(replace > 0)
                {
                    $scope.publications = $scope.publications.concat(success.data);
                }
                else if(replace < 0)
                {
                    $scope.publications = success.data.concat($scope.publications);
                }
                else
                {
                    $scope.publications = success.data;
                }
                
                success.params && ($scope.params = success.params);
            }
            else
            {
                alert("Ocurrió un error al cargar las publicaciones");
            }
            $scope.gettingPublications = false;
        });
        
    }
    $timeout($scope.loadPublications, 0);
    
    var stopTime = $interval(function(){
        if($scope.publications && $scope.publications.length)
        {
            var first = $scope.publications[0];
            $scope.loadPublications({before : first.published_time}, -1);
        }
    }, 30000);
    $element.on('$destroy', function() {
            $interval.cancel(stopTime);
    });
    
    $scope.loadMore = function()
    {
        if($scope.publications && $scope.publications.length)
        {
            var last = $scope.publications[$scope.publications.length -1];
            $scope.loadPublications({after : last.published_time}, 1);
        }
    }
  }
]);