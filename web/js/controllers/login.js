'use strict';

/* Controllers */
  // signin controller
app.controller('SigninFormController', ['$scope', '$http', '$window', '$state', 
    function(                            $scope,   $http,   $window,   $state) {
     
    var goToHome = function()
    {
        $state.go('/');
    };
    if($window.loginuser && $window.loginuser.id)
    {
        goToHome();
        return;
    }
    
  
    $window.fbAsyncInit = function() {
        FB.init({appId : '1642272955991009', cookie : true, xfbml : true, version : 'v2.2'});
        /*FB.getLoginStatus(function(response) {statusChangeCallback(response);});*/
    };
    
    (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
  
    
    var statusChangeCallback = function(response) {
        
        if (response.status === 'connected') {
          // Logged into your app and Facebook.
          
        } else if (response.status === 'not_authorized') {
          // The person is logged into Facebook, but not your app.
          document.getElementById('status').innerHTML = 'Please log ' +
            'into this app.';
        } else {
          // The person is not logged into Facebook, so we're not sure if
          // they are logged into this app or not.
          document.getElementById('status').innerHTML = 'Please log ' +
            'into Facebook.';
        }
    }
    
    $scope.fb_login = function(){
        FB.login(
            function(response)
            {
                if (response.authResponse && response.status == 'connected')
                {
                    var signedRequest = response.authResponse.signedRequest;
                    var token = response.authResponse.accessToken;
                    $http.post('login', {signed_request: signedRequest, token : token})
                    .then(function(response) {
                      if ( !response.data.success ) {
                        $scope.authError = 'Error al iniciar sesión';
                      }else{
                        $scope.app.loginuser = response.data.success;
                        goToHome();
                      }
                    }, function(x) {
                        var error = (x.data && x.data.error) || 'Error en el servidor';
                      $scope.authError = error;
                    });
                } else {
                    $scope.authError = "Error al iniciar sesión en facebook."
                }
            },
            {scope: 'public_profile,email'}
        );
    },
    
    $scope.checkLoginState = function() {
      FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
      });
    }
    
    $scope.user = {};
    $scope.authError = null;
    $scope.login = function() 
    {
        $scope.authError = null;
        // Try to login
        $http.post('login', {username: $scope.user.username, password: $scope.user.password})
        .then(function(response) {
          if ( !response.data.success ) {
            $scope.authError = 'Usuario o contraseña incorrecto';
          }else{
                $scope.app.loginuser = response.data.success;
                goToHome();
          }
        }, function(x) {
            var error = (x.data && x.data.error) || 'Error en el servidor';
            $scope.authError = error;
        });
    };
  }])
;