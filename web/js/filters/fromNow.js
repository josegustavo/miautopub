'use strict';

/* Filters */
// need load the moment.js to use this filter. 
angular.module('app')
  .filter('fromNow', function() {
    return function(timestamp) {
      moment && moment.locale('es');
      if(timestamp>0)
        return moment.unix(timestamp).fromNow();
      else
        return 'Indefinido';  
    }
  });