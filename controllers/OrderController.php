<?php
namespace app\controllers;

use Yii;
use app\helpers\Controller;
use app\helpers\Functions;
use app\helpers\EntryHelper;
use app\helpers\PublisherHelper;
use app\helpers\OrderHelper;
use app\helpers\Meta;
use app\models\Publisher;
use app\models\Publication;
use app\models\Group;
use app\models\Comunity;
use app\models\ComunityMeta;

class OrderController extends Controller
{
    public function actionIndex()
    {
        $isAdmin = PublisherHelper::isAdmin();
        
        $params = ['isAdmin' => $isAdmin];
      
        $options = [];
        
        if(($status = Yii::$app->request->get("status"))&& (in_array($status, ['paid','pending','canceled'])))
          $options['filter_status'] = $status ;
          
        if(($amount =  Yii::$app->request->get("amount")) && ($amount == 'aboveZero'))
          $options['above_zero'] = TRUE;
          
        $orders = OrderHelper::getOrders($options);
        
        $params['orders'] = [];
        foreach ($orders as $order)
        {
            $order['status_name'] = Functions::getStatusOrderName($order['status']);
            $params['orders'] []= $order;
        }
            
        
        return $this->render('/site/order', $params);
    }
    
    public function actionView($number)
    {
        $order = OrderHelper::getOrder($number);
        if(!$order)
        {
            Yii::$app->getSession()->setFlash('authError', "No se encontró la orden solicitada #".$number);
            return $this->redirect('/order');
        }
        
        $params = ['comunity' => $order['comunity']['name'], 'order' => $order];
        
        $request = Yii::$app->request;
        if($action = $request->get('action'))
        {
            if($action == 'cancel')
            {
                OrderHelper::cancelOrder($number);
            }
            return $this->redirect('/order');
        }
        
        
        return $this->render('/site/orderview', $params);
    }
    
    public function actionPay($number)
    {
        $request = Yii::$app->request;
        $order = OrderHelper::getOrder($number);
        if(!$order)
        {
            Yii::$app->getSession()->setFlash('authError', "No se encontró la orden solicitada #".$number);
            return $this->redirect('/order');
        }
        $comunity_name = $order['comunity']['name'];
        
        $isAdmin = PublisherHelper::isAdmin();
        
        if($request->isPost)
        {
            $numberOperation = $request->post('NumberOpeartion');
            if($numberOperation)
            {
                $validate = OrderHelper::validateOrder($number, $numberOperation);
                if($validate) $order = OrderHelper::getOrder($number);
                
                Yii::$app->getSession()->setFlash('authMessage', "Se ha guardado el número de operación, por favor espere la confirmación y procesamiento de la orden.");
            }
            
            $paypal_return = $request->post('txn_id');
            if($paypal_return)
            {
                $validate = OrderHelper::validatePaypal($request->post());
                if($validate) $order = OrderHelper::getOrder($number);
            }
        }
        
        $params = [
            'isAdmin' => $isAdmin, 
            'comunity' => $comunity_name,
            'order' => $order,
            'paypal_cfg' => \Yii::$app->params['paypal']
            ];
        
        return $this->render('/site/pay', $params);
    }
    
    public function actionAprove($number)
    {
        $order = OrderHelper::getOrder($number);
        if(!$order)
        {
            Yii::$app->getSession()->setFlash('authError', "No se encontró la orden solicitada #".$number);
            return $this->redirect('/order');
        }
        
        $isAdmin = PublisherHelper::isAdmin();
        
        if(!$isAdmin)
        {
            Yii::$app->getSession()->setFlash('authError', "No tiene permisos para realizar esta operación.");
            return $this->redirect('/order/'.$number.'/pay');
        }
        
        OrderHelper::aproveOrder($number);
        
        return $this->redirect('/order/'.$number);
        
    }
    
}