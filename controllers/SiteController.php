<?php

namespace app\controllers;

use Yii;
use app\helpers\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\helpers\UserIdentity;
use app\helpers\Functions;

class SiteController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionAdmin()
    {
        return $this->render('admin');
    }
    
    public function actionSample()
    {
        $groups = \app\models\Group::find()->all();
        
        foreach ($groups as $group)
        {
            
        }
        
        return;
    }
    
    public function actionLogin()
    {
        //var_dump(Yii::$app->request->post());exit;
        $request = Yii::$app->request;
        if($request->isPost && ($data = $request->post()))
        {
            $error = NULL;
            $username = NULL;
            if(isset($data['username']) && isset($data['password']) && $data['username'] && $data['password'])
            {
                try
                {
                    $username = $data['username'];
                    $password = $data['password'];
                    $identity = new UserIdentity();

                    if($user = $identity->authenticate($username, $password))
                    {
                        Yii::$app->user->login($user);
                        
                        
                        if($url_ret = urldecode($request->get('return')))
                        {
                            return $this->redirect($url_ret);
                        }
                        else
                        {
                            return $this->redirect('/admin');
                        }
                    }
                    else
                    {
                        ($identity->errorMessage && ($error = $identity->errorMessage))
                                || ($error = "Usuario no registrado o contraseña incorrecta");
                    }
                }
                catch (\Exception $ex )
                {
                    $error = "Nombre de usuario o contraseña incorrecta";
                }
            }
            else
            {
                $error = "Nombre de usuario y contraseña requeridos.";
            }
            
            if($error)
            {
                return $this->render('login', ['authError' => $error, 'username' => $username]);
            }
        }
        else
        {
            if(Yii::$app->user->isGuest)
            {
                $request = Yii::$app->request;
                if($code = $request->get('code'))
                {
                    $error = "";
                    
                    $FbLoginAppId = Yii::$app->params['FbLoginAppId'];
                    $FbLoginAppSecret = Yii::$app->params['FbLoginAppSecret'];
                    $url = 'https://graph.facebook.com/v2.3/oauth/access_token?'
                            . 'client_id='.$FbLoginAppId 
                            . '&redirect_uri=' . $request->hostInfo . '/' . $request->pathInfo
                            . '&client_secret=' . $FbLoginAppSecret
                            . '&code=' . $code;
                    $content = Functions::file_get_contents($url);
                    if($content && ($data = json_decode($content, TRUE)) && isset($data['access_token']) && ($access_token = $data['access_token']))
                    {
                        $url = 'https://graph.facebook.com/v2.3/me?fields=id,name,email&access_token=' . $access_token;
                        $me = json_decode(Functions::file_get_contents($url), TRUE);
                        
                        if(isset($me['id']) && ($user_id = $me['id']) && isset($me['email']) && ($email = $me['email']))
                        {
                            try
                            {
                                $user = (new UserIdentity())->authenticateByFacebook($user_id, $email);
                                Yii::$app->user->login($user);
                                
                                if($url_ret = urldecode($request->get('return')))
                                {
                                    return $this->redirect($url_ret);
                                }
                                else
                                {
                                    return $this->redirect('/admin');
                                }
                            }
                            catch (\Exception $ex) 
                            {
                                $error = "Ocurrió un error al iniciar sesión " . $ex->getMessage();
                                $this->debug = [$ex->getMessage(), $ex->getFile(), $ex->getLine()];
                            }
                        }
                        else
                        {
                            $error = "No se pudo obtener los datos desde facebook";
                        }
                    }
                    if($error)
                        Yii::$app->getSession()->setFlash('authError', $error);
                    return $this->redirect('login');
                }
                else
                {
                    return $this->render('login');
                }
            }
            else
            {
                return $this->redirect('/');
            }
        }
    }
    
    

    public function actionLogout()
    {
        Yii::$app->user->logout();
        $this->redirect(Yii::$app->homeUrl);
    }
    
    public function actionSignup()
    {
        $request = Yii::$app->request;
        if($request->isPost)
        {
            $user_id = $request->post('user_id');
            $name = $request->post('name');
            $email = $request->post('email');
            $access_token = $request->post('access_token');
            if(!$user_id || !$name || !$email || !$access_token)
            {
                Yii::$app->getSession()->setFlash('error', 'Necesita autorizar a la aplicación para registrarse.');
            }
                        
            $comunity_name = $request->post('comunity_name');
            Yii::$app->getSession()->setFlash('comunity_name', $comunity_name);
            
            $user = ['id' => $user_id, 'name' => $name, 'email' => $email, 'access_token' => $access_token];
            Yii::$app->getSession()->setFlash('user', $user);
            
            $password = $request->post('password');
            
            //Registrar la comunidad
            $transaction = Yii::$app->db->beginTransaction();
            try 
            {
                $comunity = \app\helpers\ComunityHelper::create(['name' => $comunity_name]);
                
                $url = 'https://graph.facebook.com/v2.3/me/picture?redirect=false&access_token='.$access_token;
                $data = json_decode(Functions::file_get_contents($url), TRUE);
                $image_url = (isset($data['data']) && isset($data['data']['url']))?$data['data']['url']:"";
                
                $user_params = [
                    'id' => $user_id,
                    'comunity_id' => $comunity->id,
                    'username' => $email,
                    'name' => $name,
                    'email' => $email,
                    'image_url' => $image_url,
                    'password' => $password,
                    'can_login' => TRUE,
                    'role' => 'user',
                ];
                
                $publisher = \app\helpers\PublisherHelper::create($user_params);
                $transaction->commit();
                
                $identity = new UserIdentity();

                if($user = $identity->authenticate($email,$password))
                {
                    Yii::$app->user->login($user);
                    return $this->redirect('/admin');
                }
                else
                {
                    ($identity->errorMessage && ($error = $identity->errorMessage))
                            || ($error = "Ocurrió un error al iniciar sesión, vuelva a intentarlo luego");
                    throw new \Exception($error, 666);
                }
            }
            catch (\Exception $ex) 
            {
                $transaction->rollBack();
                if($ex->getCode() == 666)
                    Yii::$app->getSession()->setFlash('error', $ex->getMessage());
                else
                    Yii::$app->getSession()->setFlash('error', $ex->getMessage());
            }
            return $this->redirect('signup');
        }
        else
        {
            if(($error = $request->get('error')) && ($error ==  "access_denied"))
            {
                $error = "Tiene que autorizar a la aplicación MiAutoPub para poder registrarse.";
                Yii::$app->getSession()->setFlash('authError', $error);
                return $this->redirect('signup');
            }
            else if($code = $request->get('code'))
            {
                $error = ""; $user = [];
                $FbLoginAppId = Yii::$app->params['FbLoginAppId'];
                $FbLoginAppSecret = Yii::$app->params['FbLoginAppSecret'];
                $url = 'https://graph.facebook.com/v2.3/oauth/access_token?'
                        . 'client_id='.$FbLoginAppId 
                        . '&redirect_uri=' . $request->hostInfo . '/' . $request->pathInfo
                        . '&client_secret=' . $FbLoginAppSecret
                        . '&code=' . $code;
                $content = Functions::file_get_contents($url);
                if($content && ($data = json_decode($content, TRUE)) && isset($data['access_token']) && ($access_token = $data['access_token']))
                {
                    $url = 'https://graph.facebook.com/v2.3/me?fields=id,name,email&access_token=' . $access_token;
                    $me = json_decode(Functions::file_get_contents($url), TRUE);

                    if(isset($me['id']) && ($user_id = $me['id'])
                        && isset($me['name']) && ($user_name = $me['name'])
                        && isset($me['email']) && ($user_email = $me['email']) )
                    {
                        $user = ['id' => $user_id, 'name' => $user_name, 'email' => $user_email, 'access_token' => $access_token];
                    }
                    else
                    {
						\app\helpers\LogSystem::logSystem("notHaveNameAndEmail", ['me' => $me]);
						if(isset($me['name']) && !isset($me['email']))
						   {
							   $error = "Se ha podido acceder a su nombre pero no a su correo, por favor revise que su correo sea accesible.";
						   }
						   else
						   {
							   $error = "Debe proporcionar permisos para acceder a su nombre y correo.";
						   }
                        
                    }
                }
                
                if($user)
                {
                    $exist = \app\models\Publisher::find()->orWhere(['id' => $user_id])
                        ->orWhere(['username' => $user_name])
                        ->orWhere(['email' => $user_email])->count();
                    if($exist)
                    {
                        $error = "Su cuenta de Facebook '$user_name' ya se encuentra registrada.";
                        $user = [];
                    }
                }
            
                if($error)
                    Yii::$app->getSession()->setFlash('error', $error);
                
                Yii::$app->getSession()->setFlash('user', $user);
                return $this->redirect('signup');
            }
        }
        return $this->render('signup');
    }
    
    public function actionPricing()
    {
        
        if($code = Yii::$app->request->get('code'))
        {
            
        }
        return $this->render('pricing');
    }
    
    public function actionError()
    {
        if ($error = Yii::$app->errorHandler->exception) 
        {
            if (Yii::$app->request->isAjax)
            {
                $this->error = $error->getMessage();
                $this->debug = $error->getTraceAsString();
            }
            else
            {
                return $this->render('error', ['error' => $error]);
            }
        }
    }
}
