<?php

namespace app\controllers;

use Yii;
use app\helpers\Controller;
use app\models\Publisher;
use app\models\PublisherMeta;
use app\models\GroupPublisher;
use app\helpers\PublisherHelper;
use app\helpers\GroupHelper;
use app\helpers\Functions;
use app\helpers\Facebook;
use app\helpers\UserIdentity;
use app\helpers\Meta;

class PublisherController extends Controller
{
    /**
     * Obtiene la lista de todos los usuarios, de acuerdo al tipo de usuario que solicita
     * @return array[Publishers]
     */
    public function actionQuery()
    {
        $user_id = Yii::$app->user->id;
        //$children_ids = PublisherHelper::getChildren();
        //$children_ids []= $user_id;
        
        $time = time();
        
        $find = Publisher::find()->where(['publisher.comunity_id' => $this->comunity_id]);
                //->andWhere(['publisher.id' => $children_ids]);
        
        $ago15 = $time - (15*24*60*60);
        
        $select = ['publisher.id','username','email','publisher.name','status',
            'publisher.image_url','modified_time', 
            '(SELECT count(*) FROM `group_publisher` WHERE publisher.id = group_publisher.publisher_id) as count_all_groups',
            '(SELECT count(*) FROM `group_publisher` WHERE publisher.id = group_publisher.publisher_id AND active = 1) as count_active_groups',
            "(SELECT count(*) FROM `publication` WHERE publisher.id = publication.publisher_id AND published_time > $ago15) as count_publication",
        ];
        
        $params = Yii::$app->request->queryParams;
        
        $moreFields = array_key_exists("moreFields",$params)?explode(",",$params['moreFields']):[];
        
        //Incluir grupos si se solicita
        if(in_array("groups", $moreFields))
        {
            $find->joinWith([
            'groupPublishers',
                'groupPublishers.group' => function($q){
                    $q->select(['id','name','wall_id','can_publish','image_url']);
                }
            ]);
        }
        
        //Incluir usuarios activos, eliminados, o esperando aprobación
        $include_active  = array_key_exists("includeActive",$params) && $params['includeActive']?TRUE:FALSE;
        $include_deleted = array_key_exists("includeDeleted",$params) && $params['includeDeleted']?TRUE:FALSE;
        $include_waiting = array_key_exists("includeWaiting",$params) && $params['includeWaiting']?TRUE:FALSE;
        $status_included = [];
        $include_active && ($status_included[]=  UserIdentity::STATUS_ACTIVE);
        $include_deleted && ($status_included[]=  UserIdentity::STATUS_DELETED);
        $include_waiting && ($status_included[]=  UserIdentity::STATUS_WAITING);
        //Predeterminado mostrar solo activos
        !$status_included && ($status_included[]=  UserIdentity::STATUS_ACTIVE);
        
        //$find->andWhere(['status' => $status_included]);
        
        $data = $find
                ->select($select)
                ->asArray()->all();

        PublisherHelper::filter_users($data);
        
        $results = ['data' => $data];
        
        return ($this->success = $results);
    }
    
    public function actionQueryByKey()
    {
        $user_id = Yii::$app->user->id;
        
        $find = Publisher::find();
        
        $select = ['publisher.id','publisher.name'];
        
        $find->andWhere(['status' => UserIdentity::STATUS_ACTIVE, 'comunity_id' => $this->comunity_id]);
        
        $all = $find
                ->select($select)
                ->asArray()->all();
        
        
        return ($this->success = $all);
    }
    
    public function actionOne($id)
    {
        
        //Fields predeterminados
        $params = Yii::$app->request->queryParams;
        
        
        $find = Publisher::find()
                ->joinWith(['groupPublishers' => function($q){
                    $q->joinWith('group');
                }])
                ->where(['publisher.id' => $id]);
        
        $publisher = PublisherHelper::filter_user($find->asArray()->one());
        
        $success = ['data' => $publisher];
        return ($this->success = $success);
    }
    
    public function actionGetGroups($id)
    {
        $groups_publisher = GroupPublisher::find()
                ->joinWith([
                    'group' => function($q){
                        $q->select(['id','name','wall_id','can_publish','image_url']);
                    }
                ])
                ->where(['publisher_id' => $id])
                ->asArray()->all();
        $mi_groups = [];
        foreach ($groups_publisher as $group_publisher)
        {
            $group = $group_publisher['group'];
            $add = [];
            $add['id']          = $group['id'];
            $add['name']        = $group['name'];
            $add['wall_id']     = $group['wall_id'];
            $add['image_url']   = $group['image_url'];
            $add['can_publish'] = (bool)$group['can_publish'];
            $add['active']      = (bool)$group_publisher['active'];
            $add['reason']      = $group_publisher['reason'];
            
            $mi_groups []= $add;
        }
        
        $this->success = $mi_groups;
    }
    
    public function actionUpdate($id)
    {
        $data = Yii::$app->request->post();
        if(!$data || !is_array($data))    return ($this->error = "No se enviaron correctamente los valores, vuelva a intentarlo");
        
        $update = array_intersect_key($data, array_flip(['password','role','can_publish','from','until','can_login','app_id','app_secret','oportunity_publish','time_betwen_pubs']));
        
        if(!$update) return ($this->error = ['No envió ningún dato']);
        
        //Verificar si tiene permisos para modificar el publicador
        $user_id = Yii::$app->user->id;
        /*
        $children_ids = PublisherHelper::getChildren($user_id);
        $children_ids []= $user_id;
        if(!in_array($id, $children_ids))
            return ($this->error = ['No puede modificar este publicador']);
        */
        //Verificar si existe el publicador
        if(!($publisher = Publisher::findOne($id)))
            return ($this->error = ['El publicador que intenta modificar no existe o ha sido eliminado']);
        
        $result = [];
        $errors = [];
        $meta = Meta::publisher($id);
        
        //Modificar la activación del inicio de sesión
        if(array_key_exists('can_login', $update))
        {
            $value = ($update['can_login'] == TRUE);
            if(!$value && ($user_id == $id))
            {
                $errors []= "No puede deshabilitar el inicio de sesión de su propio usuario";
            }
            else
            {
                $meta->can_login = $value;
                $result['can_login'] = $value;
                if(!$value)
                {
                    //TODO Eliminar Cookie de sesión;
                }
            }
        }
        
        if(array_key_exists('oportunity_publish', $update) && ($oportunity_publish = $update['oportunity_publish']))
        {
            $oportunity_publish = max(min($oportunity_publish,100),1);
            
            $meta->oportunity_publish  = $oportunity_publish;
            $result['oportunity_publish']        = $oportunity_publish;
        }
        
        if(array_key_exists('time_betwen_pubs', $update) && ($time_betwen_pubs = $update['time_betwen_pubs']))
        {
            $time_betwen_pubs = max(min($time_betwen_pubs,60),1);
            
            $meta->time_betwen_pubs  = $time_betwen_pubs;
            $result['time_betwen_pubs']        = $time_betwen_pubs;
        }
        
        //Modificar la opción de poder publicar o no
        if(array_key_exists('can_publish', $update))
        {
            $time = time();
            $value = ($update['can_publish'] == TRUE);
            
            
            $from = (isset($update['from']) && ($update['from']) && ($update['from']>$time))?$update['from']:$time;
            $until = (isset($update['until']) && ($update['until']) && ($update['until']>0))?$update['until']:0;
            if($value && !$meta->app_valid)
            {
                $meta->can_publish      = FALSE;
                $result['can_publish']  = FALSE;
                $meta->from             = 0;
                $result['from']         = 0;
                $meta->until            = 0;
                $result['until']        = 0;
                
                $errors []= "El usuario no puede publicar ya que la APP Facebook no ha sido configurada o no es válida";
            }
            else
            {
                //Si la activación se hará en un futuro
                if($from > $time)
                {
                    $meta->can_publish      = !$value;
                    $result['can_publish']  = !$value;
                    $meta->from             = $from;
                    $result['from']         = $from;
                }
                else
                {
                    $meta->can_publish      = $value;
                    $result['can_publish']  = $value;
                    $meta->from             = 0;
                    $result['from']         = 0;
                }
                
                $meta->cant_publish_reason = '';
                $result['cant_publish_reason'] = '';
                $meta->error_count = 0;
                $result['error_count'] = 0;
                
                $meta->until        = $until;
                $result['until']    = $until;
                
            }
        }
        
        //Modificar el rol del publicador
        if(array_key_exists('role', $update) && ($role = $update['role']))
        {
            //Sólo administrador puede modificar el rol de un usuario
            if(PublisherHelper::isAdmin())
            {
                if($user_id == $id)
                {
                    $errors []= "No puede modificar su propio rol";
                }
                else
                {
                    $permited_roles = Yii::$app->params['roles'];
                    if(array_key_exists($role, $permited_roles))
                    {
                        $meta->role = $role;
                        $result['role'] = $role;
                    }
                    else
                    {
                        $errors []= "El rol especificado no es válido";
                    }
                }
            }
            else
            {
                $errors []= "No tiene permiso para cambiar el rol de un publicador";
            }
        }
        
        //Modificar el app id y el app secret
        if(array_key_exists('app_id', $update) && array_key_exists('app_secret', $update) && ($app_id = $update['app_id']) && ($app_secret = $update['app_secret']))
        {
            $valid = Facebook::validateApp($app_id, $app_secret);
            if($valid)
            {
                $meta->set(['app_id' => $app_id, 'app_secret' => $app_secret, 'app_valid' => TRUE, 'token_user_id' => NULL]);
                $result['app_id'] = $app_id;
                $result['app_valid'] = TRUE;
                $result['token_user_id'] = NULL;
            }
            else
            {
                $errors []= "La combinación del 'app id' y 'app secret' no es válida";
            }
        }
        
        //Modificar la contraseña
        if(array_key_exists('password', $update) && ($password = $update['password']))
        {
            //TODO validaciones de contraseña
            $meta->password_hash = Yii::$app->security->generatePasswordHash($password);
        }
        
        $this->success = $result;
        $errors && $this->error = $errors;
    }
    
    
    private $graphUrl = "https://graph.facebook.com/v2.2/";
    
    public function actionUpdateFromFacebook()
    {
        set_time_limit(0);
        
        $data = Yii::$app->request->post();
        if(!isset($data['user_id']) || !($user_id = $data['user_id']))
        {
            return ($this->error = "Se necesita el ID del usuario");
        }
        
        $user = Publisher::findOne($user_id);
        if(!$user)
            return ($this->error = "El usuario no existe");
        
        $comunity_id = $user['comunity_id'];
        
        $meta = Meta::publisher($user_id);
        
        if(!($app_user_id = $meta->app_user_id) || !($access_token = $meta->access_token))
        {
            return ($this->error = "facebook_login_required");
        }
        
        if(!($app_id = $meta->app_id) || !($app_secret = $meta->app_secret))
            return ($this->error = "Es necesario el app_id y el app_secret");
        
        //Verificar que el app_id exista y que el secret sea correcto
        if(!Facebook::validateApp($app_id, $app_secret))
        {
            return ($this->error = 'La aplicación no existe en facebook');
        }
        
        if(!PublisherHelper::userBelongsApp($app_user_id, [$app_id, $app_secret]))
        {
            return ($this->error = "La aplicación no es propia del usuario");
        }
        
        $client = new \GuzzleHttp\Client([
            'base_uri' => $this->graphUrl,
            'verify' => FALSE,
        ]);
        
        $params = \Yii::$app->params;
        
        try
        {
            //Actualizar los datos del usuario
            $query = [
                'fields' => 'id,name,email,picture',
                'access_token' => $access_token,
            ];
            
            $res = $client->get('/me', ['query' => $query]);
            $me = json_decode($res->getBody()->getContents(), TRUE);
            
            $name = $me['name'];
            $email = $me['email'];
            $image_url = @$me['picture']->data->url;
            $user = PublisherHelper::upsertPublisher([
                'id' => $user_id,
                'name' => $name,
                'email' => $email,
                'image_url' => $image_url
            ]);
            
            
            /*
             * Actualizar los grupos de Facebook
             */
            $query = [
                'fields' => 'name,id,picture',
                'limit' => 5000,
                'access_token' => $access_token,
            ];
            
            $res = $client->get('/me/groups', ['query' => $query]);
            $graphObject = json_decode($res->getBody()->getContents(), TRUE);
            
            if($graphObject && ($groups = $graphObject['data']))
            {
                $groups_publisher = GroupPublisher::find()->where(['publisher_id' => $user_id])->asArray()->all();
                //Lista de IDs de grupos que actualmente tiene el usuario
                $groups_publisher_ids = Functions::list_pluck($groups_publisher, 'group_id');
                
                $groups_wall_ids = [];
                $groups_ids = [];
                foreach ($groups as $group)
                {
                    $wall_id = $group['id'];
                    $name = $group['name'];
                    $image_url = @$group['picture']['data']['url'];
                    
                    $members = ['data' => []];
                    try
                    {
                        //$members = (new \Facebook\FacebookRequest($session, 'GET', '/'.$id.'/members', ['fields' => 'name,id', 'limit' => 99999]))->execute()->getGraphObject()->asArray();
                    } catch (Exception $ex) {

                    }
                    
                    $members_count = count($members['data']);
                    
                    $groups_wall_ids[] = $wall_id;


                    $new_group = GroupHelper::upsertGroup([
                        'comunity_id' => $comunity_id,
                        'wall_id' => $wall_id,
                        'name' => $name,
                        'image_url' => $image_url?$image_url:"",
                        //'members_count' => $members_count,
                    ]);
                    $group_id = $new_group->id;
                    $groups_ids []= $group_id;
                    
                    //Agregar nuevos grupos si tuviera
                    $groups_insert = ['group_id' => $group_id, 'publisher_id' => $user_id];
                    //if(!GroupPublisher::find()->where($groups_insert)->count())
                    if(!in_array($group_id, $groups_publisher_ids))
                    {
                        (new GroupPublisher($groups_insert))->save();
                    }
                }
                
                //Eliminar grupos a los que ya no pertenece
                if($diff = array_diff($groups_publisher_ids,$groups_ids))
                {
                    GroupPublisher::deleteAll([ 'publisher_id' => $user_id, 'group_id' => array_values($diff) ]);
                }
				
				GroupPublisher::updateAll(['active' => 1, 'reason' => ''], ['publisher_id' => $user_id]);
            }
            $publisher = PublisherHelper::getOnePublisher($user_id);
            $publisher['count_all_groups'] = count($groups_ids);
            return ($this->success = $publisher);

        }
        catch (\GuzzleHttp\Exception\ClientException $ex) 
        {
            return $this->render('/site/message', [
                "error" => "Ha ocurrido un error al verificar el login, por favor vuelva a intentarlo.",
                "continue" => sprintf("%s/call-register?c=%s", $params['urlRoot'], $comunity_id),
                "debug" => $ex->getFile() . ":" . $ex->getLine() . "\n" . $ex->getMessage() . "\n\n" 
                    . var_export($ex->getRequest(), true) . "\n\n" 
                    . var_export($ex->getResponse(), true) . "\n\n" . $ex->getTraceAsString()
                ]);
        }
        catch (\Facebook\FacebookAuthorizationException $ex)
        {
            return ($this->error = "facebook_login_required");
        }
        catch (Exception $ex)
        {
            $this->debug = $ex->getTraceAsString();
            $this->error = $ex->getMessage();
            return;
        }
    }
    
    public function actionChangeGroupStatus()
    {
        $data = Yii::$app->request->post();
        if(!isset($data['user_id']) || !($user_id = $data['user_id']))
        {
            return ($this->error = "Se necesita el ID del usuario");
        }
        
        if(!isset($data['group_id']) || !($group_id = $data['group_id']))
        {
            return ($this->error = "Se necesita el ID del grupo");
        }
        
        if(!isset($data['active']))
        {
            return ($this->error = "Se necesita el valor del estado a asignar");
        }
        $value = $data['active']==true?1:0;
        
        $attrs = ['publisher_id' => $user_id, 'group_id' => $group_id];
        if($group_publisher = GroupPublisher::findOne($attrs))
        {
            $group_publisher->active = $value;
            $group_publisher->save();
            $this->success = ['active' => (bool)$value];
        }
        else
        {
            return ($this->error = "La relación con el grupo no existe");
        }
    }
    
    public function actionConfigureApp()
    {
        $data = Yii::$app->request->post();
        if(!isset($data['publisher_id']) || !($publisher_id = $data['publisher_id']))
            return ($this->error = "Se necesita el ID del publicador");
        
        if(!isset($data['app_id']) || !($app_id = $data['app_id']))
            return ($this->error = "Se necesita el APP ID");
        
        if(!isset($data['app_secret']) || !($app_secret = $data['app_secret']))
            return ($this->error = "Se necesita el APP Secret");
        
        $user_login_id = Yii::$app->user->id;
        
        PublisherHelper::isOwner($parent, $child);
        
    }


    public function actionRemove($id)
    {
        //No puede eliminar su propio usuario
        if(Yii::$app->user->id == $id)
        {
            return ($this->error = "No puede eliminar su propio usuario");
        }
        
        try {
            $this->success = Publisher::deleteAll(['id' => $id]);
        } catch (Exception $ex) {
            $this->error = "Este usuario no se pudo eliminar.";
            $this->debug = $ex;
        }
    }
    
    public function actionRequestToken()
    {
        $data = Yii::$app->request->post();
        
        if(!isset($data['publisher_id']) || !($publisher_id = $data['publisher_id']))
            return ($this->error = "Se necesita el Identificador del usuario.");
        
        if(!isset($data['fb_id']) || !($fb_id = $data['fb_id'])  || (!is_numeric($fb_id)))
            return ($this->error = "Se necesita el ID del usuario que quiere solicitar el token");
        
        //Buscar un app que pueda publicar y que se encuentre disponible.
        $app = [];
        $appsToPublish = Yii::$app->params['appsToPublish'];shuffle($appsToPublish);
        foreach ($appsToPublish as $appToPublish)
            if($appToPublish['expire'] > time()) $app = $appToPublish;
        if(!$app)
            return ($this->error = "En este momento no se encuentra ningún cupón disponible, por favor contacte al administrador.");
        $user_token =  $app['user_token'];
        $app_id     =  $app['app_id'];
        $app_secret =  $app['app_secret'];
        
        $role = "testers";
        
        //Verificar si el usuario ya se encuentra registrado en este App
        $userBelongsApp = PublisherHelper::userBelongsApp($fb_id, [$app_id, $app_secret]);
        
        if($userBelongsApp)
        {
            $meta = Meta::publisher($publisher_id);
            $meta->set(['app_id' => $app_id, 'app_secret' => $app_secret, 'app_valid' => TRUE, 'token_user_id' => NULL, 'token_user_id' => NULL]);
            $result = ['app_id' => $app_id, 'app_valid' => TRUE, 'token_user_id' => NULL];
            return ($this->success = $result);
        }
        
        //Si el usuario no se encuentra reigstrado en el App
        $client = new \GuzzleHttp\Client([
            'base_uri' => 'https://graph.facebook.com/v2.2/',
            'verify' => FALSE
            ]);
        
        $user = [];
        try
        {
            $res = $client->get($fb_id, ['query' => ['access_token' => $user_token]]);
            $body = $res->getBody();
            $user = json_decode($body->getContents(), TRUE);
        }
        catch (\Exception $ex)
        {
			$this->debug = $ex->getFile() . ":" . $ex->getLine() . "\n" . $ex->getMessage() . "\n\n" 
                    . var_export($ex->getRequest(), true) . "\n\n" 
                    . var_export($ex->getResponse(), true) . "\n\n" . $ex->getTraceAsString();
            return ($this->error = "El ID de la cuenta de facebook que ha ingresado no existe o no es accesible.");
        }
        
        try
        {
            $res = $client->post('/'.$app_id.'/roles', 
                ['form_params' => ['user' => $fb_id,'role' => $role, 'access_token' => $user_token]]
            );
            $body = $res->getBody();
            $content = json_decode($body->getContents(), TRUE);
            if(!$content['success'])
            {
                throw new \Exception("Error al agregar al usuario");
            }
        }
        catch (\Exception $ex)
        {
            return ($this->error = "Ocurrió un error al agregar al usuario, porfavor inténtelo de nuevo si no se soluciona contacte al administrador.");
        }   
        
        $result = [
            'user' => $user['name'],
            'app_id' => $app_id
        ];
            
        return ($this->success = $result);
        
    }
    
    public function actionVerifyRequestToken()
    {
        $request = Yii::$app->request;
        
        if(!($publisher_id = $request->get('publisher_id')))
            return ($this->error = "Se necesita el Identificador del usuario.");
        
        if(!($fb_id = $request->get('fb_id')) || !is_numeric($fb_id))
            return ($this->error = "Se necesita el ID del usuario que quiere verificar que aceptó la invitación.");
        
        
        
        if(!($app_id = $request->get('app_id')))
            return ($this->error = "Se necesita el ID de la aplicación que desea verificar.");
        
        $appsToPublish = Yii::$app->params['appsToPublish'];
        
        if(!array_key_exists($app_id, $appsToPublish))
            return ($this->error = "EL ID de la aplicación que se ha especificado no existe, intente obtener otro.");
        
        $app = $appsToPublish[$app_id];
        
        $user_token =  $app['user_token'];
        $app_id     =  $app['app_id'];
        $app_secret =  $app['app_secret'];
        
        $userBelongsApp = PublisherHelper::userBelongsApp($fb_id, [$app_id, $app_secret]);
        
        if($userBelongsApp)
        {
            $meta = Meta::publisher($publisher_id);
            $meta->set(['app_id' => $app_id, 'app_secret' => $app_secret, 'app_valid' => TRUE, 'token_user_id' => NULL]);
            $result = ['app_id' => $app_id, 'app_valid' => TRUE, 'token_user_id' => NULL];
            return ($this->success = $result);
        }
        
        return ($this->success = $userBelongsApp);
    }
}
