<?php
namespace app\controllers;

use Yii;
use app\helpers\Controller;
use app\helpers\Functions;
use app\helpers\EntryHelper;
use app\helpers\PublisherHelper;
use app\helpers\SubscriptionHelper;
use app\helpers\Meta;
use app\models\Publisher;
use app\models\Publication;
use app\models\Group;
use app\models\Comunity;
use app\models\ComunityMeta;

class SubscriptionController extends Controller
{
    public function actionIndex()
    {
        $isAdmin = PublisherHelper::isAdmin();
        
        $params = ['isAdmin' => $isAdmin];
        
        $subscriptions = SubscriptionHelper::getSubscriptions();
        
        $params['subscriptions'] = [];
        foreach ($subscriptions as $subscription)
        {
            $params['subscriptions'] []= $subscription;
        }
                    
        return $this->render('/site/subscription', $params);
    }
    
    public function actionView($number)
    {
        $subscription = SubscriptionHelper::getSubscription($number);
        if(!$subscription)
        {
            Yii::$app->getSession()->setFlash('authError', "No se encontró la suscripción solicitada #".$number);
            return $this->redirect('/subscription');
        }
        $subscription['created_time'] = date("D d M Y", $subscription['created_time']);
		$subscription['start_time'] = date("d/m/Y h:i a", $subscription['start_time']);
		$subscription['end_time'] = date("d/m/Y h:i a", $subscription['end_time']);
			
        $params = ['comunity' => $subscription['comunity']['name'], 'subscription' => $subscription];
        
        $request = Yii::$app->request;
        if($action = $request->get('action'))
        {
            if($action == 'cancel')
            {
                SubscriptionHelper::cancelSubscription($number);
            }
			else if($action == 'recipe')
			{
				return $this->render('/site/subscriptionrecipe', $params);
			}
            return $this->redirect('/subscription');
        }
        
        
        return $this->render('/site/subscriptionview', $params);
    }
}