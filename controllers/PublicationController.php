<?php

namespace app\controllers;

use Yii;
use app\helpers\Controller;
use app\helpers\SubscriptionHelper;
use app\helpers\LogSystem;
use app\models\Publisher;
use app\models\Group;
use app\models\Entry;
use app\models\Publication;
use app\models\Comunity;
use app\models\GroupPublisher;
use app\helpers\Meta;

class PublicationController extends Controller
{
    public function actionIndex()
    {
        
        $params = Yii::$app->request->queryParams;
        
        $query = Publication::find()
                ->joinWith(['publisher' => function($q){
                    $q->select('id');
                },'group' => function($q){
                    $q->select('id');
                },'entry' => function($q){
                    $q->select('id');
                }])
                ->orderBy('published_time DESC')
                ->andWhere(['publication.comunity_id' => $this->comunity_id])
                ->select([
                    'publisher_id','publisher.name as publisher_name','publisher.image_url as publisher_image_url',
                    'group_id','group.name as group_name', 'group.wall_id as group_wall_id',
                    'entry_id','entry.post_title as name','entry.post_url as link','entry.og_image as image',
                    'publication.id','publication.content','publication.published_time'])
                ->limit(50)->asArray();
        
        if(isset($params['filterByPublisherId']) && ($filterByPublisherId = $params['filterByPublisherId']))
        {
            $query->andWhere(['publication.publisher_id' => $filterByPublisherId]);
            $params['filterByPublisherName'] = Publisher::findOne($filterByPublisherId)->name;
        }
        else
        {
            unset($params['filterByPublisherName']);
        }
        
        if(isset($params['before']) && ($before = $params['before']))
        {
            $query->andWhere(['>', 'publication.published_time', $before]);
            unset($params['before']);
        }
        
        if(isset($params['after']) && ($after = $params['after']))
        {
            $query->andWhere(['<', 'publication.published_time', $after]);
            unset($params['after']);
        }
        
        $publications = $query->all();
        
        foreach ($publications as $k => $publication)
        {
            $content = unserialize($publication['content']);
            unset($content['access_token']);
            $publications[$k] = array_merge($publications[$k], $content);
            unset($publications[$k]['content']);
            unset($publications[$k]['publisher']);
            unset($publications[$k]['group']);
            unset($publications[$k]['entry']);
        }
        
        $success = ['data' => $publications];
        
        $success['params'] = $params;
        
        $this->success = $success;
    }    
    
    public function actionCron()
    {
        set_time_limit(0);
        
        $time = time();
        
        $comunities = Comunity::find()->asArray()->all();
        
        foreach ($comunities as $key_com => $comunity)
        {
            $comunity_id = $comunity['id'];
            $subscription = SubscriptionHelper::comunityHasActiveSubscription($comunity_id);
            if(!$subscription) continue;
                
            $comunity['active_users'] = intval($subscription['active_users']);
            $comunity  = array_merge($comunity, Meta::comunity($comunity_id)->get(['time_betwen_pubs', 'oportunity_publish']));
            if(!isset($comunity['time_betwen_pubs'])) $comunity['time_betwen_pubs'] = 5;
            if(!isset($comunity['oportunity_publish'])) $comunity['oportunity_publish'] = 80;
            $comunity['count_used_users'] = 0;
            
            $publishers = Publisher::find()
                ->joinWith(['groupPublishers' => function($q){
                    $q->where(['active' => 1])
                            ->select(['group_id','publisher_id']);
                }])
                ->where(['status' => \app\helpers\UserIdentity::STATUS_ACTIVE, 'comunity_id' => $comunity_id])
                ->select(['publisher.id','publisher.comunity_id','name'])
                ->asArray()->all();
            shuffle($publishers);
            
            $groups = Group::find()
                ->where(['can_publish' => TRUE, 'comunity_id' => $comunity_id])
                ->orderBy('last_published_time ASC')
                ->asArray()->all();
            shuffle($groups);
            $entries = Entry::find()
                ->where(['can_publish' => true, 'comunity_id' => $comunity_id])
                //->orderBy('publication_count ASC')
                ->asArray()->all();
            shuffle($entries);
            
            foreach ($publishers as $publisher)
            {
                $publisher_id   = $publisher['id'];

                $meta = Meta::publisher($publisher_id);
				
				/*
				$meta->cant_publish_reason = "Por favor programe a que hora comienza este publicador.";
				$meta->can_publish = FALSE;
				continue;
				*/
                $metas = $meta->get(['can_publish','from','until','error_count','app_valid','app_id','app_secret','last_publication_time','last_entry_id','last_group_id','access_token','oportunity_publish','time_betwen_pubs']);

                //Verificar si ya puede publicar
                if(isset($metas['can_publish']))
                {
                    $can_publish    = $metas['can_publish'];
                    $from           = isset($metas['from'])?$metas['from']:0;
                    $until          = isset($metas['until'])?$metas['until']:0;
                    if(!$can_publish && $from >0 && $until>0 && $from <= $time)
                    {
                        $metas['can_publish'] = TRUE;
                        $meta->can_publish = $metas['can_publish'];
                        $meta->from = 0;
                    }
                    else if($can_publish && ($until < $time))
                    {
                        $metas['can_publish'] = FALSE;
                        $meta->can_publish = $metas['can_publish'];
						$meta->from = 0;
                        $meta->until = 0;
                    }
                }
                
                if(!isset($metas['error_count']))
                {
                    $metas['error_count'] = 0;
                }
                //Verificar si cuenta con errores
                if($metas['error_count'] >= 3)
                {
                    $metas['can_publish'] = FALSE;
                    $meta->cant_publish_reason = "El publicador ha tenido 3 errores continuos al intentar publicar.";
                    $meta->can_publish = FALSE;
                    $meta->from = 0;
                    $meta->until = 0;
                }

                $oportunity_publish = isset($metas['oportunity_publish'])?$metas['oportunity_publish']:$comunity['oportunity_publish'];
                $oportunity = mt_rand(1,100);
                if($oportunity > $oportunity_publish) continue;


                //Verificar si puede publicar
                if(!isset($metas['can_publish']) || !$metas['can_publish']) continue;

                //Verificar si tine el app válido
                if(!isset($metas['app_valid']) || !$metas['app_valid']) continue;

                //Verificar que no se haya publicado dentro de un minuto
                $last_publication_time = isset($metas['last_publication_time'])?$metas['last_publication_time']:0;


                //Verificar si tiene una subscripción activa
                if($comunity['active_users'] <= 0) continue;

                //Verificar si ya uso el limite de usuarios permitidos
                if($comunity['count_used_users'] >= $comunity['active_users']) continue;

                $time_betwen_pubs = (isset($metas['time_betwen_pubs']) && $metas['time_betwen_pubs']>0)?(($metas['time_betwen_pubs']*60)-1):($comunity['time_betwen_pubs']*60-1);
                //var_dump($last_publication_time);continue;
                if(($time-$last_publication_time) <= $time_betwen_pubs) continue;

                //Verificar el token
                if(!isset($metas['access_token']) || !($access_token = $metas['access_token'])) continue;
                if(!isset($metas['app_id']) || !($app_id = $metas['app_id'])) continue;
                if(!isset($metas['app_secret']) || !($app_secret = $metas['app_secret'])) continue;
                //Buscar grupo en el cual publicar

                //Preparar los grupos en los cuales puede publicar
                $publisher_groups = [];
                $groupPublishers = $publisher['groupPublishers'];
                foreach ($groupPublishers as $groupPublisher)
                    $publisher_groups []= $groupPublisher['group_id'];

                if(count($publisher_groups) <= 0) return;

                $group_id = NULL;
                $group_wall_id = NULL;
                $group_key = NULL;
                foreach ($groups as $k_group => $group)
                {
                    //No publicar en el mismo grupo al menos en 60 minutos
                    if(($time-$group['last_published_time']) < 10800) continue;

                    //Publicar en el grupo que tenga el publicador
                    if(in_array($group['id'], $publisher_groups))
                    {
                        $group_id = $group['id'];
                        $group_wall_id = $group['wall_id'];
                        $group_key = $k_group;

                        if(!$group_id || !$group_wall_id || !$group_key) continue; //Si no se encontro ningun grupo

                        if(isset($metas['last_group_id']) && ($group_id == $metas['last_group_id']))continue;

                        $group_tags = isset($groups[$group_key]['tags'])?  array_keys(unserialize($groups[$group_key]['tags'])):[];

                        $group_publication_count = $group['publication_count'];

                        //Buscar una entrada que esté permitida para el grupo
                        $entries_permit = [];
                        foreach ($entries as $entry)
                        {
                            $publishers  = isset($entry['publishers'])?unserialize($entry['publishers']):[];
                            $tags_groups = isset($entry['tags_groups'])?unserialize($entry['tags_groups']):[];

                            //Si se ha definido las reglas de incluir o excluir publicadores
                            if(isset($publishers['rule']) && ($rule = $publishers['rule']) && ($rule != 'all') && isset($publishers['list']) && ($list = $publishers['list']))
                            {
                                //preparar la lista de usuarios permitidos o no
                                $list_ids = [];foreach ($list as $item) $list_ids []= $item['id'];

                                //Si la regla es incluir y no esta en la lista, entrada no permitida
                                if($rule == 'include' && !in_array($publisher_id, $list_ids))continue;
                                //Si la regla es excluir y esta en la lista, entrada no permitida
                                else if($rule == 'exclude' && in_array($publisher_id, $list_ids))continue;
                            }

                            if(isset($tags_groups['rule']) && ($rule = $tags_groups['rule']) && ($rule != 'all') && isset($tags_groups['tags']) && ($tags = $tags_groups['tags']))
                            {
                                //Por cada tag que tiene el grupo
                                //Si la regla es incluir y no esta en la lista, entrada no permitida
                                if($rule == 'include' && (count(array_diff($group_tags, $tags)) == count($group_tags)))continue;
                                //Si la regla es excluir y esta en la lista, entrada no permitida
                                else if($rule == 'exclude' && (count(array_diff($group_tags, $tags)) != count($group_tags)))continue;
                            }
                            $entries_permit []= $entry;
                        }
                        $count_entries_permit = count($entries_permit);
                        if($count_entries_permit > 0)
                        {
                            //Seleccionar la entrada permitida
                            $entry_selected = $entries_permit[array_rand($entries_permit)];
                            $max_while = 0;
                            //No publicar la misma entrada seguida
                            //while(isset($metas['last_entry_id']) && ($metas['last_entry_id'] == $entry_selected['id']) && ($max_while++ < 10))
                            //    $entry_selected = $entries_permit[array_rand($entries_permit)];

                            $entry_selected_id = $entry_selected['id'];
                            $entry_selected_publication_count = $entry_selected['publication_count'];

                            //print_r($entry_selected);

                            //Obtener los contenidos posibles de la publicacion
                            $publish_links = isset($entry_selected['publish_links'])?@unserialize($entry_selected['publish_links']):[];
                            $publish_messages = isset($entry_selected['publish_messages'])?@unserialize($entry_selected['publish_messages']):[];
                            $publish_pictures = isset($entry_selected['publish_pictures'])?@unserialize($entry_selected['publish_pictures']):[];
                            $publish_names = isset($entry_selected['publish_names'])?@unserialize($entry_selected['publish_names']):[];
                            $publish_captions = isset($entry_selected['publish_captions'])?@unserialize($entry_selected['publish_captions']):[];
                            $publish_descriptions = isset($entry_selected['publish_descriptions'])?@unserialize($entry_selected['publish_descriptions']):[];

                            if(empty($publish_links)) $publish_links []= $entry_selected['post_url'];
                            if(empty($publish_messages)) $publish_messages[]=  mb_strtolower($entry_selected['post_title'], 'UTF-8');

                            $message = $publish_messages[array_rand($publish_messages)] . "..".chr(rand(97,122)).chr(rand(97,122));
                            $body = array_filter(array(
                                    'message'         => $message,
                                    'link'            => $publish_links[array_rand($publish_links)],
                                    'url'             => empty($publish_links)?(isset($entry_selected['post_url'])?$entry_selected['post_url']:''):$publish_links[array_rand($publish_links)],
                                    'picture'         => empty($publish_pictures)?(isset($entry_selected['og_image'])?$entry_selected['og_image']:''):$publish_pictures[array_rand($publish_pictures)],
                                    'name'            => empty($publish_names)?(isset($entry_selected['post_title'])?$entry_selected['post_title']:''):$publish_names[array_rand($publish_names)],
                                    'caption'         => empty($publish_captions)?'':$publish_captions[array_rand($publish_captions)],
                                    'description'     => empty($publish_descriptions)?'':$publish_descriptions[array_rand($publish_descriptions)],
                                    'access_token'    => $access_token,
                                    //'appsecret_proof' => $appsecret_proof
                                ));
                            //print_r($body);exit;
                            try
                            {
                                $fb = new \Facebook\Facebook([
                                    'app_id' => $app_id,
                                    'app_secret' => $app_secret
                                ]);
                                $request = $fb->request('POST', '/'.$group_wall_id.'/feed', $body, $access_token);

                                $response = $fb->getClient()->sendRequest($request);

                                $objects = $response->getGraphNode();

                                //var_dump($objects);
                                //Si se envió la publicación
                                if( isset($objects['id']) && ($fb_object_id = $objects['id']) )
                                {
                                    $publication = (new Publication([
                                        'comunity_id'  => $comunity_id,
                                        'publisher_id'  => $publisher_id,
                                        'group_id'      => $group_id,
                                        'entry_id'      => $entry_selected['id'],
                                        'fb_object_id'  => $fb_object_id,
                                        'status'        => 'published',
                                        'created_time'  => $time,
                                        'modified_time'  => $time,
                                        'published_time'  => time(),
                                        'content'       => serialize($body),
                                        'comment_count'     => 0,
                                        'like_count'     => 0,
                                    ]));
                                    $publication->save();

                                    $comunity['count_used_users'] = $comunity['count_used_users'] + 1;

                                    $meta->last_publication_time = $time;
                                    $meta->last_entry_id = $entry_selected['id'];
                                    $meta->last_group_id = $group_id;

                                    Group::updateAll(['last_published_time' => $time, 'publication_count' => ($group_publication_count+1)], ['id' => $group_id]);
                                    Entry::updateAll(['last_published_time' => $time, 'publication_count' => ($entry_selected_publication_count+1)], ['id' => $entry_selected_id]);

                                    $meta->error_count = 0;
                                    echo "se publicó: " . $fb_object_id."\n";
                                }

                            }
                            catch(\Facebook\Exceptions\FacebookResponseException $e)
                            {
                                GroupPublisher::updateAll(['active' => 0, 'reason' => $e->getMessage()],['group_id' => $group_id, 'publisher_id' => $publisher_id]);
                                $meta->error_count = $metas['error_count']+1;
                                print_r(array('error' => $e->getMessage()));
                                LogSystem::logSystem("cron.error", [ 'error' => ['message' => $e->getMessage(), 'publisher' => $publisher['name'], 'body' => $body]]);
                            }
                            catch(\Facebook\FacebookSDKException $e)
                            {
                                GroupPublisher::updateAll(['active' => 0, 'reason' => $e->getMessage()],['group_id' => $group_id, 'publisher_id' => $publisher_id]);
                                $meta->error_count = $metas['error_count']+1;
                                print_r(array('error' => $e->getMessage()));
                                LogSystem::logSystem("cron.error", [ 'error' => ['message' => $e->getMessage(), 'publisher' => $publisher['name'], 'body' => $body]]);
                            }
                            catch(\Facebook\FacebookRequestException $e)
                            {
                                GroupPublisher::updateAll(['active' => 0, 'reason' => $e->getMessage()],['group_id' => $group_id, 'publisher_id' => $publisher_id]);
                                $meta->error_count = $metas['error_count']+1;
                                print_r(array('error' => $e->getMessage()));
                                LogSystem::logSystem("cron.error", [ 'error' => ['message' => $e->getMessage(), 'publisher' => $publisher['name'], 'body' => $body]]);
                            }
                            catch(\Facebook\FacebookAuthorizationException $e)
                            {
                                GroupPublisher::updateAll(['active' => 0, 'reason' => $e->getMessage()],['group_id' => $group_id, 'publisher_id' => $publisher_id]);
                                $meta->error_count = $metas['error_count']+1;
                                print_r(array('error' => $e->getMessage()));
                                LogSystem::logSystem("cron.error", [ 'error' => ['message' => $e->getMessage(), 'publisher' => $publisher['name'], 'body' => $body]]);
                            }
                            catch(\Exception $e)
                            {
                                GroupPublisher::updateAll(['active' => 0, 'reason' => $e->getMessage()],['group_id' => $group_id, 'publisher_id' => $publisher_id]);
                                $meta->error_count = $metas['error_count']+1;
                                print_r(array('error' => $e->getMessage()));
                                LogSystem::logSystem("cron.error", [ 'error' => ['message' => $e->getMessage(), 'publisher' => $publisher['name'], 'body' => $body]]);
                            }

                            //Ya no dejar que otro usuario publique en el grupo seleccionado
                            unset($groups[$group_key]);
                            //Terminar con la seleccion de grupo
                            break;
                        }
                    }
                }
				
            }
			echo $comunity['count_used_users'] . " usuarios de la comunidad " . $comunity['name'] . " publicaron\n";
        }
        
        
                
        
        
    }
}