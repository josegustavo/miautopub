<?php
namespace app\controllers;

use Yii;
use app\helpers\Controller;
use app\helpers\Functions;
use app\helpers\EntryHelper;
use app\helpers\PublisherHelper;
use app\models\Entry;
use app\models\Publisher;

class EntryController extends Controller
{
    
    private static $limit = 12;
    
    public function actionIndex()
    {
        $user_id = Yii::$app->user->id;
        
        $messages = [];
        $params = Yii::$app->request->queryParams;
        
        /**
         * Comprobar si existen nuevas entradas
         */
        if(array_key_exists("refresh",$params) && ($params['refresh'] == 1))
        {
            $new_entries_ids = EntryHelper::obtainNewEntries($this->comunity_id);
            $new_entries_ids && ($messages []= sprintf("Se encontraron %d entradas nuevas.", count($new_entries_ids)));
            unset($params['refresh']);
            $params['filter'] = 'all';
        }
        
        
        
        /**
         * Verificar y asignar como se está ordenando
         */
        $orderByOptions = EntryHelper::getOrderByOptions(true);
        
        $firstOrderBy = array_keys($orderByOptions)[0];
        //Si no se ha enviado el parámetro de order, ordenar por el primer elemento
        $orderByParam = array_key_exists("orderBy",$params)?$params['orderBy']:$firstOrderBy;
        //Verificar si el parámetro enviado existe en los elementos ordenables
        if(array_key_exists($orderByParam, $orderByOptions))
        {
            $orderBy = $orderByOptions[$orderByParam];
        }
        else
        {
            $orderBy = $orderByOptions[$firstOrderBy];
            $orderByParam = $firstOrderBy;
        }
              
        $query = Entry::find()
                ->where(['comunity_id' => $this->comunity_id]);
        
        //Si se ha establecido correctamente el elemento de orden
        if($orderBy && count($orderBy) === 1)
        {
            $query->orderBy($orderBy);
            $params['orderBy'] = $orderByParam;
        }
        //Adicionar ordenamiento por el primer elemento ordebale
        if($orderByParam != $firstOrderBy)
        {
            //$query->addOrderBy($orderByOptions[$firstOrderBy]);
        }
       isset($params['search']) && ($search = $params['search']) &&
            $query->andWhere(['like', 'post_title', $search]); 
        /**
         * Filtrar los resultados por activos o inactivos
         */
        $filter_options = ['can_publish','cant_publish','all'];
        $filter = (isset($params['filter']) && in_array($params['filter'],$filter_options))?$params['filter']:$filter_options[0];
        if($filter=='can_publish' || $filter=='cant_publish')
        {
            $query->andWhere(['can_publish' => $filter=='can_publish']);
        }
        $params['filter'] = $filter;
        
        /**
         * Limitar el numero de entradas a obtener, máximo 100 entradas
         */
        $limit = self::$limit;
        if(isset($params['limit']) && ($params['limit']>0))
        {
            $limit = min($params['limit'],100);
        }
        $params['limit'] = $limit;
        
        /*
         * Obtener entradas por página, predeterminado la primera página
         */
        $page = 1;
        isset($params['page']) && ($params['page'] > 0) &&  ( $page = $params['page']);
        $query->offset( ($page-1)*$limit );
        $params['page'] = $page;
        
        //Contar antes de establecer el límite
        $total_items = intval($query->count());
        $total_pages = ceil($total_items/$limit);
        
        
        $query->limit($limit);
        
                
        $all = $query->asArray()->all();
        $data = EntryHelper::filter_entries($all);
        
        $result = ['data' => $data, 'params' => $params ];
        
        $result['total_items'] = $total_items;
        $result['total_pages'] = $total_pages;
        $messages && ($result['messages'] = $messages);
        $orderByOptions && ($result['orderByOptions'] = array_keys($orderByOptions));
        
        return ($this->success = $result);
    }
    
    public function actionGetEntries()
    {
        $entries_ids = EntryHelper::obtainNewEntries($this->comunity_id);
        $this->success = $entries_ids;
    }
    
    public function actionUpdate($id)
    {
        $data = Yii::$app->request->post();
        $update = array_intersect_key($data, array_flip(['tags_groups']));
        
        if(!$update) return ($this->error = ['No envió ningún dato']);
        
        
        $entry = Entry::findOne($id);
        if(!$entry)
            return ($this->error = "La entrada no existe o se ha eliminado");
        
        $result = [];
        
        
        if(isset($update['tags_groups']) && ($groups = $update['tags_groups']))
        {
            Entry::updateAll(['tags_groups' => serialize($groups)],['id' => $id]);
            $result['tags_groups'] = $groups;
        }
        $this->success = $result;
    }
    
    public function actionChangeStatus()
    {
        $user_id = Yii::$app->user->id;
        if(!$user_id)
            return ($this->error = 'Usuario no válido');
        
        
        $data = Yii::$app->request->post();
        if(!isset($data['entry_id']) || !($entry_id = $data['entry_id']))
            return ($this->error = "Se necesita el identificador de la entrada");
        if(!isset($data['can_publish']))
            return ($this->error = "Se necesita el nuevo valor del estado");
        
        $can_publish = $data['can_publish']==true?1:0;
        
        $entry = Entry::findOne($entry_id);
        
        if(!$entry)
            return ($this->error = "La entrada no existe o se ha eliminado");
        
        $time = time();
        
        $entry->can_publish = $can_publish;
        $entry->modified_time = $time;
        if($entry->save())
        {
            $result = ['modified_time' => $time,'can_publish' => (bool)$can_publish];
            if($can_publish && ($update = EntryHelper::updateFromFacebook($entry)))
            {
                $result = array_merge($result, $update);
            }
            return ($this->success = $result);
        }
        else
        {
            $this->debug = $entry->getErrors();
            return ($this->error = "Ocurrio un error desconocido al intentar guardar la entrada.");
        }
    }
    
    public function actionChangePublishList()
    {
        $user_id = Yii::$app->user->id;
        if(!$user_id)
            return ($this->error = 'Usuario no válido');
        
        $data = Yii::$app->request->post();
        if(!isset($data['entry_id']) || !($entry_id = $data['entry_id']))
            return ($this->error = "Se necesita el identificador de la entrada");
        if(!isset($data['list']) || !($list_name = $data['list']))
            return ($this->error = "Se necesita el identificador de la lista");
        if(!isset($data['value']) || !is_array($data['value']))
            return ($this->error = "Se necesita el nuevo valor de la lista");
        $value = $data['value'];
        $entry = Entry::findOne($entry_id);
        
        if(!$entry)
            return ($this->error = "La entrada no existe o se ha eliminado");
        
        if(!array_key_exists($list_name, $entry->attributes))
            return ($this->error = "El identificador de la lista no existe");
        
        $entry[$list_name] = serialize($value);
        if($entry->save())
        {
            $result = [$list_name => $value];
            return ($this->success = $result);
        }
        else
        {
            return ($this->error = "Ocurrio un error desconocido al intentar guardar la entrada.");
        }
    }
    
    public function actionChangeTagsGroups()
    {
        $data = Yii::$app->request->post();
        if(!isset($data['entry_id']) || !($entry_id = $data['entry_id']))
            return ($this->error = "Se necesita el identificador de la entrada");
        if(!isset($data['groups']) || !($groups = $data['groups']))
            return ($this->error = "Se necesita las opciones de categorías");
        
        $entry = Entry::findOne($entry_id);
        
        if(!$entry)
            return ($this->error = "La entrada no existe o se ha eliminado");
        
        $entry->tags_groups = serialize($groups);
        $entry->save();
        
        return ($this->success = ['groups' => $groups]);
        
    }
    
    public function actionChangePublishers()
    {
        $data = Yii::$app->request->post();
        if(!isset($data['entry_id']) || !($entry_id = $data['entry_id']))
            return ($this->error = "Se necesita el identificador de la entrada");
        if(!isset($data['publishers']) || !($publishers = $data['publishers']))
            return ($this->error = "Se necesita las opciones de los publicadores");
        
        $entry = Entry::findOne($entry_id);
        
        if(!$entry)
            return ($this->error = "La entrada no existe o se ha eliminado");
        
        $entry->publishers = serialize($publishers);
        $entry->save();
        
        return ($this->success = ['publishers' => $publishers]);
        
    }
    
    public function actionCreate()
    {
        $time = time();
        $data = Yii::$app->request->post();
        
        $entry = new Entry([
            "comunity_id" => $this->comunity_id,
            "created_time" => $time,
            "modified_time" => $time,
        ]);
        
        if(isset($data['link']))
        {
            $entry->post_url = $data['link'];
            $entry->publish_links = serialize([$data['link']]);
        }
        
        if(isset($data['og_id']))
        {
            $entry->og_id = $data['og_id'];
        }
        
        if(isset($data['title']))
        {
            $entry->post_title = $data['title'];
            $entry->publish_names = serialize([$data['title']]);
        }
        
        if(isset($data['description']))
        {
            $entry->publish_descriptions = serialize([$data['description']]);
        }
        
        if(isset($data['image']))
        {
            $entry->publish_pictures = serialize([$data['image']]);
            $entry->og_image = $data['image'];
        }
        
        if(isset($data['message']))
        {
            $entry->publish_messages = serialize([$data['message']]);
        }
        
        if(isset($data['caption']))
        {
            $entry->publish_captions = serialize([$data['caption']]);
        }
        
        if($entry->save())
        {
            $this->success = TRUE;
        }
        else
        {
            $this->error = "Ocurrió un error al crear la entrada";
            $this->debug = $entry->getErrors();
        }
    }
    
    public function actionRemove($id)
    {
        $deleteAll = Entry::deleteAll(['id' => $id]);
        $this->success  = true;
    }
    
    public function actionLoadDataFromLink()
    {
        $data = Yii::$app->request->post();
        if(!isset($data['link']) || !($link = $data['link']))
            return ($this->error = "Se necesita la dirección URL de donde se obtendrán los datos");
        $data = EntryHelper::loadFromFacebook($link);
        if($data)
        {
            $this->success = $data;
        }
        else
        {
            $this->error = "No se pudo obtener los datos del enlace proporcionado";
        }
    }
}
