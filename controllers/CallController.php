<?php
namespace app\controllers;

use Yii;
use app\helpers\Controller;
use app\helpers\Functions;
use app\helpers\EntryHelper;
use app\helpers\PublisherHelper;
use app\helpers\Meta;
use app\models\Comunity;
use app\models\Publisher;
use app\models\Publication;
use app\models\Group;
use app\models\Entry;

class CallController extends Controller
{
    private $oauthUrl = "https://www.facebook.com/dialog/oauth";
    private $graphUrl = "https://graph.facebook.com/v2.2/";
    
    public function actionCallRegister()
    {
        $comunity_id = \Yii::$app->request->get("c");
        
        if(!Comunity::find()->where(['id' => $comunity_id])->count())
            return $this->render('/site/message', ["error" => "El enlace que ha seguido ya no es válido."]);
        
        $params = \Yii::$app->params;
        $FbLoginAppId = $params['FbLoginAppId'];
        $uriRoot = urlencode(sprintf("%s/call-back-register?c=%s", $params['urlRoot'], $comunity_id));
        $scope = "public_profile,email";
        
        $url = sprintf("%s?client_id=%s&scope=%s&redirect_uri=%s", $this->oauthUrl, $FbLoginAppId, $scope, $uriRoot);
        
        return $this->redirect($url);
    }
    
    public function actionCallBackRegister()
    {
        $isGuest = Yii::$app->user->isGuest;
        
        $comunity_id = \Yii::$app->request->get("c");
        if(!Comunity::find()->where(['id' => $comunity_id])->count())
            return $this->render('/site/message', ["error" => "El enlace que ha seguido ya no es válido."]);
        
        $code = Yii::$app->request->get('code');
        if(!$code)
            return $this->render('/site/message', ["error" => "No ha realizado la autorización correctamente."]);
        
        $params = \Yii::$app->params;
        
        $client = new \GuzzleHttp\Client([
            'base_uri' => $this->graphUrl,
            'verify' => FALSE,
            
            ]);
        
        $redirect_uri = sprintf("%s/call-back-register?c=%s", $params['urlRoot'], $comunity_id);
        
        try
        {
            $query = [
                'client_id' => $params['FbLoginAppId'],
                'redirect_uri' => $redirect_uri,
                'client_secret' => $params['FbLoginAppSecret'],
                'code' => $code,
            ];
            $res = $client->get('oauth/access_token', ['query' => $query]);
            $body = $res->getBody();
            
            $result = json_decode($body->getContents(),TRUE);
            
            $access_token = $result['access_token'];
            
            $query = [
              'fields' => "id,name,email,picture",
              'access_token' => $access_token
            ];
            $res = $client->get('me', ['query' => $query]);
            $graphObject = json_decode($res->getBody()->getContents(), TRUE);
            
            $id = $graphObject['id'];
            $name = $graphObject['name'];
                
            //Validar que no se haya agregado ya el usuario
            if(Publisher::findOne($id))
            {
                $vars = ["error" => "$name ya se encuentra registrado en el sistema"];
                if(!$isGuest) $vars['continue'] = "/admin/publishers";
                return $this->render('/site/message', $vars);
            }

            if(isset($graphObject['email']))
            {
                    $email = $graphObject['email'];
            }
            else
            {
                    $email = str_replace(" ","",$name)."@unknow.com";
            }
            $username = $email;
            
            $image_url = @$graphObject['picture']['data']['url'];
            
            $user_params = [
                'id' => $id,
                'comunity_id' => $comunity_id,
                'username' => $username,
                'name' => $name,
                'email' => $email,
                //'access_token' => $token,
                'image_url' => $image_url?$image_url:"",
                'password' => "",
                'can_login' => FALSE,
                'can_publish' => FALSE,
                'role' => 'publisher',
            ];

            $user = PublisherHelper::upsertPublisher($user_params);
            if(!$user->getErrors())
            {
                $vars = ["success" => "Felicidades, todo ha salido como lo planeado!"];
                if(!$isGuest) $vars['continue'] = "/admin/publishers";
                return $this->render('/site/message', $vars);
                
            }
            else
            {
                return $this->render('/site/message', ["error" => "Ha ocurrido un error al crear el usuario, por favor contacte con el administrador y brinde sus datos."]);
            }
        }
        catch (\GuzzleHttp\Exception\ClientException $ex) 
        {
            return $this->render('/site/message', [
                "error" => "Ha ocurrido un error al verificar el login, por favor vuelva a intentarlo.",
                "continue" => sprintf("%s/call-register?c=%s", $params['urlRoot'], $comunity_id),
                "debug" => $ex->getFile() . ":" . $ex->getLine() . "\n" . $ex->getMessage() . "\n\n" 
                    . var_export($ex->getRequest(), true) . "\n\n" 
                    . var_export($ex->getResponse(), true) . "\n\n" . $ex->getTraceAsString()
                ]);
        }
    }
    
    public function actionCallAuthorize()
    {
        $user_id = \Yii::$app->request->get("u");
        if(!$user_id)
            return $this->render('/site/message', ['error' => 'El enlace no es válido.']);
        
        $user = Publisher::findOne($user_id);
        if(!$user)
            return $this->render('/site/message', ["error" => "El usuario especificado no existe."]);
        
        $meta = Meta::publisher($user_id);
        $app_id = $meta->app_id;
        
        $params = \Yii::$app->params;
        
        $uriRoot = urlencode(sprintf("%s/call-back-authorize?u=%s", $params['urlRoot'], $user_id));
        
        $scope = "public_profile,email,read_stream,publish_actions,user_groups,user_likes,manage_pages";
        
        $url = sprintf("%s?client_id=%s&scope=%s&redirect_uri=%s", $this->oauthUrl, $app_id, $scope, $uriRoot);
        
        return $this->redirect($url);
    }
    
    public function actionCallBackAuthorize()
    {
        $isGuest = Yii::$app->user->isGuest;
        
        $user_id = \Yii::$app->request->get("u");
        if(!$user_id)
            return $this->render('/site/message', ['error' => 'El enlace seguido no fue válido.']);
        
        $code = \Yii::$app->request->get("code");
        if(!$code)
            return $this->render('/site/message', ["error" => "No ha realizado la autorización correctamente."]);
        
        $user = Publisher::findOne($user_id);
        if(!$user)
            return $this->render('/site/message', ["error" => "El usuario especificado no existe."]);
        
        $meta = Meta::publisher($user_id);
        $app_id = $meta->app_id;
        $app_secret = $meta->app_secret;
        
        
        $params = \Yii::$app->params;
        
        $client = new \GuzzleHttp\Client([
            'base_uri' => $this->graphUrl,
            'verify' => FALSE,
        ]);
        
        $redirect_uri = sprintf("%s/call-back-authorize?u=%s", $params['urlRoot'], $user_id);
        
        try
        {
            //Validar el código devuelto y obtener el access_token
            $query = [
                'client_id' => $app_id,
                'redirect_uri' => $redirect_uri,
                'client_secret' => $app_secret,
                'code' => $code,
            ];
            
            //var_dump($query);exit;
                        
            $res = $client->get('/oauth/access_token', ['query' => $query]);
            $body = $res->getBody();
            
            $result = json_decode($body->getContents(), TRUE);
            
            $fb_exchange_token = $result['access_token'];
            
            
            //Extender el access_token y guardarlo
            $query = [
                'grant_type' => 'fb_exchange_token',
                'client_id' => $app_id,
                'client_secret' => $app_secret,
                'fb_exchange_token' => $fb_exchange_token
            ];
            $res = $client->get('/oauth/access_token', ['query' => $query]);
            $result = json_decode($res->getBody()->getContents(), TRUE);
            $extended_token = $result['access_token'];
            

            $query = ['fields' => 'id,name,email','access_token' => $extended_token];
            $res = $client->get('/me', ['query' => $query]);
            $graphObject = json_decode($res->getBody()->getContents(), TRUE);
            
			if(!isset($graphObject['email']))
			{
				$vars = ["error" => "No se ha podido acceder al email de su cuenta, verifique que su correo sea válido en su cuenta de Facebook."];
                return $this->render('/site/message', $vars);
			}
			
            if($user['email'] != $graphObject['email'])
            {
                $vars = ["error" => "La cuenta que desea brindar permisos no es la misma que ha iniciado sesión, por favor inicie sesión en Facebook con la cuenta que esta solicitando permisos."];
                return $this->render('/site/message', $vars);
            }
            
            $app_user_id = $graphObject['id'];
            
            $meta->access_token =  $extended_token;
            $meta->app_user_id =  $app_user_id;
            
            $vars = ["success" => "Felicidades, todo ha salido como lo planeado!"];
            if(!$isGuest) $vars['continue'] = "/admin/publishers";
            return $this->render('/site/message', $vars);
        }
        catch (\GuzzleHttp\Exception\ClientException $ex) 
        {
            return $this->render('/site/message', [
                "error" => "Ha ocurrido un error al verificar el login, por favor vuelva a intentarlo.",
                "continue" => sprintf("%s/call-authorize?u=%s", $params['urlRoot'], $user_id),
                "debug" => $ex->getFile() . ":" . $ex->getLine() . "\n" . $ex->getMessage() . "\n\n" 
                    . var_export($ex->getRequest(), true) . "\n\n" 
                    . var_export($ex->getResponse(), true) . "\n\n" . $ex->getTraceAsString()
                ]);
        }
		catch(\Exception $ex)
		{
			return $this->render('/site/message', [
                "error" => "Ha ocurrido un error desconocido, por favor contacte con el administrador.",
                "continue" => sprintf("%s/call-authorize?u=%s", $params['urlRoot'], $user_id),
                "debug" => $ex->getFile() . ":" . $ex->getLine() . "\n" . $ex->getMessage() . "\n\n" 
                    //. var_export($ex->getRequest(), true) . "\n\n" 
                    //. var_export($ex->getResponse(), true) . "\n\n" . $ex->getTraceAsString()
                ]);
		}
    }    
}
