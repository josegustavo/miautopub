<?php
namespace app\controllers;

use Yii;
use app\helpers\Controller;
use app\helpers\Functions;
use app\helpers\EntryHelper;
use app\helpers\PublisherHelper;
use app\helpers\Meta;
use app\helpers\OrderHelper;
use app\models\Publisher;
use app\models\Publication;
use app\models\Group;
use app\models\Comunity;

class PaymentController extends Controller
{
    
    public function actionIndex()
    {
        $request = Yii::$app->request;
        
        if(Yii::$app->user->isGuest)
        {
            Yii::$app->getSession()->setFlash('authError', 'Para comprar un plan debe iniciar sesión primero');
            $plan = $request->get('plan');
            $url_return = urlencode("/payment?plan=$plan");
            return $this->redirect('/login?return=' . $url_return);
        }
        
        if($request->isGet)
        {
            return $this->showMethodForm();
        }
        else if($request->isPost)
        {
            return $this->processMethodForm();
        }
    }
    
    private function showMethodForm()
    {
        $request = Yii::$app->request;
        
        $date = new \DateTime();
        $comunity = Comunity::findOne($this->comunity_id);
        
        $params = $this->getParamsOrder();
        
        $meta = Meta::comunity($this->comunity_id);
        
        //Verificar que no tenga ninguna orden pendiente
        $last_order = $meta->order; //Obtener la última orden
        if($last_order && $last_order['status'] == 'Pending')
        {
            $params['authError'] = "Ya tiene una orden pendiente, cancele o pague la orden anterior. <a href='/order'><u>ver orden</u></a>";
            $params['cantRequest'] = true;
        }
        
        
        return $this->render('/site/payment', $params);
    }
    
    private function getParamsOrder()
    {
        $request = Yii::$app->request;
        $comunity = Comunity::findOne($this->comunity_id);
        
        $date = new \DateTime();
        
        $params = [
            'from' => 'peru',
            'comunity' => $comunity->name,
        ];
        
        
        $planes = Yii::$app->params['planes'];
        
        if(($plan = $request->get('plan')) && array_key_exists($plan, $planes))
        {
            $params['plan'] = $plan;
        }
        else
        {
            $params['plan'] = 'premiun';
        }
        
        $params['from'] = $request->get('from')?$request->get('from'):'other';
        $months = $request->get('months') && (int)$request->get('months')?(int)$request->get('months'):1;
        $params['months'] = $months;
        $params['money'] = ($params['from']=='peru')?'S/.':  '$';
        
        $options_plan = $planes[$params['plan']];
        
        
        
        $params['plan_name'] = $options_plan['name'];
        $params['publishers'] = $options_plan['publishers'];
        $params['starts'] = $date->format('d/m/Y') ;
        
        if(isset($options_plan['days']))
        {
            $params['days'] = $options_plan['days'];
            $date->add(new \DateInterval('P10D'));
            $params['ends'] = $date->format('d/m/Y') ;
            $params['subtotal'] = ($params['from']=='peru')?$options_plan['price_peru']:$options_plan['price_other'];
            unset($params['months']);
        }
        else
        {
            $params['months'] = min($options_plan['month_max'], max($options_plan['month_min'],$params['months']));
            $date->add(new \DateInterval("P".$params['months']."M"));
            $params['ends'] = $date->format('d/m/Y') ;
            $params['subtotal'] = (($params['from']=='peru')?$options_plan['price_peru']:$options_plan['price_other'])*$params['months'];
            if($options_plan['discount_month'] && $params['months'] > 1)
            {
                $params['discount_percent'] = $options_plan['discount_month'];
                //Aplicar descuento
                $params['discount'] = (int)($params['subtotal']*($options_plan['discount_month']/100));
            }
        }

        $params['total'] = $params['subtotal'] - (isset($params['discount'])?$params['discount']:0);
        
        return $params;
    }
    
    private function processMethodForm()
    {
        $comunity_id = $this->comunity_id;
        $comunity = Comunity::findOne($this->comunity_id);
        $request = Yii::$app->request;
        
        $plan = $this->getParamsOrder();
        
        
        if(OrderHelper::comunityHasPendingOrder($comunity_id))
        {
            Yii::$app->session->setFlash('authError', "Ya tiene una orden pendiente, cancele o pague la orden anterior.");
            return $this->showMethodForm();
        }
        else
        {
            
            if($plan['plan'] == 'free')
            {
                $free_plan_used = FALSE;
                $subscriptions = \app\helpers\SubscriptionHelper::getSubscriptions(['comunity_id' => $comunity_id]);
                foreach ($subscriptions as $subscription)
                {
                    if($subscription['plan']['plan'] == 'free')
                    {
                        $free_plan_used = TRUE;
                        break;
                    }
                }
                if($free_plan_used)
                {
                    Yii::$app->session->setFlash('authError', "Ya ha usado el plan de Prueba, esperamos que le haya sido útil la herramienta durante la prueba y pueda adquirir un plan de pago. Gracias.");
                    return $this->showMethodForm();
                }
                else
                {
                    $order = OrderHelper::createOrder($plan, $comunity_id);
                    \app\helpers\SubscriptionHelper::createSubscription($order);
                    return $this->redirect('/subscription');
                }
            }
            else
            {
                $order = OrderHelper::createOrder($plan, $comunity_id);
                return $this->redirect('/order/' . $order['id']);
            }
        }
    }
    
    public function actionAddOrder()
    {
        $comunity = Comunity::find()->where(['id' => $this->comunity_id])
                ->asArray()->one();
        $meta = Meta::comunity($id);
        
    }
}