<?php
namespace app\controllers;

use Yii;
use app\helpers\Controller;
use app\helpers\Functions;
use app\helpers\PublisherHelper;
use app\models\Publisher;
use app\helpers\UserIdentity;


class DashboardController extends Controller
{
    public function actionIndex()
    {
		$isAdmin = PublisherHelper::isAdmin();
		if(!$isAdmin) return;
		
		$find = Publisher::find()->joinWith(['comunity','publisherMetas']);
		
		$data = $find
				->where(['publisher_meta.key' => 'can_login', 'publisher_meta.value' => 'b:1;'])
                ->select(["publisher.name", 'publisher.id', 'comunity_id', 'comunity.name as comunity_name'])
				->orderBy('publisher.name')
                ->asArray()->all();
        
		$params = ['publishers' => $data];
		return $this->render('/site/dashboard', $params);
	}
	
	public function actionAnylogin($id)
	{
		$isAdmin = PublisherHelper::isAdmin();
		if(!$isAdmin) return;
		$identity = new UserIdentity();
		if($user = $identity->authenticateById($id))
		{
			Yii::$app->user->login($user);
			return $this->redirect('/admin');
		}
		
	}
}