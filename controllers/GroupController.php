<?php
namespace app\controllers;

use Yii;
use app\helpers\Controller;
use app\helpers\Functions;
use app\helpers\GroupHelper;
use app\helpers\PublisherHelper;
use app\models\Group;
use app\models\Publisher;

class GroupController extends Controller
{
    
    private static $limit = 12;
    
    
    public function actionIndex()
    {
        $user_id = Yii::$app->user->id;
        
        
        $messages = [];
        $params = Yii::$app->request->queryParams;
        
        
        /**
         * Verificar y asignar como se está ordenando
         */
        $orderByOptions = GroupHelper::getOrderByOptions(true);
        
        $firstOrderBy = array_keys($orderByOptions)[0];
        //Si no se ha enviado el parámetro de order, ordenar por el primer elemento
        $orderByParam = array_key_exists("orderBy",$params)?$params['orderBy']:$firstOrderBy;
        //Verificar si el parámetro enviado existe en los elementos ordenables
        if(array_key_exists($orderByParam, $orderByOptions))
        {
            $orderBy = $orderByOptions[$orderByParam];
        }
        else
        {
            $orderBy = $orderByOptions[$firstOrderBy];
            $orderByParam = $firstOrderBy;
        }
              
        $query = Group::find()
                ->where(['comunity_id' => $this->comunity_id]);
        
        //Si se ha establecido correctamente el elemento de orden
        if($orderBy && count($orderBy) === 1)
        {
            $query->orderBy($orderBy);
            $params['orderBy'] = $orderByParam;
        }
        
        isset($params['search']) && ($search = $params['search']) &&
            $query->andWhere(['or', ['like', 'group.name' , $search], ['like', 'group.tags' , $search]]);
        
        /**
         * Filtrar los resultados por activos o inactivos
         */
        $filter_options = ['can_publish','cant_publish','all'];
        $filter = (isset($params['filter']) && in_array($params['filter'],$filter_options))?$params['filter']:$filter_options[0];
        if($filter=='can_publish' || $filter=='cant_publish')
        {
            $query->andWhere(['can_publish' => $filter=='can_publish']);
        }
        $params['filter'] = $filter;
        
        /**
         * Limitar el numero de entradas a obtener, máximo 100 entradas
         */
        $limit = self::$limit;
        if(isset($params['limit']) && ($params['limit']>0))
        {
            $limit = min($params['limit'],100);
        }
        $params['limit'] = $limit;
        
       
        
        //Incluir usuarios
        $query->with(['groupPublishers' => function($q){
                $q->joinWith(['publisher' => function($q) {
                    $q->andWhere(['status' => \app\helpers\UserIdentity::STATUS_ACTIVE])
                    ->select(['publisher.id','publisher.name'])
                    ->joinWith(['publisherMetas' => function($q){
                        $q->andWhere(['key' => 'can_publish'])
                          ->select(['id','value','publisher_id']);
                    }]);
                }]);
            }]);
        
            
         /*
         * Obtener entradas por página, predeterminado la primera página
         */
        $page = 1;
        isset($params['page']) && ($params['page'] > 0) &&  ( $page = $params['page']);
        $query->offset( ($page-1)*$limit );
        $params['page'] = $page;
        
        //Contar antes de establecer el límite
        $total_items = $query->count();
        $total_pages = ceil($total_items/$limit);
        
        
        //Establecer el límite
        $query->limit($limit);
        
        
        $all = $query->asArray()->all();
        
        $data = GroupHelper::filter_groups($all);
        
        $result = ['data' => $data, 'params' => $params ];
        
        $result['total_items'] = $total_items;
        $result['total_pages'] = $total_pages;
        $messages && ($result['messages'] = $messages);
        $orderByOptions && ($result['orderByOptions'] = array_keys($orderByOptions));
        
        return ($this->success = $result);
    }
    
    
    public function actionChangeStatus()
    {
        $user_id = Yii::$app->user->id;
        if(!$user_id)
            return ($this->error = 'Usuario no válido');
        
        $data = Yii::$app->request->post();
        if(!isset($data['group_ids']) || !($group_ids = $data['group_ids']))
            return ($this->error = "Se necesita los identificadores de los grupos");
        if(!isset($data['can_publish']))
            return ($this->error = "Se necesita el nuevo valor del estado");
        $can_publish = $data['can_publish']==true?1:0;
        $reason = "";
        if(!$can_publish && isset($data['reason']))
        {
            $reason = substr($data['reason'], 0, 250);
        }
        
        
        $group_count = Group::find($group_ids)->count();
        
        if(!$group_count)
            return ($this->error = "Ningún grupo especificado existe o han sido eliminados");
        
        $time = time();
        
        $attr = ['can_publish' => $can_publish,'reason' => $reason];
        
        $update = Group::updateAll($attr, ['id' => $group_ids]);
        if($update)
        {
            $result = ['reason' => $reason,'can_publish' => (bool)$can_publish];
            return ($this->success = $result);
        }
        else
        {
            return ($this->error = "Ocurrio un error desconocido al intentar guardar el grupo.");
        }
    }
    
    public function actionAddCategory()
    {
        $user_id = Yii::$app->user->id;
        $children_ids = PublisherHelper::getChildren($user_id);
        $children_ids []= $user_id;
        
        $data = Yii::$app->request->post();
        if(!isset($data['group_ids']) || !($group_ids = $data['group_ids']))
            return ($this->error = "Se necesita los identificadores de los grupos");
        if(!isset($data['tag']) || !($tag = trim($data['tag'])))
            return ($this->error = "Se necesita el nombre de la categoría");
        
        
        $groups = Group::find()
                ->where(['comunity_id' => $this->comunity_id])
                ->joinWith(['groupPublishers' => function($q)use ($children_ids,$group_ids){
                    $q->where(['publisher_id' => $children_ids, 'group_id' => $group_ids]);
                }])
                ->all();
            
        if(!$groups)
            return ($this->error = "Ningún grupo especificado existe o han sido eliminados");

        $result = [];
        foreach ($groups as $group)
        {
            $tags = $group->tags?unserialize($group->tags):[];
            if(!array_key_exists($tag, $tags))
            {
                $tags [$tag]= $user_id;
                $group->tags = serialize($tags);
                $group->save();
            }
            $result []= ['id' => $group->id, 'tags' => $tags];
        }
        
        return ($this->success = $result);
    }
    
    public function actionRemoveCategory()
    {
        $user_id = Yii::$app->user->id;
        
        $data = Yii::$app->request->post();
        if(!isset($data['group_id']) || !($group_id = $data['group_id']))
            return ($this->error = "Se necesita el identificador del grupo");
        if(!isset($data['tag']) || !($tag = trim($data['tag'])))
            return ($this->error = "Se necesita el nombre de la categoría");
        
        
        $group = Group::findOne($group_id);                
            
        if(!$group)
            return ($this->error = "Ningún grupo especificado existe o han sido eliminados");

        $tags = $group->tags?unserialize($group->tags):[];
        //Sólo puede eliminar el creador del tag o si es administrador
        if(array_key_exists($tag, $tags) && (($tags[$tag] == $user_id) || PublisherHelper::isAdmin()))
        {
            unset($tags[$tag]);
            $group->tags = serialize($tags);
            $group->save();
        }
        $result = ['tags' => $tags];
        
        return ($this->success = $result);
    }
    
    public function actionChangePublishList()
    {
        $user_id = Yii::$app->user->id;
        if(!$user_id)
            return ($this->error = 'Usuario no válido');
        
        $data = Yii::$app->request->post();
        if(!isset($data['group_id']) || !($group_id = $data['group_id']))
            return ($this->error = "Se necesita el identificador de la entrada");
        if(!isset($data['list']) || !($list_name = $data['list']))
            return ($this->error = "Se necesita el identificador de la lista");
        if(!isset($data['value']) || !is_array($data['value']))
            return ($this->error = "Se necesita el nuevo valor de la lista");
        $value = $data['value'];
        $group = Group::findOne($group_id);
        
        if(!$group)
            return ($this->error = "La entrada no existe o se ha eliminado");
        
        if(!array_key_exists($list_name, $group->attributes))
            return ($this->error = "El identificador de la lista no existe");
        
        $group[$list_name] = serialize($value);
        if($group->save())
        {
            $result = [$list_name => $value];
            return ($this->success = $result);
        }
        else
        {
            return ($this->error = "Ocurrio un error desconocido al intentar guardar la entrada.");
        }
    }
    
    public function actionGetTags()
    {
        $groups = Group::find()
                ->where(['comunity_id' => $this->comunity_id])
                ->select(['id','tags'])->asArray()->all();
        $tags = [];
        foreach ($groups as $group)
        {
            is_array($group_tags = unserialize($group['tags']))
            &&
            ($tags = array_merge($tags, $group_tags));
        }
        
        return ($this->success = array_keys($tags));
    }
}