<?php
namespace app\controllers;

use Yii;
use app\helpers\Controller;
use app\helpers\Functions;
use app\helpers\EntryHelper;
use app\helpers\PublisherHelper;
use app\helpers\Meta;
use app\models\Comunity;
use app\models\Publisher;
use app\models\Publication;
use app\models\Group;
use app\models\Entry;

class ComunityController extends Controller
{
    public function actionIndex()
    {
        $comunity = Comunity::find()->where(['id' => $this->comunity_id])
                ->asArray()->one();
        
        $publisher_count = Publisher::find()->where(['comunity_id' => $this->comunity_id])->count();
        $publication_count = Publication::find()->where(['comunity_id' => $this->comunity_id])->count();
        $group_count = Group::find()->where(['comunity_id' => $this->comunity_id])->count();
        $entry_count = Entry::find()->where(['comunity_id' => $this->comunity_id])->count();
        
        $comunity['publisher_count']    = $publisher_count;
        $comunity['publication_count']  = $publication_count;
        $comunity['group_count']        = $group_count;
        $comunity['entry_count']        = $entry_count;
        
        $meta = Meta::comunity($this->comunity_id);
        
        $metas = $meta->get(['email','whatsapp','wordpress','time_betwen_pubs', 'oportunity_publish']);
        
        $comunity = array_merge($comunity, $metas);
        
        $subscription = \app\helpers\SubscriptionHelper::comunityHasActiveSubscription($this->comunity_id);
		
        if($subscription)
        {
            $comunity['subscription']   = [
                'active_users' => $subscription['active_users'],
                'start_time' => $subscription['start_time'],
                'start_date' => date("d/m/Y h:i a", $subscription['start_time']),
                'end_time' => $subscription['end_time'],
                'end_date' => date("d/m/Y h:i a", $subscription['end_time']),
                'plan_name' => unserialize($subscription['plan'])['plan_name']
            ];
        }
        
        $this->success = $comunity;
    }
    
    public function actionUpdate($id)
    {
        //Verificar que se hayan enviado los datos correctamente
        $data = Yii::$app->request->post();
        if(!$data || !is_array($data))    return ($this->error = "No se enviaron correctamente los valores, vuelva a intentarlo");
        
        //Filtrar los valores modificables
        $update = array_intersect_key($data, array_flip(['name','email','whatsapp','wordpress','time_betwen_pubs','oportunity_publish']));
        
        if(!$update) return ($this->error = ['No envió ningún dato']);
        
        
        $comunity = Comunity::findOne($id);
        if(!$comunity) return ($this->error = ['La comunidad especificada no existe.']);
        
        //Verificar si tiene permisos para modificar la comunidad
        $user_id = Yii::$app->user->id;
        //Obtener la comunidad del usuario actualmente logueado
        $own_comunity_id = $this->comunity_id;
        
        if( ($id != $own_comunity_id) && !PublisherHelper::isAdmin())
            return ($this->error = ['No tiene permisos para realizar esta acción.']);
        
        $result = [];
        $errors = [];
        $meta = Meta::comunity($id);
        
        
        if(array_key_exists('name', $update) && ($name = $update['name']))
        {
            $comunity->name = $name;
            $comunity->save();
            $result['name'] = $name;
        }
        
        if(array_key_exists('email', $update) && ($email = $update['email']))
        {
            $meta->email = $email;
            $result['email'] = $email;
        }
        
        if(array_key_exists('whatsapp', $update) && ($whatsapp = $update['whatsapp']))
        {
            $meta->whatsapp = $whatsapp;
            $result['whatsapp'] = $whatsapp;
        }
        
        if(array_key_exists('wordpress', $update) && ($wordpress = $update['wordpress']))
        {
            $meta->wordpress = $wordpress;
            $result['wordpress'] = $wordpress;
        }
        
        if(array_key_exists('time_betwen_pubs', $update) && ($time_betwen_pubs = $update['time_betwen_pubs']))
        {
            $time_betwen_pubs = max(min($time_betwen_pubs,60),1);
            
            $meta->time_betwen_pubs  = $time_betwen_pubs;
            $result['time_betwen_pubs']        = $time_betwen_pubs;
        }
        
        if(array_key_exists('oportunity_publish', $update) && ($oportunity_publish = $update['oportunity_publish']))
        {
            $oportunity_publish = max(min(intval($oportunity_publish),100),1);
            
            $meta->oportunity_publish  = $oportunity_publish;
            $result['oportunity_publish']        = $oportunity_publish;
        }
        
        
        $this->success = $result;
        $errors && $this->error = $errors;
    }
}