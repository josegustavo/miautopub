<?= $this->context->renderPartial('/layouts/nologin/header'); ?>
<?php
    $authErrorMsg = ((isset($authError) && $authError) || ($authError = Yii::$app->session->getFlash('authError')))?$authError:"";
    
?>
<div id="content">
    <div class="container m-t-xxl padder-v">
        <div class="m-b-lg padder-v-xl">
            <div class="wrapper text-center m-b-lg">
                <?php if($authErrorMsg){ ?>
                <p class="error-summary h4"><?=$authErrorMsg?></p>
                <?php } ?>
            </div>
            <div class="row">
                <div class="col-sm-8 col-sm-push-2">
                    <div class="panel b-a m">
                        <div class="panel-heading bg-info dk no-border wrapper-sm">
                            <span class="h2">Pago de la orden #<?=$order['id']?></span>
                        </div>
                        <?php if($order['plan']['from'] != "peru") { ?>
                        <div class="panel-body text-center" >
                            <div class="m-b-md m-t-sm">
                                <span class="fa-stack fa-2x">
                                    <i class="fa fa-circle-o fa-stack-2x"></i>
                                    <label class="fa-stack-1x">1</label>
                                </span>
                                <span class="text-2x">Click en el boton Ir a PayPal y realizar la transacción</span>
                            </div>
                            <div class="m-b-md">
                                <span class="fa-stack fa-2x">
                                    <i class="fa fa-circle-o fa-stack-2x"></i>
                                    <label class="fa-stack-1x">2</label>
                                </span>
                                <span class="text-2x">Se le confirmará con un correo cuando el pago se haya realizado</span>
                            </div>
                            <div class="m-b-md">
                                <span class="fa-stack fa-2x">
                                    <i class="fa fa-circle-o fa-stack-2x"></i>
                                    <label class="fa-stack-1x">3</label>
                                </span>
                                <span class="text-2x">Aparecerá en el panel que el pago ya se realizó y podrá utilizar la aplicación</span>
                            </div>
                            <?php if($order['status'] == 'paid'){ ?>
                            <div class="m-b-md">
                                <span class="fa-stack fa-2x text-success">
                                    <i class="fa fa-circle-o fa-stack-2x"></i>
                                    <label class="fa-stack-1x"><i class="fa fa-check"></i></label>
                                </span>
                                <span class="text-2x">Su pago ha sido realizado correctamente en paypal, Gracias.</span>
                            </div>
                            <?php } ?>
                        </div>
                        <div class="panel-footer">
                            <form action="<?=$paypal_cfg['url']?>" method="post">
                                
                                <input type="hidden" name="cmd" value="_xclick"/>
                                <INPUT TYPE="hidden" NAME="return" value="<?=Yii::$app->params['urlRoot']?>/order/<?=$order['id']?>/pay">
                                <input type="hidden" name="cancel_return" value="<?=Yii::$app->params['urlRoot']?>/order/<?=$order['id']?>?cancel-paypal">
                                
                                <INPUT TYPE="hidden" NAME="notify_url" value="<?=Yii::$app->params['urlRoot']?>/order/<?=$order['id']?>/pay">
                                <INPUT TYPE="hidden" NAME="currency_code" value="USD">
                                <input type="hidden" name="no_shipping" value="1">
                                
                                <input type="hidden" name="business" value="<?=$paypal_cfg['business']?>">
                                
                                <input type="hidden" name="item_name" value="Plan <?=$order['plan']['plan_name']?> - <?=$order['plan']['publishers']?> publicador<?=$order['plan']['publishers']>1?"es":""?>">
                                <input type="hidden" name="item_number" value="<?=$order['id']?>">
                                <input type="hidden" name="amount" value="<?=$order['plan']['total']?>">

                                <a href="/order/<?=$order['id']?>" class="btn btn-warning btn-lg"><i class="fa fa-share fa-rotate-180"></i> Regresar</a>
                                <div class="pull-right">
                                    <?php if($order['status'] == 'pending'){ ?>
                                    <button class="btn btn-success btn-lg" <?=isset($cantRequest)?"disabled":""?>><i class="fa fa-hand-o-right"></i> Ir a Paypal</button>
                                    <?php } ?>
									<?php if($order['status'] == 'pending' && $isAdmin){ ?>
                                    <a href="/order/<?=$order['id']?>/aprove" class="btn btn-success btn-lg"><i class="fa fa-check"></i> Aprobar manualmente</a>
                                    <?php } ?>
                                </div>
                            </form>
                        </div>
                        <?php } else { ?>
                        <form method="post">
                        <div class="panel-body text-center" >
                            <div class="m-b-md m-t-sm">
                                <span class="fa-stack fa-2x">
                                    <i class="fa fa-circle-o fa-stack-2x"></i>
                                    <label class="fa-stack-1x">1</label>
                                </span>
                                <div class="text-2x">Ubicar el agente BCP más cercano y dirigirte con el monto total de S/.<?=$order['plan']['total']?> (Soles).</div>
                            </div>
                            <div class="m-b-md">
                                <span class="fa-stack fa-2x">
                                    <i class="fa fa-circle-o fa-stack-2x"></i>
                                    <label class="fa-stack-1x">2</label>
                                </span>
                                <div class="text-2x">
                                    <p>Brindar el número de cuenta para el depósito:</p>
                                    <code>194-22352637-0-20</code>
                                    <p>Verificar que corresponda a la persona:</p>
                                    <code>Jose Quilca</code>
                                </div>
                            </div>
                            <div class="m-b-md">
                                <span class="fa-stack fa-2x">
                                    <i class="fa fa-circle-o fa-stack-2x"></i>
                                    <label class="fa-stack-1x">3</label>
                                </span>
                                <div class="text-2x">
                                    <?php if(isset($order['meta']['numberOperation'])){ ?>
                                    <p><i class="fa fa-check"></i> Ya se ha ingresado el código por favor espere la aprobación...</p>
                                    <code><?=$order['meta']['numberOperation']?></code>
                                    <p class="text-sm">Si tiene alguna duda respecto a este tiempo de espera, por favor contáctenos.</p>
                                    <?php } else { ?>
                                    <p>Ingresar el <strong>Número de operación</strong> que aparece en el voucher y dar click en el boton Enviar:</p>
                                    <input type="number" placeholder="Nro.OPE" name="NumberOpeartion" required>
                                    <?php } ?>
                                </div>
                                <?php if($order['status'] == 'paid'){ ?>
                                <div class="m-b-md">
                                    <span class="fa-stack fa-2x text-success">
                                        <i class="fa fa-circle-o fa-stack-2x"></i>
                                        <label class="fa-stack-1x"><i class="fa fa-check"></i></label>
                                    </span>
                                    <span class="text-2x">Su pago ha sido aprobado, Gracias.</span>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="panel-footer">
                                <a href="/order/<?=$order['id']?>" class="btn btn-warning btn-lg"><i class="fa fa-share fa-rotate-180"></i> Regresar</a>
                                <div class="pull-right">
                                    <?php if($order['status'] == 'pending'){ ?>
                                    <button class="btn btn-success btn-lg" <?=isset($cantRequest)?"disabled":""?>><i class="fa fa-hand-o-right"></i> Enviar</button>
                                    <?php } ?>
                                    <?php if($order['status'] == 'validating' && $isAdmin){ ?>
                                    <a href="/order/<?=$order['id']?>/aprove" class="btn btn-success btn-lg"><i class="fa fa-check"></i> Aprobar</a>
                                    <?php } ?>
                                </div>
                        </div>
                        </form>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
</div>
</div>
<script>
    
</script>
<?= $this->context->renderPartial('/layouts/nologin/footer'); ?>