<?= $this->context->renderPartial('/layouts/nologin/header'); ?>
<?php
    $authErrorMsg = ((isset($authError) && $authError) || ($authError = Yii::$app->session->getFlash('authError')))?$authError:"";
    
?>
<div id="content">
    <div class="container m-t-xxl padder-v">
        <div class="m-b-lg padder-v-xl">
            <div class="wrapper text-center m-b-lg">
                <?php if($authErrorMsg){ ?>
                <p class="error-summary h4"><?=$authErrorMsg?></p>
                <?php } ?>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel b-a m">
                        <div class="panel-heading bg-info dk no-border wrapper-sm">
                            <span class="h2">Publicadores</span>
                        </div>
                        <div class="panel-body table-responsive"  style="min-height: 200px">
                            <?php if(isset($publishers) && $publishers){ ?>
                            <table class="table">
                                <thead>
                                    <tr>
										<th>#</th>
                                        <th>Comunidad</th>
                                        <th>Usuario</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody class="text-primary-dker">
                                    <?php foreach ($publishers as $k => $publisher){ ?>
                                    <tr>
										<td><?=$k+1?></td>
                                        <td><?=$publisher['comunity_name']?></td>
                                        <td><a><?=$publisher['name']?></a></td>
                                        <td></td>
                                        <td></td>
                                        <td style="width: 150px">
                                            <a class="btn btn-xs btn-success" href="/dashboard/anylogin?id=<?=$publisher['id']?>">Login</a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <?php } else { ?>
                            <p class="error-summary h4">No se encontraron publicadores</p>
                            <?php } ?>
                        </div>
                        <div class="panel-footer">
                            <a href="/" class="btn btn-warning btn-lg"><i class="fa fa-share fa-rotate-180"></i> Inicio</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</div>
<script>
    
</script>
<?= $this->context->renderPartial('/layouts/nologin/footer'); ?>