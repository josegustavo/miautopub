<?= $this->context->renderPartial('/layouts/nologin/header'); ?>
<?php
    $authErrorMsg = ((isset($authError) && $authError) || ($authError = Yii::$app->session->getFlash('authError')))?$authError:"";
?>
<div id="content">
    <div class="container m-t-xxl padder-v">
        <div class="m-b-lg padder-v-xl">
            <div class="wrapper text-center padder-v-xxl m-b-lg">
                <span class="h1 text-primary">Planes y precios</span>
            </div>
            <div class="row no-gutter">
                <div class="col-lg-3 col-md-4 col-sm-6">
                  <div class="panel b-a">
                    <div class="panel-heading wrapper-xs bg-primary no-border">          
                    </div>
                    <div class="wrapper text-center b-b b-light">
                      <h4 class="text-u-c m-b-none">prueba 10 dias</h4>
                      <h2 class="m-t-none">
                        <sup class="pos-rlt" style="top:-22px">$</sup>
                        <span class="text-2x text-lt">0</span>
                        <span class="text-xs">/ mes</span>
                      </h2>
                    </div>
                    <ul class="list-group text-center no-borders m-t-sm">
                      <li class="list-group-item">
                        <i class="icon-check text-success m-r-xs"></i> Publicadores ilimitados
                      </li>
                      <li class="list-group-item">
                        <i class="icon-check text-success m-r-xs"></i> Máx <b>1 publicador activo</b>
                      </li>
                      <li class="list-group-item">
                        <i class="icon-check text-success m-r-xs"></i> Grupos ilimitados
                      </li>
                      <li class="list-group-item">
                        <i class="icon-check text-success m-r-xs"></i> Máximo 2 entradas
                      </li>
                      <li class="list-group-item">
                        <i class="icon-check text-success m-r-xs"></i> Máximo 100 publicaciones por día
                      </li>
                      <li class="list-group-item">
                        <i class="icon-close text-danger m-r-xs"></i> <span class="text-l-t">Asesoría y Apoyo</span>
                      </li>
                      <li class="list-group-item">
                        <i class="icon-close text-danger m-r-xs"></i> <span class="text-l-t">Soporte y Actualizaciones</span>
                      </li>
                    </ul>
                    <div class="panel-footer text-center">
                      <a href="/payment?plan=free" class="btn btn-primary font-bold m">Solicitar</a>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                  <div class="panel b-a m-t-n-md m-b-xl">
                    <div class="panel-heading wrapper-xs bg-warning dker no-border">

                    </div>
                    <div class="wrapper bg-warning text-center m-l-n-xxs m-r-n-xxs">
                      <h4 class="text-u-c m-b-none">Basic</h4>
                      <h2 class="m-t-none">
                        <sup class="pos-rlt" style="top:-22px">$</sup>
                        <span class="text-2x text-lt">10</span>
                        <span class="text-xs">/ mes</span>
                      </h2>
                    </div>
                    <ul class="list-group text-center no-borders m-t-sm">
                      <li class="list-group-item">
                        <i class="icon-check text-success m-r-xs"></i> Publicadores ilimitados
                      </li>
                      <li class="list-group-item">
                        <i class="icon-check text-success m-r-xs"></i> Máx. <b>1 publicador activo</b>
                      </li>
                      <li class="list-group-item">
                        <i class="icon-check text-success m-r-xs"></i> Grupos ilimitados
                      </li>
                      <li class="list-group-item">
                        <i class="icon-check text-success m-r-xs"></i> Entradas ilimitadas
                      </li>
                      <li class="list-group-item">
                        <i class="icon-check text-success m-r-xs"></i> Publicaciones ilimitadas
                      </li>
                      <li class="list-group-item">
                        <i class="icon-check text-success m-r-xs"></i> Asesoría y Apoyo
                      </li>
                      <li class="list-group-item">
                        <i class="icon-check text-success m-r-xs"></i> Soporte y Actualizaciones
                      </li>
                    </ul>
                    <div class="panel-footer text-center b-t m-t bg-light lter">
                       <a href="/payment?plan=basic" class="btn btn-warning font-bold m">Solicitar</a>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                  <div class="panel b-a">
                    <div class="panel-heading wrapper-xs bg-primary no-border">          
                    </div>
                    <div class="wrapper text-center b-b b-light">
                      <h4 class="text-u-c m-b-none">business</h4>
                      <h2 class="m-t-none">
                        <sup class="pos-rlt" style="top:-22px">$</sup>
                        <span class="text-2x text-lt">15</span>
                        <span class="text-xs">/ mes</span>
                      </h2>
                    </div>
                    <ul class="list-group text-center no-borders m-t-sm">
                      <li class="list-group-item">
                        <i class="icon-check text-success m-r-xs"></i> Publicadores ilimitados
                      </li>
                      <li class="list-group-item">
                        <i class="icon-check text-success m-r-xs"></i> Máx. <b>3 publicadores activos</b>
                      </li>
                      <li class="list-group-item">
                        <i class="icon-check text-success m-r-xs"></i> Grupos ilimitados
                      </li>
                      <li class="list-group-item">
                        <i class="icon-check text-success m-r-xs"></i> Entradas ilimitadas
                      </li>
                      <li class="list-group-item">
                        <i class="icon-check text-success m-r-xs"></i> Publicaciones ilimitadas
                      </li>
                      <li class="list-group-item">
                        <i class="icon-check text-success m-r-xs"></i> Asesoría y Apoyo
                      </li>
                      <li class="list-group-item">
                        <i class="icon-check text-success m-r-xs"></i> Soporte y Actualizaciones
                      </li>
                    </ul>
                    <div class="panel-footer text-center">
                      <a href="/payment?plan=business" class="btn btn-primary font-bold m">Solicitar</a>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 hidden-md">
                  <div class="panel b-a">
                    <div class="panel-heading wrapper-xs bg-primary no-border">

                    </div>
                    <div class="wrapper text-center b-b b-light">
                      <h4 class="text-u-c m-b-none">Premium</h4>
                      <h2 class="m-t-none">
                        <sup class="pos-rlt" style="top:-22px">$</sup>
                        <span class="text-2x text-lt">20</span>
                        <span class="text-xs">/ mes</span>
                      </h2>
                    </div>
                    <ul class="list-group text-center no-borders m-t-sm">
                      <li class="list-group-item">
                        <i class="icon-check text-success m-r-xs"></i> Publicadores ilimitados
                      </li>
                      <li class="list-group-item">
                        <i class="icon-check text-success m-r-xs"></i> Máx. <b>5 publicadores activos</b>
                      </li>
                      <li class="list-group-item">
                        <i class="icon-check text-success m-r-xs"></i> Grupos ilimitados
                      </li>
                      <li class="list-group-item">
                        <i class="icon-check text-success m-r-xs"></i> Entradas ilimitadas
                      </li>
                      <li class="list-group-item">
                        <i class="icon-check text-success m-r-xs"></i> Publicaciones ilimitadas
                      </li>
                      <li class="list-group-item">
                        <i class="icon-check text-success m-r-xs"></i> Asesoría y apoyo
                      </li>
                      <li class="list-group-item">
                        <i class="icon-check text-success m-r-xs"></i> Soporte y Actualizaciones
                      </li>
                    </ul>
                    <div class="panel-footer text-center">
                      <a href="/payment?plan=premiun" class="btn btn-primary font-bold m">Solicitar</a>
                    </div>
                  </div>
                </div>
            </div>
        </div>
</div>
</div>
<?= $this->context->renderPartial('/layouts/nologin/footer'); ?>