<?= $this->context->renderPartial('/layouts/nologin/header'); ?>

<div id="content">
    <div class="container m-t-xxl padder-v">
        <div class="m-b-lg padder-v-xl  m-t-xxl">
            <div class="wrapper text-center m-b-lg m-t-xxl">
                <?php if(isset($error)){ ?>
                <p class="error-summary h4">
                    <?=$error?>
                </p>
                <?php } else if (isset($success)) { ?>
                <p class="success-summary h4">
                    <?=$success?>
                </p>
                <?php } ?>
            </div>
            <?php if(isset($continue)){ ?>
            <div class="wrapper text-center m-b m-t-xs">
                <a class="btn btn-lg btn-primary" href="<?=$continue?>">Continuar</a>
            </div>
            <?php } ?>
            <?php if(YII_DEBUG && isset($debug)){ ?>
            <div class="wrapper m-b-lg m-t-xxl">
                <p>Debug info:</p>
                <pre><?=$debug?></pre>
            </div>
            <?php } ?>
        </div>
</div>
</div>

<?= $this->context->renderPartial('/layouts/nologin/footer'); ?>