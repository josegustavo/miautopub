<?= $this->context->renderPartial('/layouts/nologin/header'); ?>
<?php
    $authErrorMsg = ((isset($authError) && $authError) || ($authError = Yii::$app->session->getFlash('authError')))?$authError:"";
    
?>
<div id="content">
    <div class="container m-t-xxl padder-v">
        <div class="m-b-lg padder-v-xl">
            <div class="wrapper text-center m-b-lg">
                <?php if($authErrorMsg) { ?>
                <p class="error-summary h4"><?=$authErrorMsg?></p>
                <?php } ?>
            </div>
            <div class="row">
                <?php if(isset($order)){ ?>
                <div class="col-sm-6 col-sm-push-3">
                    <div class="panel b-a m">
                        <div class="panel-heading bg-info dk no-border wrapper-sm">
                            <span class="h2">Orden</span> <span class="h4">- <?=  \app\helpers\Functions::getStatusOrderName($order['status'])?></span>
                        </div>
                        <div class="panel-body">
                            <div class="font-bold m-b clearfix b-b">
                                    <span><i class="icon-check text-success m-r"></i> Comunidad:</span>
                                    <span class="pull-right"><?=$order['comunity']['name']?></span>
                            </div>
                            <div class="font-bold m-b clearfix b-b">
                                    <span><i class="icon-check text-success m-r"></i> Plan:</span>
                                    <span class="pull-right"><?=$order['plan']['plan_name']?></span>
                            </div>
                            <div class="font-bold m-b clearfix b-b">
                                    <span><i class="icon-check text-success m-r"></i> Publicadores activos:</span>
                                    <span class="pull-right text-2x"><?=$order['plan']['publishers']?></span>
                            </div>
                            <div class="font-bold m-b clearfix b-b">
                                <div class="m-b-xs">
                                    <span><i class="icon-check text-success m-r"></i> Periodo:</span>
                                    <span class="pull-right"><?=(isset($order['plan']['days'])?($order['plan']['days']." dias"):(isset($order['plan']['months'])?($order['plan']['months']." mes".($order['plan']['months']>1?"es":"")):""))?></span>
                                </div>
                            </div>
                            <div class=" clearfix  m-t-lg">
                                <?php if(isset($order['plan']['discount_percent'])){ ?>
                                <div>
                                    <span><i class="fa fa-dollar text-success m-r"></i> Subtotal</span>
                                    <span class="pull-right "><?=$order['plan']['money']?> <?=$order['plan']['subtotal']?></span>
                                </div>
                                <div class="<?=isset($discount)?'':'text-muted'?>">
                                    <span><i class="fa fa-dollar text-success m-r"></i> Descuento <?=isset($order['plan']['discount_percent'])?'(-'.$order['plan']['discount_percent'].'%)':''?></span>
                                    <span class="pull-right "><?=$order['plan']['money']?> <?=isset($order['plan']['discount'])?('-'.$order['plan']['discount']):'0'?></span>
                                </div>
                                <?php } ?>
                                <div class="text-2x">
                                    <span><i class="fa fa-dollar text-success m-r"></i> Total</span>
                                    <span class="pull-right "><?=$order['plan']['money']?> <?=$order['plan']['total']?></span>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <a href="/order" class="btn btn-warning btn-sm m-t"><i class="fa fa-share fa-rotate-180"></i> Regresar</a>
                            <a class="btn btn-danger btn-sm m-t" <?=($order['status']=='pending')?"":"disabled"?> href="/order/<?=$order['id']?>?action=cancel"><i class="fa fa-check"></i> Cancelar orden</a>
                            <div class="pull-right">
                                <a class="btn btn-success btn-lg" <?=($order['status']=='pending')?"":"disabled"?> href="/order/<?=$order['id']?>/pay"><i class="fa fa-check"></i> Pagar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
</div>
</div>
<script>
    
</script>
<?= $this->context->renderPartial('/layouts/nologin/footer'); ?>