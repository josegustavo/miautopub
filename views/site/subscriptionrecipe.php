
    
        
<title>MiAutoPub</title>
<style>
.ExternalClass h1, .ExternalClass h2, .ExternalClass td, .ExternalClass p {
font-family:helvetica, arial, sans-serif;
}

</style>
    
    
        
        <table width="100%" cellpadding="10" cellspacing="0" >
            <tbody><tr>
                <td valign="top" align="center">
                    
                    <table width="600" cellpadding="0" cellspacing="0" id="ecxheaderTable">
                        <tbody><tr>
                            <td>
                                <h1 style="font-family:helvetica, arial, sans-serif;font-size:28px;">
                                    <a href="http://miautopub.info" title="MiAutoPub" style="text-decoration:none;color:#555555;" target="_blank" class="">MiAutoPub</a>
                                </h1>
                            </td>
                        </tr>
                    </tbody></table>
                    
                </td>
            </tr>
        </tbody></table>
        


<table id="ecxaccount-info" width="100%" cellpadding="0" cellspacing="0" bgcolor="#F3F3F3" style="border-top:3px solid #CCCCCC;border-bottom:3px solid #CCCCCC;">
    <tbody><tr>
        <td valign="top" align="center">
            <table width="600" cellpadding="0" border="0" cellspacing="0" style="padding-bottom: 10px;padding-top: 10px;">
                <tbody><tr>
                    <td>
                        <p style="font-size:12px;"><strong><?=$subscription['created_time']?></strong>
                        </p>

                        <h2 style="font-size:16px;">Orden de Pago #<?=$subscription['order_id']?></h2>

                        <p style="font-size:12px;">
                            <strong><?=$subscription['plan']['comunity']?></strong><br>
                             </p>
                    </td>
                </tr>
            </tbody></table>
        </td>
    </tr>
</tbody></table>

<table width="100%" cellpadding="10" cellspacing="0">
    <tbody><tr>
        <td valign="top" align="center">
            <table width="600" cellpadding="0" border="0" cellspacing="0">
                <tbody><tr>
                    <td>
                        <h1 style="font-size:20px;">Confirmación de la suscripción</h1>

                        <table width="100%" cellpadding="0" cellspacing="0" style="padding-bottom: 10px;padding-top: 10px;">
                            <tbody><tr>
                                <td width="320" id="ecxmessage-body" align="left" valign="top" style="font-size:12px;">
                                    
                                    <p>Gracias por suscribirse!</p>
                                    <p>Tu suscripción ha comenzado 
                                        <?=$subscription['start_time']?>.
                                        y estará activa hasta
										<?=$subscription['end_time']?>.</p>

                                    <p>El pago recibido es
                                        <?=$subscription['plan']['money']?> <?=number_format($subscription['plan']['total'],2)?>.</p>
                                    
                                </td>
                                <td width="20">&nbsp;</td>
                                <td width="260" align="right" valign="top">
                                    
                                    <div id="details" style="background-color:#f3f3f3;border-radius:1.5em;padding:1.5em 20px;width:260px;text-align:left;">
                                        <h2 style="font-size:16px;">Detalles</h2>

                                        <p style="font-size:12px;">Tu
                                            plan:<br> <strong style="font-size:14px;">
											<?=$subscription['plan']['plan_name']?> - <?=$subscription['plan']['publishers']?> publicador<?=$subscription['plan']['publishers']>1?'es':''?>
											</strong>
                                        </p>
                                        <p style="font-size:12px;">
											Fecha de vencimiento:<br> 
											<strong style="font-size:14px;"><?=$subscription['end_time']?></strong>
                                        </p>
                                    </div>
                                    
                                </td>
                            </tr>
                        </tbody></table>

                        
                        <table id="ecxinvoice-table" width="100%" cellpadding="4" cellspacing="0" style="margin-top:10px">
                            
                            <thead>
                            <tr bgcolor="#CCCCCC">
                                <th align="left" style="font-size:12px;"><strong>Fecha</strong>
                                </th>
                                <th align="left" style="font-size:12px;"><strong>Descripción</strong>
                                </th>
                                <th align="right" width="20" style="font-size:12px;">
                                    <strong>Monto</strong></th>
                            </tr>
                            </thead>
                            
                            <tfoot>
                            <tr>
                                <td align="right" colspan="2" style="border-bottom:1px solid #f3f3f3;font-size:12px;">
                                    <strong>Subtotal:</strong></td>
                                <td align="right" width="20" style="border-bottom:1px solid #f3f3f3;font-size:12px;">
                                    <strong><?=$subscription['plan']['money'] . number_format($subscription['plan']['subtotal'],2)?></strong></td>
                            </tr>
                             <tr style="border-bottom:1px solid #f3f3f3;">
                                <td align="right" colspan="2" style="border-bottom:1px solid #f3f3f3;font-size:12px;">
                                    <strong>Pagado:</strong></td>
                                <td align="right" width="20" style="border-bottom:1px solid #f3f3f3;font-size:12px;">
                                    <strong><?=$subscription['plan']['money'] . number_format($subscription['plan']['total'],2)?></strong>
                                </td>
                            </tr>
                            <tr id="ecxinvoice-total" bgcolor="#F3F3F3" style="border-bottom:1px solid #cccccc;">
                                <td align="right" colspan="2" style="border-bottom:1px solid #cccccc;font-size:12px;">
                                    <strong>Total:</strong></td>
                                <td align="right" width="20" style="border-bottom:1px solid #cccccc;font-size:12px;">
                                    <strong><?=$subscription['plan']['money'] . number_format(0,2)?></strong>
                                </td>
                            </tr>
                            </tfoot>
                            
                            <tbody>
                            <tr>
                                <td align="left" valign="top" style="border-bottom:1px solid #f3f3f3;font-size:12px;">
                                    <?=$subscription['start_time']?> — <?=$subscription['end_time']?>
                                </td>
                                <td align="left" valign="top" style="border-bottom:1px solid #f3f3f3;font-size:12px;">
                                    Plan  <?=$subscription['plan']['plan_name']?> - <?=$subscription['plan']['publishers']?> publicador<?=($subscription['plan']['publishers']>1?'es':'')?>
                                </td>
                                <td align="right" valign="top" width="20" style="border-bottom:1px solid #f3f3f3;font-size:12px;">
                                    <strong><?=$subscription['plan']['money'] . number_format($subscription['plan']['total'],2)?></strong></td>
                            </tr>
                            </tbody>
                        </table>

                    </td>
                </tr>
            </tbody></table>
        </td>
    </tr>
</tbody></table>

    


