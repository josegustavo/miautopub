<?= $this->context->renderPartial('/layouts/nologin/header'); ?>
<?php
    $authErrorMsg = ((isset($authError) && $authError) || ($authError = Yii::$app->session->getFlash('authError')))?$authError:"";
    
?>
<div id="content">
    <div class="container m-t-xxl padder-v">
        <div class="m-b-lg padder-v-xl">
            <div class="wrapper text-center m-b-lg">
                <?php if($authErrorMsg){ ?>
                <p class="error-summary h4"><?=$authErrorMsg?></p>
                <?php } ?>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel b-a m">
                        <div class="panel-heading bg-info dk no-bsubscription wrapper-sm">
                            <span class="h2">Suscripciones</span>
                        </div>
                        <div class="panel-body table-responsive"  style="min-height: 200px">
                            <?php if(isset($subscriptions) && $subscriptions){ ?>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Comunidad</th>
                                        <th>Orden</th>
                                        <th>Generado</th>
                                        <th>Inicia</th>
                                        <th>Termina</th>
                                        <th>Plan</th>
                                        <th>Estado</th>
                                    </tr>
                                </thead>
                                <tbody class="text-primary-dker">
                                    <?php foreach ($subscriptions as $subscription){ ?>
                                    <tr>
                                        <td><?=$subscription['plan']['comunity']?></td>
                                        <td><a href="/order/<?=$subscription['order_id']?>"><?=$subscription['order_id']?></a></td>
                                        <td><?=date('d/m/Y h:i a', $subscription['created_time'])?></td>
                                        <td><?=date('d/m/Y h:i a', $subscription['start_time'])?></td>
                                        <td><?=date('d/m/Y h:i a', $subscription['end_time'])?></td>
                                        <td><?=$subscription['plan']['plan_name'] . " " . $subscription['active_users']?></td>
                                        <td>
											<span class="label <?=($subscription['active']==1)?'bg-success':'bg-dark'?>">
												<?=($subscription['active']==1)?'Activo':'Inactivo'?> 
											</span> 
											<span class="label">
											<a class="btn btn-xs btn-primary" href="/subscription/<?=$subscription['id']?>?action=recipe">Ver recibo</a>
											</span>
										</td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <?php } else { ?>
                            <p class="error-summary h4">No se encontraron subscripciones</p>
                            <?php } ?>
                        </div>
                        <div class="panel-footer">
                            <a href="/order" class="btn btn-default btn-lg pull-right"><i class="fa fa-caret-right"></i> Ver órdenes</a>
                            <a href="/" class="btn btn-warning btn-lg"><i class="fa fa-share fa-rotate-180"></i> Regresar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</div>
<script>
    
</script>
<?= $this->context->renderPartial('/layouts/nologin/footer'); ?>