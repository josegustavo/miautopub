<?= $this->context->renderPartial('/layouts/nologin/header'); ?>
<?php
    $authErrorMsg = ((isset($authError) && $authError) || ($authError = Yii::$app->session->getFlash('authError')))?$authError:"";
    
?>
<div id="content">
    <div class="container m-t-xxl padder-v">
        <div class="m-b-lg padder-v-xl">
            <div class="wrapper text-center m-b-lg">
                <?php if($authErrorMsg) { ?>
                <p class="error-summary h4"><?=$authErrorMsg?></p>
                <?php } ?>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="panel b-a m">
                        <div class="panel-heading bg-info dk no-border wrapper-sm">
                            <span class="h2">Método de pago</span>
                        </div>
                        <div class="hbox text-center b-t b-light">         
                            <a <?=($from=='peru')?('href="/payment?plan='.$plan.'"'):''?> class="col padder-v text-muted b-r <?=$from!='peru'?'bg-white':'bg-light grayscale'?>">
                                <div class="h4 m-b-xs">
                                    <img src="/img/paypal.png" alt="paypal" height="57" >
                                </div>
                              <span>Para pagos desde otros paises</span>
                            </a>
                            <a <?=($from!='peru')?('href="/payment?plan='.$plan.'&from=peru"'):''?> class="col padder-v text-muted <?=$from=='peru'?'bg-white':'bg-light grayscale'?>">
                              <div class="h4 m-b-xs">
                                  <img src="/img/agentebcp_icon.png" alt="Agente BCP" height="57" >
                              </div>
                              <span>Para pagos desde Perú</span>
                            </a>
                            
                        </div>
                        <div class="panel-body b-t">
                            <ul class="list-unstyled text-lg text-justify">
                                <?php if($from!='peru'){ ?>
                                <li class="m-b-sm">
                                    <i class="fa fa-check text-success"></i> Uno de los métodos más seguros para pagos en linea y con activación del servicio instantáneo.
                                </li>
                                <li class="m-b-sm">
                                    <i class="fa fa-check text-success"></i> Asegúrese de tener una cuenta de Paypal o puede crearse una durante el proceso de pago.
                                </li>
                                <li class="m-b-sm">
                                    <i class="fa fa-check text-success"></i> Si tiene una tarjeta de crédito para compras por internet puede asociarla a su cuenta de Paypal para hacer más seguro su pago.
                                </li>
                                <li class="m-b-sm">
                                    <i class="fa fa-check text-success"></i> Luego de presionar el botón 'Solicitar' se le redirigirá a la página oficial de Paypal.
                                </li>
                                <?php } else { ?>
                                <li class="m-b-sm">
                                    <i class="fa fa-check text-success"></i> Facilidad para pagos locales en cualquier Agente BCP (Pago en Agencia de Banco tiene un costo adicional de S/.7).
                                </li>
                                <li class="m-b-sm">
                                    <i class="fa fa-check text-success"></i> Tambien puede realizar tranferencia por internet usando Via BCP.
                                </li>
                                <li class="m-b-sm">
                                    <i class="fa fa-check text-success"></i> El pago se hace en el tipo de moneda local (S/.) por lo que el total es menor por los gastos y comisiónes de las conversion de dolares.
                                </li>
                                <li class="m-b-sm">
                                    <i class="fa fa-check text-success"></i> Luego de realizar la solicitud se brindará mas información sobre el depósito y cómo enviar el código de operación.
                                </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="panel b-a m">
                        <div class="panel-heading bg-info dk no-border wrapper-sm">
                            <span class="h2">Detalles</span>
                        </div>
                        <div class="panel-body">
                            <div class="font-bold m-b clearfix b-b">
                                    <span><i class="icon-check text-success m-r"></i> Comunidad:</span>
                                    <span class="pull-right"><?=$comunity?></span>
                            </div>
                            <div class="font-bold m-b clearfix b-b">
                                    <span><i class="icon-check text-success m-r"></i> Plan:</span>
                                    <span class="pull-right"><?=$plan_name?></span>
                            </div>
                            <div class="font-bold m-b clearfix b-b">
                                    <span><i class="icon-check text-success m-r"></i> Publicadores activos:</span>
                                    <span class="pull-right text-2x"><?=$publishers?></span>
                            </div>
                            <div class="font-bold m-b clearfix b-b">
                                <div class="m-b-xs">
                                    <span><i class="icon-check text-success m-r"></i> Periodo:</span>
                                    <span class="pull-right"><?=(isset($days)?($days." dias"):(isset($months)?($months." mes".($months>1?"es":"")):""))?></span>
                                </div>
                            </div>
                            <div class=" clearfix  m-t-lg">
                                <div>
                                    <span><i class="fa fa-dollar text-success m-r"></i> Subtotal</span>
                                    <span class="pull-right "><?=$money?> <?=$subtotal?></span>
                                </div>
                                <div class="<?=isset($discount)?'':'text-muted'?>">
                                    <span><i class="fa fa-dollar text-success m-r"></i> Descuento <?=isset($discount_percent)?'(-'.$discount_percent.'%)':''?></span>
                                    <span class="pull-right "><?=$money?> <?=isset($discount)?('-'.$discount):'0'?></span>
                                </div>
                                <div class="text-2x">
                                    <span><i class="fa fa-dollar text-success m-r"></i> Total</span>
                                    <span class="pull-right "><?=$money?> <?=$total?></span>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <form method="post">
                                <input type="hidden" name="plan" value="<?=$plan?>"/>
                                <a href="/pricing" class="btn btn-warning btn-lg"><i class="fa fa-share fa-rotate-180"></i> Regresar</a>
                                <div class="pull-right">
                                    <button class="btn btn-success btn-lg" <?=isset($cantRequest)?"disabled":""?>><i class="fa fa-check"></i> Solicitar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</div>
<script>
    
</script>
<?= $this->context->renderPartial('/layouts/nologin/footer'); ?>