<?= $this->context->renderPartial('/layouts/nologin/header'); ?>
<?php
    $authErrorMsg = ((isset($authError) && $authError) || ($authError = Yii::$app->session->getFlash('authError')))?$authError:"";
?>
<div id="content">
    <div class="container w-xxl w-auto-xs m-t-xxl padder-v" ng-controller="SigninFormController" ng-init="app.settings.container = false;">
      <div class="m-b-lg padder-v-xl">
        <div class="wrapper text-center">
            <span class="h1 text-primary">Inicia sesión para acceder</span>
        </div>
        <form name="form" method="POST" class="form-validation">
            <div class="text-danger wrapper text-center" id="authError"><?=$authErrorMsg?></div>
          <div class="list-group list-group-sm">
            <div class="list-group-item m-b-xs">
                <input type="text" placeholder="Nombre de usuario o email" class="form-control no-border" name="username" required value="<?=isset($username)?$username:''?>">
            </div>
            <div class="list-group-item">
                <input type="password" placeholder="Contraseña" class="form-control no-border" name="password" required>
            </div>
          </div>
          <button type="submit" class="btn btn-lg btn-primary btn-block" ng-click="login()" ng-disabled='form.$invalid'>Iniciar sesión</button>
          <?php if($FbLoginAppId = Yii::$app->params['FbLoginAppId']) { ?>
          <a class="btn btn-lg btn-primary btn-block facebook-login-button" href="https://www.facebook.com/dialog/oauth?client_id=<?=$FbLoginAppId?>&scope=public_profile,email&redirect_uri=<?=Yii::$app->request->hostInfo . '/' . Yii::$app->request->pathInfo?>">
              <i class="fa fa-facebook"></i> Iniciar con Facebook
          </a>
          <?php } ?>
          <div class="text-center m-t m-b"><a ui-sref="access.forgotpwd">¿Olvido su contraseña?</a></div>
          <div class="line line-dashed"></div>
          <p class="text-center"><small>¿Aún no tiene una cuenta?</small></p>
          <a href="/signup" class="btn btn-lg btn-default btn-block">Crear una cuenta</a>
        </form>
      </div>
</div>
</div>
<?= $this->context->renderPartial('/layouts/nologin/footer'); ?>