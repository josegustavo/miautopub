<?php
/* @var $this SiteController */
/* @var $error array */
$code = isset($error->statusCode)?$error->statusCode:500;
if($code == 404)
{
    $message = 'La página solicitada no existe';
}
else if($code == 500)
{
    $message = 'Ha ocurrido un error, vuelva a intentarlo más tarde.';
}
else if($code == 403 || $code == 401)
{
    $message = 'No esta autorizado para realizar esta acción.';
}
if(YII_DEBUG)
{
    (!isset($error) && ($error = Yii::$app->errorHandler->exception));
    $message = $error->getMessage();
    $debug = $error;
}
?>
<?= $this->context->renderPartial('/layouts/nologin/header'); ?>
<div class="app ng-scope app-header-fixed app-aside-fixed" id="app" ng-class="{'app-header-fixed':app.settings.headerFixed, 'app-aside-fixed':app.settings.asideFixed, 'app-aside-folded':app.settings.asideFolded, 'app-aside-dock':app.settings.asideDock, 'container':app.settings.container}" ui-view=""><!-- uiView:  -->
    <div ui-view="" class="fade-in-right-big smooth ng-scope">
        <div class="container w-xxl w-auto-xs ng-scope" ng-init="app.settings.container = false;">
            <div class="text-center m-b-lg">
                <h1 class="text-shadow text-white"><?php echo ((isset($code) && $code)?$code:'Error'); ?></h1>
                <h4 class="text-danger-dker"><?= isset($message)?$message:'';?></h4>
            </div>
            <div class="list-group bg-info auto m-b-sm m-b-lg">
                <a href="#/" class="list-group-item">
                  <i class="fa fa-fw fa-mail-forward m-r-xs"></i> Ir al inicio
                </a>
            </div>
            
            <div class="text-center ng-scope" ng-include="'tpl/blocks/page_footer.html'">
                <p class="ng-scope">
                    <small class="text-muted">MiAutopub<br>© 2015</small>
                </p>
            </div>
        </div>
        <div class="text-muted wrapper-lg">
            <pre><?php echo isset($debug)?htmlentities($debug):'';?></pre>
        </div>
    </div>
</div>
<?= $this->context->renderPartial('/layouts/nologin/footer'); ?>