<?= $this->context->renderPartial('/layouts/nologin/header'); ?>
<?php
    $FbLoginAppId = Yii::$app->params['FbLoginAppId'];
    
    $authErrorMsg = ((isset($authError) && $authError) || ($authError = Yii::$app->session->getFlash('error')))?$authError:"";
    
    $comunity_name = (isset($comunity_name)?$comunity_name:(($comunity_name = Yii::$app->session->getFlash('comunity_name'))?$comunity_name:""));
    
    $user = Yii::$app->session->getFlash('user');
    $user = $user ? $user : [];
?>
<div id="content">
    <div class="container m-t-xxl padder-v">
        <div class="wrapper text-center m-t-xl">
            <span class="h1 text-primary">Registro</span>
        </div>
        <div class="text-danger wrapper text-center" id="authError"><?=$authErrorMsg?></div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <div class="row m-b-lg">
                    <form name="form" method="POST" class="form-validation text-primary">
                        <input type="hidden" name="user_id" value="<?=isset($user['id'])?$user['id']:''?>" >
                        <input type="hidden" name="access_token" value="<?=isset($user['access_token'])?$user['access_token']:''?>" >
                        <input type="hidden" name="name" value="<?=isset($user['name'])?$user['name']:''?>" >
                        <input type="hidden" name="email" value="<?=isset($user['email'])?$user['email']:''?>" >
                        <div class="col-sm-12">
                            <div class="col-sm-6 col-sm-push-3 col-sm-pull-3">
                                <a class="btn btn-lg btn-primary btn-block facebook-login-button m-b-lg <?=$user?'disabled':''?>" href="https://www.facebook.com/dialog/oauth?client_id=<?=$FbLoginAppId?>&scope=public_profile,email&redirect_uri=<?=Yii::$app->request->hostInfo . '/' . Yii::$app->request->pathInfo?>">
                                    <i class="fa <?=$user?'fa-check':'fa-facebook'?>"></i> <?=$user?"Autorización correcta":"Autorizar con Facebook"?>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="list-group list-group-sm">
                              <label>Nombre:</label>
                              <div class="list-group-item m-b-xs disabled">
                                  <input type="text" placeholder="Cargará luego de autorizar por facebook" value="<?=isset($user['name'])?$user['name']:''?>" class="form-control no-border" required disabled="">
                              </div>
                              <label>Email:</label>
                              <div class="list-group-item disabled">
                                  <input type="email" placeholder="Cargará luego de autorizar por facebook" value="<?=isset($user['email'])?$user['email']:''?>" class="form-control no-border" required disabled="">
                              </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="list-group list-group-sm">
                              <label>Negocio:</label>
                              <div class="list-group-item m-b-xs">
                                  <input type="text" placeholder="Ingrese su nombre o el de su negocio" class="form-control no-border" name="comunity_name" required value="<?=$comunity_name?>">
                              </div>
                              <label>Contraseña:</label>
                              <div class="list-group-item">
                                  <input type="password" placeholder="Ingrese una nueva contraseña" class="form-control no-border" name="password" required>
                              </div>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="col-xs-6 col-xs-push-3 col-xs-pull-3">
                                <button type="submit" class="btn btn-lg btn-primary btn-block" <?=(!isset($user['name'])||!isset($user['name']))?"disabled":""?>>Registrarse</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="col-sm-8 col-sm-push-2 col-sm-pull-2 col-xs-6 col-xs-push-3 col-xs-pull-3 m-b-lg">
                    <p class="text-center"><small>¿Ya tiene una cuenta?</small></p>
                        <a href="/login" class="btn btn-lg btn-default btn-block">Inicie sesión</a>
                </div>
            </div>
        </div>
    </div>
    
    
</div>
<?= $this->context->renderPartial('/layouts/nologin/footer'); ?>