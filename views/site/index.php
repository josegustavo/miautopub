<?= $this->context->renderPartial('/layouts/nologin/header'); ?>
<div id="content">
    <div id="what" class="bg-primary">
        <div class="bg-primary container">
          <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center">
              <div class="m-t-xxl m-b-xxl padder-v">
                <h1 class="font-bold l-h-1x m-t-xxl padder-v animated fadeInDown">
                  MiAutoPub es un servicio que te ayuda a realizar publicaciones en grupos de facebook
                </h1>
                <h3 class="text-muted m-t-xl l-h-1x">Con MiAutoPub olvídate de las publicaciones manuales para promocionar tus productos o servicios</h3>
              </div>
              <p class="text-center m-b-xxl wrapper">
                <a href="/pricing" class="btn b-2x btn-lg b-black btn-warning btn-rounded text-lg font-bold m-b-xxl animated fadeInUpBig">Adquiérelo desde US$10</a>
              </p>
            </div>
          </div>
        </div>
    </div>
    <div id="features" class="bg-info padder-v-xl">
        <div  class="container m-b-xxl padder-v-xl ">
          <div class="row no-gutter m-t-xxl">
            <div class="col-md-3 col-sm-6">
              <div class=" bg-white bg-gd b m-r-n-md no-m-xs no-m-sm">
                <a href class="wrapper-xl block">
                    <span class="h1 m-t-xs text-black text-3x pull-left m-r"><i class="icon-users font-bold"></i></span>
                    <span class="h3 m-t-sm text-black">Gestiona tus publicadores</span>
                    <span class="block m-b-md m-t">Puedes agregar ilimitadamente los usuarios publicadores que quieras y seleccionar quienes realizarán las publicaciones.</span>
                </a>
              </div>
            </div>
            <div class="col-md-3 col-sm-6">
              <div class="bg-white b m-t-xl m-l-n-md m-r-n-md no-m-xs no-m-sm">
                <a href class="wrapper-xl block">
                    <span class="h1 m-t-xs text-black text-3x pull-left m-r"><i class="icon-basket font-bold"></i></span>
                    <span class="h3 m-t-sm">Segmenta todos tus grupos</span>
                    <span class="block m-b-md m-t">Te permite segmentar todos los grupos de tus publicadores para publicaciones mejor orientadas a un público objetivo.</span>
                </a>
              </div>
            </div>
            <div class="col-md-3 col-sm-6">
              <div class="bg-white bg-gd b m-t-n m-l-n-md m-r-n-md no-m-xs no-m-sm">
                <a href class="wrapper-xl block">
                    <span class="h1 m-t-xs text-black text-3x pull-left m-r"><i class="icon-layers font-bold"></i></span>
                    <span class="h3 m-t-sm text-black">Administra tus entradas</span>
                    <span class="block m-b-md m-t">Podrás crear entradas ilimitadamente y programar las que quieras publicar en los grupos y por publicadores seleccionados.</span>
                </a>
              </div>
            </div>
            <div class="col-md-3 col-sm-6">
              <div class="bg-white b m-t m-l-n-md no-m-xs no-m-sm">
                <a href class="wrapper-xl block">
                    <span class="h1 m-t-xs text-black text-3x pull-left m-r"><i class="icon-feed font-bold"></i></span>
                    <span class="h3 m-t-sm text-black">Revisa tus publicaciones</span>
                    <span class="block m-b-md m-t">Las publicaciones se mostrarán en tiempo real, tal como fueron publicadas. También podrás acceder a los comentarios realizados en tus publicaciones.</span>
                </a>
              </div>
            </div>
          </div>
        </div>
    </div>
    <div>
        <div id="why" class="bg-light">
          <div class="container">
            <div class="m-t-xxl m-b-xl text-center wrapper">
              <h2 class="text-black font-bold">¿Por qué usar <span class="b-b b-dark">MiAutoPub</span> para tus publicaciones?</h2>
              <p class="text-muted h4 m-b-xl">Porque es la forma más facil de que tus productos o servicios lleguen a más personas</p>
            </div>
            <div class="row m-t-xl m-b-xl text-center">
              <div class="col-sm-4" data-ride="animated" data-animation="fadeInLeft" data-delay="300">
                <p class="h3 text-3x m-b-xl inline b b-3x b-dark rounded wrapper-lg facebook-color">
                  <i class="fa w-1x fa-facebook font-bold"></i>
                </p>
                <div class="m-b-xl">
                  <h4 class="m-t-none facebook-color">Orientado a grupos de Facebook</h4>
                  <p class="text-muted m-t-lg">MiAutoPub esta orientado a publicaciones en grupos de Facebook, por lo que depende de tí la cantidad de personas a las que quieres llegar</p>
                </div>
              </div>
              <div class="col-sm-4" data-ride="animated" data-animation="fadeInLeft" data-delay="600">
                <p class="h3 text-3x m-b-xl inline b b-3x b-dark rounded wrapper-lg b-primary b-dark text-primary text-primary-dk">
                  <i class="fa w-1x fa-gears font-bold"></i>
                </p>
                <div class="m-b-xl">
                  <h4 class="m-t-none text-primary-dk">Personalizable</h4>
                  <p class="text-muted m-t-lg">Gran cantidad de funcionalidades y sencillas opciones de personalización para programar a tu propio estilo tus publicaciones.</p>
                </div>
              </div>
              <div class="col-sm-4" data-ride="animated" data-animation="fadeInLeft" data-delay="900">
                <p class="h3 text-3x  m-b-xl inline b b-3x b-dark rounded wrapper-lg b-success b-dark text-success text-success-dk">
                  <i class="fa w-1x fa-clock-o font-bold"></i>
                </p>
                <div class="m-b-xl">
                  <h4 class="m-t-none text-success-dk">Comienza en minutos</h4>
                  <p class="text-muted m-t-lg">Comenzar con MiAutoPub es increiblemente fácil, y no necesita mucha configuración, puedes comenzar a publicar en minutos, no necesitas instalar nada.</p>
                </div>
              </div>
            </div>
            <div class="m-t-xl m-b-xl text-center wrapper">
              <p class="h4">Puedes usarlo para 
                <span class="text-primary">vender productos</span>, 
                <span class="text-primary">ofrecer servicios</span>, 
                <span class="text-primary">promocionar páginas</span>
                <span class="text-primary">y muchas cosas más</span>...
              </p>
              <p class="m-t-xl">
                  <a href="/pricing" class="btn btn-lg btn-white b-2x b-dark btn-rounded bg-empty m-sm">Comienza tu prueba de 10 días</a>
              </p>
            </div>
          </div>
        </div>
    </div>
    <div class="bg-white-only">
      <div class="container">
        <div class="row m-t-xl m-b-xxl">
          <div class="col-sm-6" data-ride="animated" data-animation="fadeInLeft" data-delay="300">
              <div class="m-t-xxl">
                <div class="m-b">
                  <a href class="pull-left thumb-sm avatar"><img src="img/a1.jpg" alt=""></a>
                  <div class="m-l-sm inline">
                    <div class="pos-rlt wrapper b b-light r r-2x">
                      <span class="arrow left pull-up"></span>
                      <p class="m-b-none">
                          <b>Marco Salas</b> ha publicado en el grupo <b>Compra y Venta por FACEBOOK</b>
                      </p>
                    </div>
                    <small class="text-muted"><i class="fa fa-ok text-success"></i> Hace unos segundos</small>
                  </div>
                </div>
                <div class="m-b">
                  <a href class="pull-left thumb-sm avatar"><img src="img/a2.jpg" alt=""></a>
                  <div class="m-l-sm inline">
                    <div class="pos-rlt wrapper b b-light r r-2x">
                      <span class="arrow left pull-up"></span>
                      <p class="m-b-none">
                          <b>Juan Luna</b> ha publicado en el grupo <b>VENDE DE TODO FACEBOOK</b>
                      </p>
                    </div>
                    <small class="text-muted"><i class="fa fa-ok text-success"></i> Hace 1 minutos</small>
                  </div>
                </div>
                <div class="m-b">
                  <a href class="pull-left thumb-sm avatar"><img src="img/a3.jpg" alt=""></a>
                  <div class="m-l-sm inline">
                    <div class="pos-rlt wrapper b b-light r r-2x">
                      <span class="arrow left pull-up"></span>
                      <p class="m-b-none">
                          <b>Ana Salazar</b> ha publicado en el grupo <b>MERCADOLIBRE FB</b>
                      </p>
                    </div>
                    <small class="text-muted"><i class="fa fa-ok text-success"></i> Hace 1 minutos</small>
                  </div>
                </div> 
                <div class="m-b">
                  <a href class="pull-left thumb-sm avatar"><img src="img/a1.jpg" alt=""></a>
                  <div class="m-l-sm inline">
                    <div class="pos-rlt wrapper b b-light r r-2x">
                      <span class="arrow left pull-up"></span>
                      <p class="m-b-none">
                          <b>Marco Salas</b> ha publicado en el grupo <b>PUBLICITA TU NEGOCIO</b>
                      </p>
                    </div>
                    <small class="text-muted"><i class="fa fa-ok text-success"></i> Hace 2 minutos</small>
                  </div>
                </div>
              </div>
          </div>
          <div class="col-sm-6 wrapper-xl">
            <h3 class="text-dark font-bold m-b-lg">Ahorra tiempo con esta gran herramienta</h3>
            <ul class="list-unstyled  m-t-xl">
              <li data-ride="animated" data-animation="fadeInUp" data-delay="600">
                <i class="icon-check pull-left text-lg m-r m-t-sm"></i>
                <p class="clear m-b-lg"><strong>Crea tus entradas</strong>, configúrala con múltiples contenidos, imágenes y comentarios para evitar publicar siempre lo mismo. </p>
              </li>
              <li data-ride="animated" data-animation="fadeInUp" data-delay="900">
                <i class="icon-check pull-left text-lg m-r m-t-sm"></i>
                <p class="clear m-b-lg"><strong>Elige tu objetivo</strong>, selecciona tu público objetivo y programa las publicaciones como mejor se adecúe a tus necesidades...</p>
              </li>
              <li data-ride="animated" data-animation="fadeInUp" data-delay="1100">
                <i class="icon-check pull-left text-lg m-r m-t-sm"></i>
                <p class="clear m-b-lg"><strong>Relájate</strong>, MiAutoPub hara todo el trabajo por ti, solo tienes que revisar desde cualquier dispositivo que se esté realizando correctamente el trabajo y responder a las solicitudes de tus clientes.</p>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div> 
    <div class="home-platforms">
      <div class="container">
        <div class="row m-t-xl m-b-xxl">
          <div class="col-sm-6 wrapper-xl">
            <h3 class="text-black font-bold m-b-lg">Funciona en cualquier dispositivo</h3>
            <p class="h4 l-h-1x">Puedes administrar tus publicaciones en MiAutoPub desde cualquier dispositivo, solo necesitas un navegador y conexión a internet. </p>
            <p>&nbsp;</p>
            <p class="h4 l-h-1x">MiAutoPub ha sido diseñado para ordenadores y laptops, pero también puedes usarlo en tablets, smart phones, etc.</p>
          </div>
          <div class="col-sm-6" data-ride="animated" data-animation="fadeInLeft" data-delay="300">
            <div class="m-t-xxl text-center">
              <span class="text-2x text-muted"><i class="icon-screen-smartphone text-2x"></i></span>
              <span class="text-3x text-muted"><span class="text-2x"><i class="icon-screen-desktop text-2x"></i></span></span>
              <span class="text-3x text-muted"><i class="icon-screen-tablet text-2x"></i></span>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="bg-black dker">
      <div class="container">
        <div class="row m-t-xl m-b-xxl">
          <div class="col-sm-6" data-ride="animated" data-animation="fadeInLeft" data-delay="300">
            <div class="m-t-xxl text-center">
              <p>
              <a href class="text-sm btn btn-lg btn-rounded btn-default m-sm">
                <i class="fa fa-apple fa-3x pull-left m-l-sm"></i>
                <span class="block clear m-t-xs text-left m-r m-l">Pronto <b class="text-lg block font-bold">App Store</b>
                </span>
              </a>
              </p>
              <p>
              <a href class="text-sm btn btn-lg btn-rounded btn-default m-sm">
                <i class="fa fa-android fa-3x pull-left m-l-sm"></i>
                <span class="block clear m-t-xs text-left m-r m-l">Pronto <b class="text-lg block font-bold">Google Play</b>
                </span>
              </a>
              </p>
            </div>
          </div>
          <div class="col-sm-6 wrapper-xl">
            <h3 class="font-bold m-b-lg">Desarrollo continuo</h3>
            <p class="h4 l-h-1x">Para que puedas administrar tus publicaciones, nuestros desarrolladores están trabajando en aplicaciones móviles, para brindar esta característica a todos nuestros usuarios.</p>
            <p class="h4 l-h-1x">&nbsp;</p>
            <p class="h4 l-h-1x">Recibimos sugerencias par agregar características nuevas, resolvemos inquietudes y errores en los menores tiempos posibles.</p>
          </div>
        </div>
      </div>
    </div>
</div>
<?= $this->context->renderPartial('/layouts/nologin/footer'); ?>