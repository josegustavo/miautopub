<?= $this->context->renderPartial('/layouts/nologin/header'); ?>
<?php
    $authErrorMsg = ((isset($authError) && $authError) || ($authError = Yii::$app->session->getFlash('authError')))?$authError:"";
    
?>
<div id="content">
    <div class="container m-t-xxl padder-v">
        <div class="m-b-lg padder-v-xl">
            <div class="wrapper text-center m-b-lg">
                <?php if($authErrorMsg){ ?>
                <p class="error-summary h4"><?=$authErrorMsg?></p>
                <?php } ?>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel b-a m">
                        <div class="panel-heading bg-info dk no-border wrapper-sm">
                            <span class="h2">Órdenes</span>
                        </div>
                        <div class="panel-body table-responsive"  style="min-height: 200px">
                            <?php if(isset($orders) && $orders){ ?>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Comunidad</th>
                                        <th>Número</th>
                                        <th>Generado</th>
                                        <th>Estado</th>
                                        <th>Total</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody class="text-primary-dker">
                                    <?php foreach ($orders as $order){ ?>
                                    <tr>
                                        <td><?=$order['plan']['comunity']?></td>
                                        <td><a href="/order/<?=$order['id']?>"><?=$order['id']?></a></td>
                                        <td><?=date('d/m/Y h:i a', $order['created_time'])?></td>
                                        <td><span class="label <?=($order['status']=='paid')?'bg-success':'bg-light'?>"><?=\app\helpers\Functions::getStatusOrderName($order['status'])?></span></td>
                                        <td><?=$order['plan']['money'] . " " . $order['plan']['total']?></td>
                                        <td style="width: 150px">
                                            <?php if($order['status'] == 'pending'){ ?>
                                            <a class="btn btn-xs btn-success" href="/order/<?=$order['id']?>/pay">Pagar</a>
                                            <a class="btn btn-xs btn-danger" href="/order/<?=$order['id']?>?action=cancel">Cancelar</a>
                                            <?php } ?>
                                            <?php if($order['status'] == 'validating' && $isAdmin){ ?>
                                            <a class="btn btn-xs btn-primary" href="/order/<?=$order['id']?>/pay">Confirmar</a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <?php } else { ?>
                            <p class="error-summary h4">No se encontraron órdenes</p>
                            <?php } ?>
                        </div>
                        <div class="panel-footer">
                            <a href="/subscription" class="btn btn-default btn-lg pull-right"><i class="fa fa-caret-right"></i> Ver suscripciones</a>
                            <a href="/" class="btn btn-warning btn-lg"><i class="fa fa-share fa-rotate-180"></i> Regresar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</div>
<script>
    
</script>
<?= $this->context->renderPartial('/layouts/nologin/footer'); ?>