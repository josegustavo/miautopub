<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en" data-ng-app="app">
<?= $this->context->renderPartial('/layouts/header'); ?>
<body ng-controller="AppCtrl">
    <?= $content ?>
</body>
<?= $this->context->renderPartial('/layouts/footer'); ?>
</html>
<?php $this->endPage() ?>