<header id="header" class="navbar navbar-fixed-top bg-black padder-v"  data-spy="affix" data-offset-top="1">
  <div class="container">
    <div class="navbar-header">
      <button class="btn btn-link visible-xs pull-right m-r" type="button" data-toggle="collapse" data-target=".navbar-collapse">
        <i class="fa fa-bars"></i>
      </button>
      <a href="/" class="navbar-brand m-r-lg"><span class="h3 font-bold">MiAutoPub</span></a>
    </div>
    <div class="collapse navbar-collapse">
      <ul class="nav navbar-nav font-bold">
        <li>
          <a href="/#what">¿Qué es?</a>
        </li>
        <li>
          <a href="/#features">Características</a>
        </li>
        <li>
          <a href="/#why">¿Por qué?</a>
        </li>
        <li>
          <a href="/pricing">Solicitar</a>
        </li>
        <?php if(!Yii::$app->user->isGuest){ ?>
        <li>
          <a href="/order">Órdenes</a>
        </li>
        <li>
          <a href="/subscription">Suscripciones</a>
        </li>
        <?php }  ?>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li>
          <div class="m-t-sm">
            <?php if(Yii::$app->user->isGuest){ ?>
            <a href="/login" class="btn btn-link btn-sm">Iniciar sesión</a> o 
            <a href="/signup" class="btn btn-sm btn-success btn-rounded m-l"><strong>Registrarse GRATIS</strong></a>
            <?php } else { ?>
            <a href="/logout" class="btn btn-link btn-sm">Cerrar sesión</a> o 
            <a href="/admin" class="btn btn-sm btn-success btn-rounded m-l"><strong>Ir a Administración</strong></a>
            <?php }  ?>
          </div>
        </li>
      </ul>     
    </div>
  </div>
</header>