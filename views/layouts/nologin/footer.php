<footer id="footer">
    <div class="bg-white">
      <div class="container">
        <div class="row m-t-xl m-b-xl">
          <div class="col-sm-4">
            <h4 class="text-u-c m-b font-thin"><span class="b-b b-dark font-bold">Contáctanos</span></h4>
            <p class="text-sm"></p>
            <p class="m-b-xl"><a href="mailto:<?=Yii::$app->params['adminEmail']?>"><?=Yii::$app->params['adminEmail']?></a></p>
          </div>
          <div class="col-sm-4">
            <h4 class="text-u-c font-thin"><span class="b-b b-dark font-bold">Síguenos</span></h4>
            <div class="m-b-xl">
              <a href="#" class="btn btn-icon btn-rounded btn-dark"><i class="fa fa-facebook"></i></a>
              <a href="#" class="btn btn-icon btn-rounded btn-dark"><i class="fa fa-twitter"></i></a>
              <a href="#" class="btn btn-icon btn-rounded btn-dark"><i class="fa fa-google-plus"></i></a>
              <a href="#" class="btn btn-icon btn-rounded btn-dark"><i class="fa fa-linkedin"></i></a>
              <a href="#" class="btn btn-icon btn-rounded btn-dark"><i class="fa fa-pinterest"></i></a>
            </div>
          </div>
          <div class="col-sm-4">
            <h4 class="text-u-c m-b font-thin"><span class="b-b b-dark font-bold">Noticias</span></h4>
            <form class="form-inline m-t m-b m-b-xl">
              <div class="aside-xl inline">
                <div class="input-group">
                    <input type="email" id="address" name="address" class="form-control btn-rounded" placeholder="Tu correo">
                    <span class="input-group-btn">
                      <button class="btn btn-default btn-rounded" type="submit">Subscribírse</button>
                    </span>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="bg-light dk">
      <div class="container">
        <div class="row padder-v m-t">
          <div class="col-xs-8">
            <ul class="list-inline">
              <li><a href>Condiciones de uso</a></li>
            </ul> 
          </div>
          <div class="col-xs-4 text-right">
            MiAutoPub &copy; 2015
          </div>
        </div>
      </div>
    </div>
</footer>
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?3B7Gizt6XXdkCSgIWmvqxmENjZ0wmcKn";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<div id="fb-root">
</div>
<script src="js/app.nologin.js"></script>