<head>
    <meta charset="utf-8" />
    <base href="/">
    <title>Mi Autopub</title>
    <meta name="description" content="Angularjs, Html5, Music, Landing, 4 in 1 ui kits package" />
    <meta name="keywords" content="AngularJS, angular, bootstrap, admin, dashboard, panel, app, charts, components,flat, responsive, layout, kit, ui, route, web, app, widgets" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <?php
    foreach (Yii::$app->params['css_include'] as $css)
    {
        echo '<link rel="stylesheet" href="'.$css.'" type="text/css" />'."\n";
    }
    $FbLoginAppId = Yii::$app->params['FbLoginAppId'];
    $urlRoot = Yii::$app->params['urlRoot'];
    $comunity_id = $this->context->comunity_id();
    echo "<script>window.comunity_id = '$comunity_id';window.FbLoginAppId = '$FbLoginAppId';window.urlRoot = '$urlRoot';</script>";
    if( ($user = Yii::$app->user) && !$user->isGuest)
    {
        $fields = ['id','name','email','username','image_url'];
        $user_db = app\models\Publisher::find()->select($fields)->where(['id' => $user->id])->asArray()->one();
        $role = app\helpers\Meta::publisher($user->id)->role;
        if($role) $user_db['role'] = $role;
        if($user_db)
        {
            echo '<script>window.loginuser='.json_encode($user_db).'</script>';
        }
    }
    ?>
</head>